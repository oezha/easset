<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        @page {
        margin: 0;
    },
    body {
        margin:0;
        
        
    },
    h4{
        font-size:10px;
        margin-top: 0px;
        margin-bottom: 0px;
        margin-right: 0px;
        margin-left: 0px;
    },
    h5{
        font-size:9px;
        margin-top: 0px;
        margin-bottom: 0px;
        margin-right: 0px;
        margin-left: 0px;
    },
    h6{
        font-size:10px;
        margin-top: 0px;
        margin-bottom: 0px;
        margin-right: 0px;
        margin-left: 0px;
    },
    div{
        margin-top: 0px;
        margin-bottom: 0px;
        margin-right: 0px;
        margin-left: 0px;
    },
    div.page_break + div.page_break{
    page-break-before: always;
}
  
    </style>
    <script src="main.js"></script>
</head>
<body>
@foreach($barang as $key => $value)
<div style='page-break-after: always;page-break-inside: avoid;'>
<div class='w3-display-left'><img src="{{ public_path('/img/logo_kemkes.png') }}" alt="" width=60px height=60px></div>
<div class='w3-display-middle w3-center'>
<h6>{{ $satker }}</h6>
<h5>{{ $value->ur_sskel }}</h5>
<h4>{{ $value->kd_brg }}.{{ $value->no_aset }}</h4>
</div>
<div class='w3-display-right'><img src="{{ $value->qrcode_path }}" alt="" width=60px height=60px></div>
</div>


@endforeach
</body>
</html>