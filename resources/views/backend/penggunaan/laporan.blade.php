<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Laporan</title>
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        @page {
        margin: 0;
    },
    body {
        margin:0;
        font-family: Arial, Helvetica, sans-serif;
        
        
    },
    h3{
        font-size:16px;
         
    }
    h4{
        font-size:5px;
    },
    h5{
        font-size:5px;
    },
    h6{
        font-size:7px;
    },
    div{
        margin-top: 0px;
        margin-bottom: 0px;
        margin-right: 5px;
        margin-left: 5px;
    }
    p,table, ol{
        font-size: 14px;
    }
    </style>
    <script src="main.js"></script>
</head>
<body>
<!-- <div class='w3-display-left'>
    <img src="{{ public_path('/img/logo_kemkes.png') }}" alt="" width=60px height=60px>
</div> -->
<!-- <div class='w3-display-middle w3-center'> -->

    <div class="w3-container">
        <div class="row" style="margin-top: 20%;">
            <div class="col-md-12">
                <h3 style="text-align: center; padding: 0px; margin: 0px;">
                    <b>BERITA ACARA SERAH TERIMA BARANG MILIK NEGARA (BMN)</b></h3> 
                <h3 style="text-align: center;padding: 0px; margin: 0px;">
                    <b>BERUPA TABLET PC</b></h3> 
                <h3 style="text-align: center;padding: 0px; margin: 0px;">
                    <b>DIREKTORAT PRODUKSI DAN DISTRIBUSI KEFARMASIAN</b></h3>
                <hr style="height: 2px; width: 80%;">
                <h3 ><p style="text-align: center; padding: 0px; margin: 0px;">NOMOR : <b>KN.02.03/05/ /2018</b></p></h3>


                <div class="col-md-12">
                    <p style="width: 80%; margin-left: 80px;">
                        Pada hari ini <b>Kamis</b> tanggal <b>Dua</b> bulan <b>Agustus</b> tahun <b>Dua Ribu Delapan Belas</b>, bertempat di Direktorat Jenderal Kefarmasian dan Alat Kesehatan telah diserahkan Barang Milik Negara (BMN) berupa Tablet Pc kepada penanggung jawab sebagai berikut: 
                    </p>

                    <div class="col-md-6" style="margin-left: 80px; margin-top: -20px !important;">
                        <p>Dari:</p>
                        <table style="width:100%; margin-left: 30px;">
                          <tr>
                            <td>Nama    : Drs. Budi Pramono, M.Kes</td>
                          </tr>
                          <tr>
                            <td>Nip     : 196410071992031003</td>   
                          </tr>
                          <tr>
                            <td>Jabatan : Kepala Sub Bagian Tata Usaha</td>  
                          </tr>
                        </table>
                        <p style="margin-left: 40px; padding: 2px;">Selanjutnya disemut <b>PIHAK PERTAMA</b></p>
                    </div>
                    
                    <div class="col-md-6" style="margin-left: 80px; margin-top: -20px !important;">
                        <p>Kepada:</p>
                        <table style="width:100%; margin-left: 30px;" >
                          <tr>
                            <td>Nama    : Diara Oktania</td>
                          </tr>
                          <tr>
                            <td>Nip     : 197910152003122001</td>   
                          </tr>
                          <tr>
                            <td>Jabatan : Staf Tata Usaha</td>  
                          </tr>
                        </table>
                        <p style="margin-left: 40px; padding: 2px;">Selanjutnya disebut <b>PIHAK KEDUA</b></p>
                    </div>
                    <div class="col-sm-12" style="margin-left: 80px; width: 640px; margin-top: -20px !important;">
                    <p>Berdasarkan Kepada:</p>
                    <ol>
                        <li> Peraturan Pemerintah Nomor 27 Tahun 2014 tentang Pengelolaan Barang Milik Negara/Daerah </li>
                        <li> Peraturan Menteri Keuangan Nomor 120/PMK.06/2007 tentang Penatausahaan Barang Milik Negara </li>
                    </ol>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12" style="width: 80%; margin-left: 80px;">
                    <p><b>PIHAK PERTAMA</b> dan <b>PIHAK KEDUA</b> sepakat melakukan serah terima Barang Milik Negara dengan ketentuan sebagai berikut :</p>

                    <h3 style="text-align: center;">Pasal 1</h3>
                    <p>
                        <b>PIHAK PERTAMA</b> menyerahkan kepada <b>PIHAK KEDUA</b>, dan <b>PIHAK KEDUA</b> menerima hak penggunaan atas Barang Milik Negara berupa <b>Tablet PC</b> dalam kondisi <b>Baik</b>, untuk di gunakan sebagai perlengkapan operasional alat kantor dan menunjang tugas dan fungsi, sebagaimana tercantum dalam lampiran Berita Acara yang tidak dapat dipisahkan ini
                    </p>

                    <h3 style="text-align: center;">Pasal 2</h3>
                    <p>
                        Dengan ditandatanganinya Berita Acara Penggunaan BMN ini maka <b>PIHAK KEDUA</b> berkewajiban dengan penuh tanggungjawab untuk menjaga dan merawat dan memelihara Barang Milik Negara tersebut. 
                    </p>

                    <h3 style="text-align: center;">Pasal 3</h3>
                    <p>
                        <b>PIHAK KEDUA</b> bertanggung jawab apabila terjadi kehilangan, Kerusakan Barang Milik Negara (BMN) yang di sebabkan kesalahan pengguna / penggunaan tidak wajar sesuai dengan peraturan dan perundang-undangan yang berlaku. 
                    </p>

                    <h3 style="text-align: center;">Pasal 4</h3>
                    <p>
                       <b>PIHAK KEDUA</b> berkewajiban untuk menunjukan Barang Milik Negara jika diperlukan oleh Pengurus BMN. 
                    </p>
                    <h3 style="text-align: center;">Pasal 5</h3>
                    <p>
                        <b>PIHAK KEDUA</b> berkewajiban untuk mengembalikan Barang Milik Negara jika sudah tidak berkerja di lingkungan Direktorat Produksi Dan Distribusi Kefarmasian. 
                    </p>
                    <h3 style="text-align: center;">Pasal 6</h3>
                    <p>
                        Berita Acara Penggunaan Barang Milik Negara ini berlaku sejak tanggal ditetapkan dengan ketentuan apabila dikemudian hari terdapat kekeliruan pada Berita Acara ini maka akan diadakan perbaikan sebagaimana mestinya. 

                    </p>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">

                    <table style="width: 800px;margin-left: 80px;">
                        <tr>
                            <th>PIHAK KEDUA</th>
                            <th></th>
                            <th></th>
                            <th>PIHAK KETIGA</th>
                        </tr>
                    </table>
                    <br>
                    <br>
                    <br>

                    <table style="width: 800px;margin-left: 80px;">
                        <tr>
                            <th><b>Diara Oktania</b> </th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th><b>Drs. Budi Pramono, M.Kes</b></th>
                        </tr>
                        <tr>
                            <td>NIP. 197910152003122001</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>NIP. 196410071992031003</td>
                        </tr>
                    </table>

                </div>
            </div>
        </div>

    </div>
                            
                


    
<!-- </div> -->
<!-- <div class='w3-display-right'><img src="{{ $qrcode }}" alt="" width=60px height=60px></div> -->
</body>
</html>