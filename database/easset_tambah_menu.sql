/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 100134
 Source Host           : localhost:3306
 Source Schema         : easset

 Target Server Type    : MySQL
 Target Server Version : 100134
 File Encoding         : 65001

 Date: 05/03/2019 07:46:20
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for barang
-- ----------------------------
DROP TABLE IF EXISTS `barang`;
CREATE TABLE `barang`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kd_lokasi` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kd_brg` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ur_sskel` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `no_aset` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `no_urut` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `merk_type` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `thn_perlh` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kuantitas` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `rph_perlh` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `rph_akhir` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `p_pertama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `p_reguler` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `p_transaksi` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `rph_buku` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `asal_perlh` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `no_bukti` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `keterangan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tercatat` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kond` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `transaksi` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `qrcode_path` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 560 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of barang
-- ----------------------------
INSERT INTO `barang` VALUES (1, '024070199465983000KP', '3020101001', 'Sedan', '1', '1', 'TOYOTA ALTIS', '2013', '1', '362268600', '362268600', NULL, '-25876329', '-258763284', '77628987', 'Sesditjen Kefarmasian dan Alat Kesehatan', 'KR.003/SPK/MOBIL-ESELON I', 'BAST TK KR4 Setditjen Ke Prodisfar', 'KIB', '1', NULL, 'QRcode/Array.png');
INSERT INTO `barang` VALUES (2, '024070199465983000KP', '3020102003', 'Mini Bus ( Penumpang 14 Orang Kebawah )', '3', '1', '', '2012', '1', '235330000', '235330000', '-16809286', '-201711431', '3', '16809286', '', '', '', 'KIB', '1', NULL, 'QRCode/2.png');
INSERT INTO `barang` VALUES (3, '024070199465983000KP', '3020102003', 'Mini Bus ( Penumpang 14 Orang Kebawah )', '4', '2', '', '2012', '1', '235330000', '235330000', '-16809286', '-201711431', '3', '16809286', '', '', '', 'KIB', '1', NULL, 'QRCode/3.png');
INSERT INTO `barang` VALUES (4, '024070199465983000KP', '3020102003', 'Mini Bus ( Penumpang 14 Orang Kebawah )', '5', '1', 'Toyota Kijang Inova', '2013', '1', '172170000', '172170000', NULL, '-144060613', '-3513672', '24595715', 'Setditjen', 'KN.02.03/4/782/2013', '', 'KIB', '1', NULL, 'QRCode/4.png');
INSERT INTO `barang` VALUES (5, '024070199465983000KP', '3020104001', 'Sepeda Motor', '1', '1', 'Honda Vario CBS', '2011', '1', '15800000', '15800000', NULL, '-7900002', '-7899998', '0', '', '', 'Perubahan Kode Barang', 'KIB', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (6, '024070199465983000KP', '3020104001', 'Sepeda Motor', '2', '2', 'Honda Vario CBS', '2011', '1', '15800000', '15800000', NULL, '-7900002', '-7899998', '0', '', '', 'Perubahan Kode Barang', 'KIB', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (7, '024070199465983000KP', '3020104001', 'Sepeda Motor', '3', '3', 'Honda Vario CBS', '2011', '1', '15800000', '15800000', NULL, '-7900002', '-7899998', '0', '', '', 'Perubahan Kode Barang', 'KIB', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (8, '024070199465983000KP', '3020104001', 'Sepeda Motor', '4', '4', 'Honda Vario CBS', '2011', '1', '15800000', '15800000', NULL, '-7900002', '-7899998', '0', '', '', 'Perubahan Kode Barang', 'KIB', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (9, '024070199465983000KP', '3020104001', 'Sepeda Motor', '5', '5', 'Honda Vario CBS', '2011', '1', '15800000', '15800000', NULL, '-7900002', '-7899998', '0', '', '', 'Perubahan Kode Barang', 'KIB', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (10, '024070199465983000KP', '3020201004', 'Lori Dorong', '1', '1', 'Amtrong', '2003', '1', '415000', '318000', '-318000', NULL, NULL, '0', 'APBN 2003', '', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (11, '024070199465983000KP', '3050101002', 'Mesin Ketik Manual Standard (14-16 Inci)', '1', '1', 'oliveti14-16 inci', '2002', '1', '207500', '207500', '-207500', NULL, '0', '0', 'Hibah dari Yanmed', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (12, '024070199465983000KP', '3050101003', 'Mesin Ketik Manual Langewagon (18-27 Inci)', '1', '1', 'oliveti', '2004', '1', '3650000', '3138000', '-3138000', NULL, '0', '0', 'APBN 2004', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (13, '024070199465983000KP', '3050101008', 'Mesin Ketik Elektronik/Selektrik', '1', '1', 'GX-6750', '2011', '1', '1980000', '1980000', '-594000', '-1386000', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (14, '024070199465983000KP', '3050101008', 'Mesin Ketik Elektronik/Selektrik', '2', '2', 'GX-6750', '2011', '1', '1980000', '1980000', '-594000', '-1386000', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (15, '024070199465983000KP', '3050102002', 'Mesin Hitung Listrik', '1', '1', 'Cacio', '2004', '1', '2000000', '1545000', '-1545000', NULL, NULL, '0', 'APBN tahun 2004', '', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (16, '024070199465983000KP', '3050102003', 'Mesin Hitung Elektronik/Calculator', '1', '1', 'Citisen', '2004', '1', '200000', '200000', '-200000', NULL, NULL, '0', 'APBN TH. 2004', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (17, '024070199465983000KP', '3050102003', 'Mesin Hitung Elektronik/Calculator', '2', '2', 'Citizen', '2004', '1', '200000', '200000', '-200000', NULL, NULL, '0', 'APBN 2004', '', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (18, '024070199465983000KP', '3050102007', 'Mesin Penghitung Uang', '1', '1', '', '2016', '1', '1510000', '1510000', NULL, '-906000', NULL, '604000', 'CV. Cahaya Bukit Mas', 'BN.01.02/05/AP-KANTOR/03/2016', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (19, '024070199465983000KP', '3050103009', 'Mesin Fotocopy Electronic', '1', '1', 'CANON', '2010', '1', '9500000', '9500000', '-4750000', '-4750000', NULL, '0', 'APBN 2010', '118/F/DUKXI/2010', 'MONOCHROME LASER', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (20, '024070199465983000KP', '3050103009', 'Mesin Fotocopy Electronic', '2', '1', 'HP LASERJET M1536dnf', '2013', '1', '4205251', '4205251', NULL, '-4205251', NULL, '0', 'APBN 13', 'BN.01.03/05/APD/10/2013', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (21, '024070199465983000KP', '3050103009', 'Mesin Fotocopy Electronic', '3', '2', 'HP LASERJET M1536dnf', '2013', '1', '4205251', '4205251', NULL, '-4205251', NULL, '0', 'APBN 13', 'BN.01.03/05/APD/10/2013', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (22, '024070199465983000KP', '3050103009', 'Mesin Fotocopy Electronic', '4', '3', 'HP LASERJET M1536dnf', '2013', '1', '4205251', '4205251', NULL, '-4205251', NULL, '0', 'APBN 13', 'BN.01.03/05/APD/10/2013', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (23, '024070199465983000KP', '3050103009', 'Mesin Fotocopy Electronic', '5', '4', 'HP LASERJET M1536dnf', '2013', '1', '4205251', '4205251', NULL, '-4205251', NULL, '0', 'APBN 13', 'BN.01.03/05/APD/10/2013', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (24, '024070199465983000KP', '3050103011', 'Mesin Fotocopy Lainnya', '1', '1', 'FUJI', '2016', '1', '19795000', '19795000', NULL, '-9897500', NULL, '9897500', 'PT.AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (25, '024070199465983000KP', '3050104001', 'Lemari Besi/Metal', '1', '1', 'Yunika (Pendek)', '2002', '1', '855000', '723000', '-723000', NULL, '0', '0', 'DIK 2002', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (26, '024070199465983000KP', '3050104002', 'Lemari Kayu', '1', '1', 'Lokal', '2002', '1', '1050000', '441000', '-441000', NULL, NULL, '0', 'Hibah Yanmed', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (27, '024070199465983000KP', '3050104002', 'Lemari Kayu', '10', '2', 'Lokal', '2002', '1', '1590000', '1223000', '-1223000', NULL, NULL, '0', 'DIPA 2002', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (28, '024070199465983000KP', '3050104002', 'Lemari Kayu', '11', '3', 'Lokal', '2002', '1', '1585000', '1219000', '-1219000', NULL, NULL, '0', 'DIPA 2002', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (29, '024070199465983000KP', '3050104002', 'Lemari Kayu', '12', '4', 'Lokal', '2002', '1', '1478000', '1137000', '-1137000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (30, '024070199465983000KP', '3050104002', 'Lemari Kayu', '13', '5', 'Lokal', '2002', '1', '1478000', '1137000', '-1137000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (31, '024070199465983000KP', '3050104002', 'Lemari Kayu', '14', '6', 'Lokal', '2002', '1', '1478000', '1137000', '-1137000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (32, '024070199465983000KP', '3050104002', 'Lemari Kayu', '15', '7', 'Lokal', '2002', '1', '1478000', '1137000', '-1137000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (33, '024070199465983000KP', '3050104002', 'Lemari Kayu', '16', '8', 'Lokal', '2002', '1', '1478000', '1137000', '-1137000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (34, '024070199465983000KP', '3050104002', 'Lemari Kayu', '17', '9', 'Lokal', '2002', '1', '1478000', '1137000', '-1137000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (35, '024070199465983000KP', '3050104002', 'Lemari Kayu', '18', '10', 'Lokal', '2002', '1', '1478000', '1137000', '-1137000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (36, '024070199465983000KP', '3050104002', 'Lemari Kayu', '19', '1', 'Lokal', '2004', '1', '1750000', '1428000', '-1428000', NULL, NULL, '0', 'DIK 2004', '', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (37, '024070199465983000KP', '3050104002', 'Lemari Kayu', '2', '1', 'Lokal', '2002', '1', '1050000', '441000', '-441000', NULL, NULL, '0', 'Hibah Yanmed', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (38, '024070199465983000KP', '3050104002', 'Lemari Kayu', '3', '1', '4 Pintu (160x40x210)', '2005', '1', '5351500', '5351500', '-5351500', NULL, NULL, '0', 'DIPA 2005', '', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (39, '024070199465983000KP', '3050104002', 'Lemari Kayu', '4', '2', '4 Pintu (160x40x210)', '2005', '1', '5351500', '5351500', '-5351500', NULL, NULL, '0', 'DIPA 2005', '', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (40, '024070199465983000KP', '3050104002', 'Lemari Kayu', '5', '3', '4 Pintu (160x40x210)', '2005', '1', '5351500', '5351500', '-5351500', NULL, NULL, '0', 'DIPA 2005', '', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (41, '024070199465983000KP', '3050104002', 'Lemari Kayu', '6', '4', '4 Pintu (160x40x210)', '2005', '1', '5351500', '5351500', '-5351500', NULL, NULL, '0', 'DIPA 2005', '', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (42, '024070199465983000KP', '3050104002', 'Lemari Kayu', '7', '5', '4 Pintu (160x40x210)', '2005', '1', '5351500', '5351500', '-5351500', NULL, NULL, '0', 'DIPA 2005', '', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (43, '024070199465983000KP', '3050104002', 'Lemari Kayu', '8', '6', '4 Pintu (160x40x210)', '2005', '1', '5351500', '5351500', '-5351500', NULL, NULL, '0', 'DIPA 2005', '', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (44, '024070199465983000KP', '3050104002', 'Lemari Kayu', '9', '7', 'Lokal', '2005', '1', '3795000', '3795000', '-3795000', NULL, NULL, '0', 'DIPA 2005', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (45, '024070199465983000KP', '3050104005', 'Filing Cabinet Besi', '1', '1', 'Lokal', '2002', '1', '207500', '207500', '-207500', NULL, NULL, '0', 'Hibah Yanmed', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (46, '024070199465983000KP', '3050104005', 'Filing Cabinet Besi', '2', '2', 'Lokal', '2002', '1', '207500', '207500', '-207500', NULL, NULL, '0', 'Hibah Yanmed', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (47, '024070199465983000KP', '3050104005', 'Filing Cabinet Besi', '3', '3', 'Lokal', '2002', '1', '207500', '207500', '-207500', NULL, NULL, '0', 'Hibah Yanmed', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (48, '024070199465983000KP', '3050104005', 'Filing Cabinet Besi', '4', '4', 'Lokal', '2002', '1', '1586000', '1342000', '-1342000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (49, '024070199465983000KP', '3050104005', 'Filing Cabinet Besi', '5', '5', 'Lokal', '2002', '1', '1586000', '1342000', '-1342000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (50, '024070199465983000KP', '3050104005', 'Filing Cabinet Besi', '6', '6', 'Lokal', '2002', '1', '1586000', '1342000', '-1342000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (51, '024070199465983000KP', '3050104005', 'Filing Cabinet Besi', '7', '7', 'Lokal', '2002', '1', '1586000', '1342000', '-1342000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (52, '024070199465983000KP', '3050104005', 'Filing Cabinet Besi', '8', '1', 'Lokal', '2003', '1', '1586000', '1342000', '-1342000', NULL, '0', '0', 'DIK 2002', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (53, '024070199465983000KP', '3050104005', 'Filing Cabinet Besi', '9', '2', 'Lokal', '2003', '1', '1586000', '1352000', '-1352000', NULL, '0', '0', 'DIK 2003', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (54, '024070199465983000KP', '3050104007', 'Brandkas', '1', '1', 'Lokal', '2002', '1', '12485000', '10557000', '-10557000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (55, '024070199465983000KP', '3050104013', 'Buffet', '1', '1', 'Lokal', '2005', '1', '3905000', '3905000', '-3905000', NULL, NULL, '0', 'DIPA 2005', '', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (56, '024070199465983000KP', '3050104014', 'Mobile File', '1', '1', 'Lokal', '2013', '1', '28800000', '28800000', NULL, '-28800000', NULL, '0', 'APBN 2013', '051/FT/BDJ/X/2013', 'Type I', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (57, '024070199465983000KP', '3050104014', 'Mobile File', '2', '2', 'Lokal', '2013', '1', '38400000', '38400000', NULL, '-38400000', NULL, '0', 'APBN 2013', '051/FT/BDJ/X/2013', 'Type II', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (58, '024070199465983000KP', '3050104014', 'Mobile File', '3', '3', 'Lokal', '2013', '1', '31800000', '31800000', NULL, '-31800000', NULL, '0', 'APBN 2013', '051/FT/BDJ/X/2013', 'Type III', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (59, '024070199465983000KP', '3050104014', 'Mobile File', '4', '4', 'Lokal', '2013', '1', '28800000', '28800000', NULL, '-28800000', NULL, '0', 'APBN 2013', '051/FT/BDJ/X/2013', 'Type IV', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (60, '024070199465983000KP', '3050104014', 'Mobile File', '5', '5', 'Lokal', '2013', '1', '28800000', '28800000', NULL, '-28800000', NULL, '0', 'APBN 2013', '051/FT/BDJ/X/2013', 'Type IV', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (61, '024070199465983000KP', '3050104014', 'Mobile File', '6', '6', 'Lokal', '2013', '1', '28800000', '28800000', NULL, '-28800000', NULL, '0', 'APBN 2013', '051/FT/BDJ/X/2013', 'Type V', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (62, '024070199465983000KP', '3050105015', 'Alat Penghancur Kertas', '1', '1', 'HP AURORA', '2010', '1', '3150000', '3150000', '-1575000', '-1575000', '0', '0', 'APBN 2010', '118/F/DUKXI/2010', 'CV. Delpindo Utama Karya', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (63, '024070199465983000KP', '3050105015', 'Alat Penghancur Kertas', '2', '1', '', '2016', '1', '3920000', '3920000', NULL, '-2352000', NULL, '1568000', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/AP-KANTOR/03/2016', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (64, '024070199465983000KP', '3050105015', 'Alat Penghancur Kertas', '3', '1', 'IDEAL', '2018', '1', '23713700', '23713700', NULL, '-2371370', NULL, '21342330', 'PT. Air Mas Perkasa', 'KN.01.04/IV/005/OLDAT.1/3504/2018', 'Alat Penghancur Kertas', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (65, '024070199465983000KP', '3050105039', 'Display', '1', '1', '', '2006', '1', '4550000', '4550000', '-4550000', NULL, NULL, '0', 'BAP barang (CV. Bina Utama)', 'YF.01.01.IIA.042/03-', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (66, '024070199465983000KP', '3050105039', 'Display', '2', '2', '', '2006', '1', '4550000', '4550000', '-4550000', NULL, NULL, '0', 'BAP barang (CV. Bina Utama)', 'YF.01.01.IIA.042/03-', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (67, '024070199465983000KP', '3050105039', 'Display', '3', '3', '', '2006', '1', '4550000', '4550000', '-4550000', NULL, NULL, '0', 'BAP barang (CV. Bina Utama)', 'YF.01.01.IIA.042/03-', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (68, '024070199465983000KP', '3050105039', 'Display', '4', '1', '', '2007', '1', '7500000', '7500000', '-7500000', NULL, NULL, '0', 'APBN 2007', 'SP2D 774039F TGL 09-', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (69, '024070199465983000KP', '3050105039', 'Display', '5', '1', 'Sony', '2013', '1', '74415000', '74415000', NULL, '-74415000', NULL, '0', 'APBN 2013', '30810', 'Baru', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (70, '024070199465983000KP', '3050105048', 'LCD Projector/Infocus', '1', '1', 'ACER P 1165', '2008', '1', '10872744', '10872744', '-9785470', '-1087274', '0', '0', 'CV. DELPINDO UTAMA KARYA', 'PL.01.01/3/275/2008', 'DLP, NATIVE XGA', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (71, '024070199465983000KP', '3050105048', 'LCD Projector/Infocus', '10', '1', 'Projector EPSON EB-1771W', '2015', '1', '12947000', '12947000', NULL, '-9062900', NULL, '3884100', 'CV. ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (72, '024070199465983000KP', '3050105048', 'LCD Projector/Infocus', '11', '2', 'Projector EPSON EB-1771W', '2015', '1', '12947000', '12947000', NULL, '-9062900', NULL, '3884100', 'CV. ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (73, '024070199465983000KP', '3050105048', 'LCD Projector/Infocus', '12', '1', 'Panasonic VW540', '2018', '1', '22666500', '22666500', NULL, '-2266650', NULL, '20399850', 'PT. Air Mas Perkasa', 'Kuitansi No.PSI-AMP-1812-00922', 'Pengadaan Proyektor', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (74, '024070199465983000KP', '3050105048', 'LCD Projector/Infocus', '2', '1', 'ACER P1266I', '2009', '1', '13409550', '13409550', '-9386685', '-4022865', NULL, '0', 'PT. Complus Sistem Solusi', 'PL.01.01/3/168/2009', 'Project System DLP', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (75, '024070199465983000KP', '3050105048', 'LCD Projector/Infocus', '3', '2', 'ACER P1266I', '2009', '1', '13409550', '13409550', '-9386685', '-4022865', NULL, '0', 'PT. Complus Sistem Solusi', 'PL.01.01/3/168/2009', 'Project System DLP', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (76, '024070199465983000KP', '3050105048', 'LCD Projector/Infocus', '4', '1', 'SONY', '2010', '1', '12850000', '12850000', '-6425000', '-6425000', NULL, '0', 'APBN 2010', '118/F/DUKXI/2010', '3LCD TECHNOLOGY', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (77, '024070199465983000KP', '3050105048', 'LCD Projector/Infocus', '5', '1', 'Panasonic', '2011', '1', '17806800', '17806800', '-5342040', '-12464760', NULL, '0', 'APBN', 'PL.01.01.05/LCD/13/IX/2011', 'Pembelian 2011', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (78, '024070199465983000KP', '3050105048', 'LCD Projector/Infocus', '6', '2', 'Sony', '2011', '1', '12196800', '12196800', '-3659040', '-8537760', NULL, '0', 'APBN', 'PL.01.01.05/LCD/13/IX/2011', 'Pembelian 2011', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (79, '024070199465983000KP', '3050105048', 'LCD Projector/Infocus', '7', '1', 'SONY Type EW 130', '2012', '1', '14746600', '14746600', '-1474660', '-13271940', NULL, '0', 'APBN', '905228A', '2012', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (80, '024070199465983000KP', '3050105048', 'LCD Projector/Infocus', '8', '1', 'Sony', '2013', '1', '11000000', '11000000', NULL, '-11000000', NULL, '0', 'APBN 2013', '30810', 'Infocus', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (81, '024070199465983000KP', '3050105048', 'LCD Projector/Infocus', '9', '1', 'EPSON EB-1751', '2014', '1', '11995000', '11995000', NULL, '-10795500', NULL, '1199500', 'CV.CAHAYA BUKIT MAS', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (82, '024070199465983000KP', '3050105057', 'Pintu Elektrik (yang Memakai Akses)', '1', '1', 'SOLUTION X304', '2015', '1', '4101000', '4101000', NULL, '-2460600', '-820200', '820200', 'Sesditjen farmalkes', 'BAPP KN.01.01/4/0296/2015', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (83, '024070199465983000KP', '3050105058', 'Focusing Screen/Layar LCD Projector', '1', '1', 'APOLLO', '2010', '1', '2150000', '2150000', '-1075000', '-1075000', '0', '0', 'APBN 2010', '118/F/DUKXI/2010', '100\" DIAGONAL SIZE', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (84, '024070199465983000KP', '3050105058', 'Focusing Screen/Layar LCD Projector', '2', '1', 'Infocus', '2013', '1', '9350000', '9350000', NULL, '-9350000', NULL, '0', 'APBN 2013', '30810', 'Baru', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (85, '024070199465983000KP', '3050199999', 'Alat Kantor Lainnya', '1', '1', '', '2016', '1', '18159680', '18159680', NULL, '-10895808', NULL, '7263872', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (86, '024070199465983000KP', '3050199999', 'Alat Kantor Lainnya', '2', '2', '-', '2016', '1', '1500000', '1500000', NULL, '-750000', NULL, '750000', '-', '-', 'Kabel HDMI', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (87, '024070199465983000KP', '3050201001', 'Meja Kerja Besi/Metal', '1', '1', 'Vinoti Glass Coffee Table', '2016', '1', '4950000', '4950000', NULL, '-1485000', '-990000', '2475000', 'Sesditjen Farmalkes', 'BN.06.02/3/4387/2016', 'TK PM Meubelair TA 2016', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (88, '024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '1', '1', '1/2 Biro', '2002', '1', '78250', '78250', '-78250', NULL, '0', '0', 'Hibah Itjen', 'BAST  NO.PL.00.07.1.', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (89, '024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '10', '2', '1 Biro Eselon III', '2002', '1', '95000', '95000', '-95000', NULL, NULL, '0', 'Hibah Itjen', 'No.01T.PS.19.03.214.', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (90, '024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '11', '3', '1 Biro Eselon III', '2002', '1', '95000', '95000', '-95000', NULL, NULL, '0', 'Hibah Itjen', 'No.01T.PS.19.03.214.', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (91, '024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '12', '4', '1 Biro Eselon III', '2002', '1', '95000', '95000', '-95000', NULL, NULL, '0', 'Hibah Itjen', 'No.01T.PS.19.03.214.', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (92, '024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '13', '5', '1 Biro Eselon III', '2002', '1', '95000', '95000', '-95000', NULL, NULL, '0', 'Hibah Itjen', 'No.01T.PS.19.03.214.', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (93, '024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '14', '1', 'Meja Eselon III', '2003', '1', '1445000', '1140000', '-1140000', NULL, NULL, '0', 'APBN 2003', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (94, '024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '15', '2', 'Meja Eselon III', '2003', '1', '1445000', '1140000', '-1140000', NULL, NULL, '0', 'APBN 2003', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (95, '024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '16', '3', 'Meja Eselon III', '2003', '1', '1445000', '1140000', '-1140000', NULL, NULL, '0', 'APBN 2003', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (96, '024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '17', '1', 'Meja Es.IV', '2002', '1', '1622500', '1324000', '-1324000', NULL, '0', '0', 'APBN 2004', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (97, '024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '18', '2', 'Meja Es.IV', '2002', '1', '1622500', '1324000', '-1324000', NULL, '0', '0', 'APBN 2004', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (98, '024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '19', '3', 'Meja Es.IV', '2002', '1', '1622500', '1324000', '-1324000', NULL, '0', '0', 'APBN 2004', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (99, '024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '2', '4', '1/2 Biro', '2002', '1', '78250', '78250', '-78250', NULL, '0', '0', 'Hibah Itjen', 'BAST  NO.PL.00.07.1.', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (100, '024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '20', '5', 'Meja Ess IV', '2002', '1', '1705000', '1705000', '-1705000', NULL, '0', '0', 'DIPA 2005', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (101, '024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '21', '6', 'Meja Ess IV', '2002', '1', '1705000', '1705000', '-1705000', NULL, '0', '0', 'DIPA 2005', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (102, '024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '22', '7', 'Meja Ess IV', '2002', '1', '1705000', '1705000', '-1705000', NULL, '0', '0', 'DIPA 2005', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (103, '024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '23', '8', 'Meja Ess IV', '2002', '1', '1705000', '1705000', '-1705000', NULL, '0', '0', 'DIPA 2005', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (104, '024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '24', '9', 'Meja Ess IV', '2002', '1', '1705000', '1705000', '-1705000', NULL, '0', '0', 'DIPA 2005', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (105, '024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '25', '10', 'Meja Ess IV', '2002', '1', '1705000', '1705000', '-1705000', NULL, '0', '0', 'DIPA 2005', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (106, '024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '26', '11', 'Meja Ess IV', '2002', '1', '1705000', '1705000', '-1705000', NULL, '0', '0', 'DIPA 2005', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (107, '024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '27', '12', 'Meja Ess.II', '2002', '1', '95000', '95000', '-95000', NULL, '0', '0', 'Hibah Itjen', 'No.01T.PS 19.03.214.', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (108, '024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '28', '13', 'Meja Ess.II', '2002', '1', '1720000', '1323000', '-1323000', NULL, '0', '0', 'APBN 2002', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (109, '024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '29', '1', 'lokal', '2013', '1', '3261500', '3261500', NULL, '-3131040', '-130460', '0', 'setditjen', 'KN.02.03/4/782/2013', 'Meja Kerja Direktur', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (110, '024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '3', '1', '1/2 Biro', '2002', '1', '78250', '78250', '-78250', NULL, '0', '0', 'Hibah Itjen', 'BAST  NO.PL.00.07.1.', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (111, '024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '30', '1', 'lokal', '2013', '1', '2255000', '2255000', NULL, '-2164800', '-90200', '0', 'setditjen', 'KN.02.03/4/782/2013', 'Meja Kerja Direktur', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (112, '024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '31', '2', 'lokal', '2013', '1', '2255000', '2255000', NULL, '-2164800', '-90200', '0', 'setditjen', 'KN.02.03/4/782/2013', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (113, '024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '32', '3', 'lokal', '2013', '1', '2255000', '2255000', NULL, '-2164800', '-90200', '0', 'setditjen', 'KN.02.03/4/782/2013', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (114, '024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '33', '4', 'lokal', '2013', '1', '2255000', '2255000', NULL, '-2164800', '-90200', '0', 'setditjen', 'KN.02.03/4/782/2013', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (115, '024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '34', '5', 'lokal', '2013', '1', '2255000', '2255000', NULL, '-2164800', '-90200', '0', 'setditjen', 'KN.02.03/4/782/2013', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (116, '024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '35', '6', 'lokal', '2013', '1', '2255000', '2255000', NULL, '-2164800', '-90200', '0', 'setditjen', 'KN.02.03/4/782/2013', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (117, '024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '4', '1', '1/2 Biro', '2002', '1', '78250', '78250', '-78250', NULL, '0', '0', 'Hibah Itjen', 'BAST  NO.PL.00.07.1.', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (118, '024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '5', '2', '1/2 Biro', '2002', '1', '78250', '78250', '-78250', NULL, '0', '0', 'Hibah Itjen', 'BAST  NO.PL.00.07.1.', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (119, '024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '6', '3', '1/2 Biro', '2002', '1', '78250', '78250', '-78250', NULL, '0', '0', 'Hibah Itjen', 'BAST  NO.PL.00.07.1.', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (120, '024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '7', '4', '1/2 Biro', '2002', '1', '78250', '78250', '-78250', NULL, '0', '0', 'Hibah Itjen', 'BAST  NO.PL.00.07.1.', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (121, '024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '8', '5', '1/2 Biro', '2002', '1', '78250', '78250', '-78250', NULL, '0', '0', 'Hibah Itjen', 'BAST  NO.PL.00.07.1.', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (122, '024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '9', '6', '1/2 Biro', '2002', '1', '78250', '78250', '-78250', NULL, '0', '0', 'Hibah Itjen', 'BAST  NO.PL.00.07.1.', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (123, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '1', '1', 'RAKUDA', '2005', '1', '660000', '660000', '-660000', NULL, NULL, '0', 'APBN', '', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (124, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '10', '1', 'YESTINE', '2003', '1', '727000', '620000', '-620000', NULL, NULL, '0', 'DIK 2003', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (125, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '100', '1', 'Kursi Rapat (K1)', '2016', '1', '1435000', '1435000', NULL, '-430500', '-287000', '717500', 'Sesditjen Farmalkes', 'BN.06.02/3/4387/2016', 'TK PM Meubelair TA 2016', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (126, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '101', '2', 'Kursi Rapat (K1)', '2016', '1', '1435000', '1435000', NULL, '-430500', '-287000', '717500', 'Sesditjen Farmalkes', 'BN.06.02/3/4387/2016', 'TK PM Meubelair TA 2016', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (127, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '102', '3', 'Kursi Rapat (K1)', '2016', '1', '1435000', '1435000', NULL, '-430500', '-287000', '717500', 'Sesditjen Farmalkes', 'BN.06.02/3/4387/2016', 'TK PM Meubelair TA 2016', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (128, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '103', '4', 'Kursi Rapat (K1)', '2016', '1', '1435000', '1435000', NULL, '-430500', '-287000', '717500', 'Sesditjen Farmalkes', 'BN.06.02/3/4387/2016', 'TK PM Meubelair TA 2016', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (129, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '104', '5', 'Kursi Rapat (K1)', '2016', '1', '1435000', '1435000', NULL, '-430500', '-287000', '717500', 'Sesditjen Farmalkes', 'BN.06.02/3/4387/2016', 'TK PM Meubelair TA 2016', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (130, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '105', '6', 'Kursi Rapat (K1)', '2016', '1', '1435000', '1435000', NULL, '-430500', '-287000', '717500', 'Sesditjen Farmalkes', 'BN.06.02/3/4387/2016', 'TK PM Meubelair TA 2016', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (131, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '106', '7', 'Kursi Rapat (K1)', '2016', '1', '1435000', '1435000', NULL, '-430500', '-287000', '717500', 'Sesditjen Farmalkes', 'BN.06.02/3/4387/2016', 'TK PM Meubelair TA 2016', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (132, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '107', '8', 'Kursi Rapat (K1)', '2016', '1', '1435000', '1435000', NULL, '-430500', '-287000', '717500', 'Sesditjen Farmalkes', 'BN.06.02/3/4387/2016', 'TK PM Meubelair TA 2016', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (133, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '11', '1', 'ERGOTEC', '2004', '1', '900000', '780000', '-780000', NULL, NULL, '0', 'APBN 2004', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (134, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '12', '2', 'ERGOTEC', '2004', '1', '900000', '780000', '-780000', NULL, NULL, '0', 'APBN 2004', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (135, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '13', '3', 'ERGOTEC', '2004', '1', '900000', '780000', '-780000', NULL, NULL, '0', 'APBN 2004', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (136, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '14', '4', 'ERGOTEC', '2004', '1', '900000', '780000', '-780000', NULL, NULL, '0', 'APBN 2004', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (137, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '15', '1', 'RAKUDA', '2002', '1', '1100000', '1100000', '-1100000', NULL, '0', '0', 'APBN 2005', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (138, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '16', '2', 'RAKUDA', '2002', '1', '1100000', '1100000', '-1100000', NULL, '0', '0', 'APBN 2005', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (139, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '17', '3', 'RAKUDA', '2002', '1', '1100000', '1100000', '-1100000', NULL, '0', '0', 'APBN 2005', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (140, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '18', '4', 'Rakuda', '2002', '1', '46000', '46000', '-46000', NULL, '0', '0', 'Hibah Itjen', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (141, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '19', '5', 'Rakuda', '2002', '1', '46000', '46000', '-46000', NULL, NULL, '0', 'Hibah Itjen', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (142, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '2', '1', 'RAKUDA', '2005', '1', '660000', '660000', '-660000', NULL, NULL, '0', 'APBN 2005', '', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (143, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '20', '1', 'Rakuda', '2002', '1', '46000', '46000', '-46000', NULL, NULL, '0', 'Hibah Itjen', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (144, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '21', '2', 'Rakuda', '2002', '1', '46000', '46000', '-46000', NULL, NULL, '0', 'Hibah Itjen', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (145, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '22', '3', 'Rakuda', '2002', '1', '46000', '46000', '-46000', NULL, NULL, '0', 'Hibah Itjen', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (146, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '23', '4', 'Rakuda', '2002', '1', '46000', '46000', '-46000', NULL, NULL, '0', 'Hibah Itjen', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (147, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '24', '5', 'Rakuda', '2002', '1', '46000', '46000', '-46000', NULL, NULL, '0', 'Hibah Itjen', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (148, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '25', '6', 'Rakuda', '2002', '1', '46000', '46000', '-46000', NULL, NULL, '0', 'Hibah Itjen', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (149, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '26', '7', 'Rakuda', '2002', '1', '46000', '46000', '-46000', NULL, NULL, '0', 'Hibah Itjen', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (150, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '27', '8', 'Rakuda', '2002', '1', '46000', '46000', '-46000', NULL, NULL, '0', 'Hibah Itjen', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (151, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '28', '9', 'Rakuda', '2002', '1', '46000', '46000', '-46000', NULL, NULL, '0', 'Hibah Itjen', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (152, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '29', '10', 'Rakuda', '2002', '1', '46000', '46000', '-46000', NULL, NULL, '0', 'Hibah Itjen', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (153, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '3', '1', 'RAKUDA', '2005', '1', '660000', '660000', '-660000', NULL, '0', '0', 'APBN 2005', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (154, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '30', '1', 'Rakuda', '2002', '1', '46000', '46000', '-46000', NULL, NULL, '0', 'Hibah Itjen', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (155, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '31', '1', 'Kingdom/Staf', '2004', '1', '497500', '432000', '-432000', NULL, '0', '0', 'APBN 2004', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (156, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '32', '2', 'Kingdom/Staf', '2004', '1', '497500', '432000', '-432000', NULL, '0', '0', 'APBN 2004', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (157, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '33', '3', 'Kingdom/Staf', '2004', '1', '497500', '432000', '-432000', NULL, NULL, '0', 'APBN 2004', '', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (158, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '34', '4', 'Kingdom/Staf', '2004', '1', '497500', '432000', '-432000', NULL, NULL, '0', 'APBN 2004', '', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (159, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '35', '5', 'Kingdom/Staf', '2004', '1', '497500', '432000', '-432000', NULL, NULL, '0', 'APBN 2004', '', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (160, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '36', '6', 'Kingdom/Staf', '2004', '1', '497500', '432000', '-432000', NULL, NULL, '0', 'APBN 2004', '', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (161, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '37', '7', 'Kingdom/Staf', '2004', '1', '497500', '432000', '-432000', NULL, NULL, '0', 'APBN 2004', '', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (162, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '38', '8', 'Kingdom/Staf', '2004', '1', '497500', '432000', '-432000', NULL, NULL, '0', 'APBN 2004', '', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (163, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '39', '9', 'Kingdom/Staf', '2004', '1', '497500', '432000', '-432000', NULL, NULL, '0', 'APBN 2004', '', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (164, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '4', '1', 'RAKUDA', '2005', '1', '660000', '660000', '-660000', NULL, '0', '0', 'APBN 2005', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (165, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '40', '1', 'Kingdom/Staf', '2004', '1', '497500', '432000', '-432000', NULL, NULL, '0', 'APBN 2004', '', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (166, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '41', '1', 'Kursi Rapat/Futura', '2002', '1', '147000', '147000', '-147000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (167, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '42', '2', 'Kursi Rapat/Futura', '2002', '1', '147000', '147000', '-147000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (168, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '43', '3', 'Kursi Rapat/Futura', '2002', '1', '147000', '147000', '-147000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (169, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '44', '4', 'Kursi Rapat/Futura', '2002', '1', '147000', '147000', '-147000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (170, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '45', '5', 'Kursi Rapat/Futura', '2002', '1', '147000', '147000', '-147000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (171, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '46', '6', 'Kursi Rapat/Futura', '2002', '1', '147000', '147000', '-147000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (172, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '47', '7', 'Kursi Rapat/Futura', '2002', '1', '147000', '147000', '-147000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (173, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '48', '8', 'Kursi Rapat/Futura', '2002', '1', '147000', '147000', '-147000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (174, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '49', '9', 'Kursi Rapat/Futura', '2002', '1', '147000', '147000', '-147000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (175, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '5', '10', 'ICHIKO', '2002', '1', '475000', '402000', '-402000', NULL, NULL, '0', 'APBN 2002', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (176, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '50', '11', 'Kursi Rapat/Futura', '2002', '1', '147000', '147000', '-147000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (177, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '51', '1', 'Lokal', '2013', '1', '1166000', '1166000', NULL, '-1119360', '-46640', '0', 'setditjen', 'KN.02.03/4/782/2013', 'Kursi Model A/Rapat', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (178, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '52', '2', 'Lokal', '2013', '1', '1166000', '1166000', NULL, '-1119360', '-46640', '0', 'setditjen', 'KN.02.03/4/782/2013', 'Kursi Model A/Rapat', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (179, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '53', '3', 'Lokal', '2013', '1', '1166000', '1166000', NULL, '-1119360', '-46640', '0', 'setditjen', 'KN.02.03/4/782/2013', 'Kursi Model A/Rapat', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (180, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '54', '4', 'Lokal', '2013', '1', '1166000', '1166000', NULL, '-1119360', '-46640', '0', 'setditjen', 'KN.02.03/4/782/2013', 'Kursi Model A/Rapat', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (181, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '55', '5', 'Lokal', '2013', '1', '1166000', '1166000', NULL, '-1119360', '-46640', '0', 'setditjen', 'KN.02.03/4/782/2013', 'Kursi Model A/Rapat', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (182, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '56', '6', 'Lokal', '2013', '1', '1166000', '1166000', NULL, '-1119360', '-46640', '0', 'setditjen', 'KN.02.03/4/782/2013', 'Kursi Model A/Rapat', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (183, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '57', '7', 'Lokal', '2013', '1', '1166000', '1166000', NULL, '-1119360', '-46640', '0', 'setditjen', 'KN.02.03/4/782/2013', 'Kursi Model A/Rapat', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (184, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '58', '8', 'Lokal', '2013', '1', '1166000', '1166000', NULL, '-1119360', '-46640', '0', 'setditjen', 'KN.02.03/4/782/2013', 'Kursi Model A/Rapat', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (185, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '59', '9', 'Lokal', '2013', '1', '1485000', '1485000', NULL, '-1425600', '-59400', '0', 'setditjen', 'KN.02.03/4/782/2013', 'Kursi Model M/Rapat Es.II', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (186, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '6', '1', 'ICHIKO', '2002', '1', '475000', '402000', '-402000', NULL, NULL, '0', 'APBN 2002', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (187, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '60', '1', 'Lokal', '2013', '1', '1485000', '1485000', NULL, '-1425600', '-59400', '0', 'setditjen', 'KN.02.03/4/782/2013', 'Kursi Model M/Rapat Es.II', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (188, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '61', '2', 'Lokal', '2013', '1', '1485000', '1485000', NULL, '-1425600', '-59400', '0', 'setditjen', 'KN.02.03/4/782/2013', 'Kursi Model M/Rapat Es.II', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (189, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '62', '3', 'Lokal', '2013', '1', '1485000', '1485000', NULL, '-1425600', '-59400', '0', 'setditjen', 'KN.02.03/4/782/2013', 'Kursi Model M/Rapat Es.II', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (190, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '63', '4', 'Lokal', '2013', '1', '1485000', '1485000', NULL, '-1425600', '-59400', '0', 'setditjen', 'KN.02.03/4/782/2013', 'Kursi Model M/Rapat Es.II', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (191, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '64', '5', 'Lokal', '2013', '1', '1485000', '1485000', NULL, '-1425600', '-59400', '0', 'setditjen', 'KN.02.03/4/782/2013', 'Kursi Model M/Rapat Es.II', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (192, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '65', '6', 'Lokal', '2013', '1', '1485000', '1485000', NULL, '-1425600', '-59400', '0', 'setditjen', 'KN.02.03/4/782/2013', 'Kursi Model M/Rapat Es.II', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (193, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '66', '7', 'lokal', '2013', '1', '1650000', '1650000', NULL, '-1584000', '-66000', '0', 'setditjen', 'KN.02.03/4/782/2013', 'Kursi Model C / Es.II', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (194, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '67', '8', 'lokal', '2013', '1', '632500', '632500', NULL, '-607200', '-25300', '0', 'setditjen', 'KN.02.03/4/782/2013', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (195, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '68', '9', 'lokal', '2013', '1', '632500', '632500', NULL, '-607200', '-25300', '0', 'setditjen', 'KN.02.03/4/782/2013', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (196, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '69', '10', 'lokal', '2013', '1', '632500', '632500', NULL, '-607200', '-25300', '0', 'setditjen', 'KN.02.03/4/782/2013', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (197, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '7', '1', 'YESTINE', '2002', '1', '1240000', '1049000', '-1049000', NULL, NULL, '0', 'APBN 2002', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (198, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '70', '1', 'lokal', '2013', '1', '632500', '632500', NULL, '-607200', '-25300', '0', 'setditjen', 'KN.02.03/4/782/2013', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (199, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '71', '2', 'lokal', '2013', '1', '632500', '632500', NULL, '-607200', '-25300', '0', 'setditjen', 'KN.02.03/4/782/2013', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (200, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '72', '1', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (201, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '73', '2', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (202, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '74', '3', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (203, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '75', '4', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (204, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '76', '5', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (205, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '77', '6', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (206, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '78', '7', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (207, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '79', '8', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (208, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '8', '1', 'YESTINE', '2003', '1', '727000', '620000', '-620000', NULL, NULL, '0', 'DIK 2003', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (209, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '80', '1', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (210, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '81', '2', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (211, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '82', '3', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (212, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '83', '4', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (213, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '84', '5', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (214, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '85', '6', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (215, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '86', '7', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (216, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '87', '8', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (217, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '88', '9', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (218, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '89', '10', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (219, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '9', '1', 'YESTINE', '2003', '1', '727000', '620000', '-620000', NULL, NULL, '0', 'DIK 2003', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (220, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '90', '1', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-651252', '-431808', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (221, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '91', '2', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-651252', '-431808', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (222, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '92', '3', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-651252', '-431808', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (223, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '93', '4', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-651252', '-431808', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (224, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '94', '5', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (225, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '95', '6', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (226, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '96', '7', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (227, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '97', '8', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (228, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '98', '1', 'Kursi Rapat (K1)', '2016', '1', '1435000', '1435000', NULL, '-430500', '-287000', '717500', 'Sesditjen Farmalkes', 'BN.06.02/3/4387/2016', 'TK PM Meubelair TA 2016', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (229, '024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '99', '2', 'Kursi Rapat (K1)', '2016', '1', '1435000', '1435000', NULL, '-430500', '-287000', '717500', 'Sesditjen Farmalkes', 'BN.06.02/3/4387/2016', 'TK PM Meubelair TA 2016', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (230, '024070199465983000KP', '3050201005', 'Sice', '1', '1', 'Morales', '2002', '1', '4980000', '3829000', '-3829000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (231, '024070199465983000KP', '3050201005', 'Sice', '2', '1', 'Ess.III', '2005', '1', '2332000', '2332000', '-2332000', NULL, NULL, '0', 'DIPA 2005', '', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (232, '024070199465983000KP', '3050201005', 'Sice', '3', '2', 'Ess.III', '2005', '1', '2332000', '2332000', '-2332000', NULL, NULL, '0', 'DIPA 2005', '', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (233, '024070199465983000KP', '3050201005', 'Sice', '4', '3', 'Ess.III', '2005', '1', '2332000', '2332000', '-2332000', NULL, NULL, '0', 'DIPA 2005', '', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (234, '024070199465983000KP', '3050201005', 'Sice', '5', '1', 'Vinoti', '2016', '1', '9900000', '9900000', NULL, '-2970000', '-1980000', '4950000', 'Sesditjen Farmalkes', 'BN.06.02/3/4387/2016', 'TK PM Meubelair TA 2016', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (235, '024070199465983000KP', '3050201005', 'Sice', '6', '2', 'Vinoti', '2016', '1', '5450000', '5450000', NULL, '-1635000', '-1090000', '2725000', 'Sesditjen Farmalkes', 'BN.06.02/3/4387/2016', 'TK PM Meubelair TA 2016', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (236, '024070199465983000KP', '3050201008', 'Meja Rapat', '1', '1', '', '2005', '1', '4510000', '4510000', '-4510000', NULL, NULL, '0', 'DIPA 2005', '', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (237, '024070199465983000KP', '3050201008', 'Meja Rapat', '2', '1', 'lokal', '2013', '1', '5720000', '5720000', NULL, '-5491200', '-228800', '0', 'setditjen', 'KN.02.03/4/782/2013', 'Meja Rapat C/Panjang', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (238, '024070199465983000KP', '3050201008', 'Meja Rapat', '3', '2', 'lokal', '2013', '1', '2491500', '2491500', NULL, '-2391840', '-99660', '0', 'setditjen', 'KN.02.03/4/782/2013', 'Meja Rapat Model D/ Bulat', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (239, '024070199465983000KP', '3050201028', 'Workstation', '1', '1', 'Work Station (WS4)', '2016', '1', '13400000', '13400000', NULL, '-4020000', '-2680000', '6700000', 'Sesditjen Farmalkes', 'BN.06.02/3/4387/2016', 'TK PM Meubelair TA 2016', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (240, '024070199465983000KP', '3050201999', 'Meubelair Lainnya', '1', '1', '-', '2018', '1', '42900000', '42900000', NULL, '-4290000', NULL, '38610000', 'CV.Putra Bathuphat Mandiri', 'Kuitansi No.018/KW-PBM/XII/2018', 'Mebeler Ruang Direktur', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (241, '024070199465983000KP', '3050202003', 'Jam Elektronik', '1', '1', 'Lokal', '2002', '1', '85000', '85000', '-85000', NULL, '0', '0', 'Hibah Biro Umum', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (242, '024070199465983000KP', '3050202003', 'Jam Elektronik', '2', '2', 'Lokal', '2002', '1', '85000', '85000', '-85000', NULL, '0', '0', 'Hibah Biro Umum', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (243, '024070199465983000KP', '3050202003', 'Jam Elektronik', '3', '1', '', '2004', '1', '375000', '244000', '-244000', NULL, NULL, '0', 'DIK 2004', '', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (244, '024070199465983000KP', '3050202003', 'Jam Elektronik', '4', '2', '', '2004', '1', '375000', '244000', '-244000', NULL, NULL, '0', 'DIK 2004', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (245, '024070199465983000KP', '3050204001', 'Lemari Es', '1', '1', 'Sanyo 1 Pintu', '2005', '1', '1870000', '1870000', '-1870000', NULL, NULL, '0', 'DIPA 2005', '', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (246, '024070199465983000KP', '3050204004', 'A.C. Split', '1', '1', 'LG', '2002', '1', '6904000', '3889000', '-3889000', NULL, NULL, '0', 'APBN 2002', '', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (247, '024070199465983000KP', '3050206002', 'Televisi', '1', '1', 'Sharp', '2005', '1', '4485000', '4485000', '-4485000', NULL, NULL, '0', 'DIPA 2005', '', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (248, '024070199465983000KP', '3050206002', 'Televisi', '2', '1', 'LG Standart', '2013', '1', '10450000', '10450000', NULL, '-10450000', NULL, '0', 'APBN 2013', '30810', 'Baru', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (249, '024070199465983000KP', '3050206002', 'Televisi', '3', '1', 'Samsung', '2014', '1', '8371000', '8371000', NULL, '-5022600', '-2511300', '837100', 'Sesditjen Farmalkes', 'BAPHP KN.01.01/4/0475/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (250, '024070199465983000KP', '3050206002', 'Televisi', '4', '1', 'Samsung 65\"', '2018', '1', '32717000', '32717000', NULL, '-3271700', NULL, '29445300', 'PT. Mitsindo Visual Pratama', 'KN.01.04/005/OLDAT.2/3506/2018;\'17-10-2018', 'Tv samsung di ruang direktur', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (251, '024070199465983000KP', '3050206002', 'Televisi', '5', '2', 'Samsung 65\"', '2018', '1', '32717000', '32717000', NULL, '-3271700', NULL, '29445300', 'PT. Mitsindo Visual Pratama', 'KN.01.04/005/OLDAT.2/3506/2018;\'17-10-2018', 'Tv samsung di ruang direktur', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (252, '024070199465983000KP', '3050206004', 'Tape Recorder (Alat Rumah Tangga Lainnya ( Home Use ))', '1', '1', 'Polytron', '2005', '1', '625000', '625000', '-625000', NULL, '0', '0', 'DIPA 2005', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (253, '024070199465983000KP', '3050206046', 'Handy Cam', '1', '1', 'Mini DV Kamera Recorder', '2006', '1', '43700000', '43700000', '-43700000', NULL, '0', '0', 'BAP barang (CV Citra Media', 'YF.01.01.IIA.023', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (254, '024070199465983000KP', '3050206046', 'Handy Cam', '2', '1', '', '2007', '1', '7700000', '7700000', '-7700000', NULL, NULL, '0', 'APBN 2007', 'SP2D 774039F TGL 09-', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (255, '024070199465983000KP', '3050206056', 'Karpet', '1', '1', 'Lokal', '2002', '1', '7266000', '5586000', '-5586000', NULL, NULL, '0', 'APBN 2002', '', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (256, '024070199465983000KP', '3050206056', 'Karpet', '2', '1', '', '2016', '1', '44179520', '44179520', NULL, '-26507712', NULL, '17671808', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (257, '024070199465983000KP', '3050206058', 'Gordyin/Kray', '1', '1', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (258, '024070199465983000KP', '3050206058', 'Gordyin/Kray', '10', '2', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (259, '024070199465983000KP', '3050206058', 'Gordyin/Kray', '11', '3', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (260, '024070199465983000KP', '3050206058', 'Gordyin/Kray', '12', '4', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (261, '024070199465983000KP', '3050206058', 'Gordyin/Kray', '13', '5', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (262, '024070199465983000KP', '3050206058', 'Gordyin/Kray', '14', '6', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (263, '024070199465983000KP', '3050206058', 'Gordyin/Kray', '15', '7', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (264, '024070199465983000KP', '3050206058', 'Gordyin/Kray', '16', '8', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (265, '024070199465983000KP', '3050206058', 'Gordyin/Kray', '17', '9', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (266, '024070199465983000KP', '3050206058', 'Gordyin/Kray', '18', '10', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (267, '024070199465983000KP', '3050206058', 'Gordyin/Kray', '19', '11', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (268, '024070199465983000KP', '3050206058', 'Gordyin/Kray', '2', '12', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (269, '024070199465983000KP', '3050206058', 'Gordyin/Kray', '20', '13', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (270, '024070199465983000KP', '3050206058', 'Gordyin/Kray', '21', '14', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (271, '024070199465983000KP', '3050206058', 'Gordyin/Kray', '22', '15', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (272, '024070199465983000KP', '3050206058', 'Gordyin/Kray', '23', '16', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (273, '024070199465983000KP', '3050206058', 'Gordyin/Kray', '24', '17', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (274, '024070199465983000KP', '3050206058', 'Gordyin/Kray', '25', '18', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (275, '024070199465983000KP', '3050206058', 'Gordyin/Kray', '26', '19', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (276, '024070199465983000KP', '3050206058', 'Gordyin/Kray', '27', '20', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (277, '024070199465983000KP', '3050206058', 'Gordyin/Kray', '28', '21', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (278, '024070199465983000KP', '3050206058', 'Gordyin/Kray', '29', '22', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (279, '024070199465983000KP', '3050206058', 'Gordyin/Kray', '3', '23', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (280, '024070199465983000KP', '3050206058', 'Gordyin/Kray', '30', '24', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (281, '024070199465983000KP', '3050206058', 'Gordyin/Kray', '31', '25', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (282, '024070199465983000KP', '3050206058', 'Gordyin/Kray', '4', '26', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (283, '024070199465983000KP', '3050206058', 'Gordyin/Kray', '5', '27', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (284, '024070199465983000KP', '3050206058', 'Gordyin/Kray', '6', '28', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (285, '024070199465983000KP', '3050206058', 'Gordyin/Kray', '7', '29', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (286, '024070199465983000KP', '3050206058', 'Gordyin/Kray', '8', '30', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (287, '024070199465983000KP', '3050206058', 'Gordyin/Kray', '9', '31', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (288, '024070199465983000KP', '3060102003', 'Camera Electronic', '1', '1', '', '2007', '1', '4700000', '4700000', '-4700000', NULL, '0', '0', 'APBN 2007', 'SP2D 774039F TGL 09-', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (289, '024070199465983000KP', '3060102012', 'Video Monitor', '1', '1', 'LG 75\" With VIA Campus KRAMER', '2018', '1', '165593862', '165593862', NULL, '-16559386', NULL, '149034476', 'PT. Mitsindo Visual Pratama', 'KN.01.04/005/VWALL/3505/2018', 'Video Wall', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (290, '024070199465983000KP', '3060201003', 'Pesawat Telephone', '1', '1', 'Toriphone', '2002', '1', '196800', '196800', '-196800', NULL, NULL, '0', 'APBN 2002', '', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (291, '024070199465983000KP', '3060201003', 'Pesawat Telephone', '2', '2', 'Toriphone', '2002', '1', '196800', '196800', '-196800', NULL, NULL, '0', 'APBN 2002', '', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (292, '024070199465983000KP', '3060201003', 'Pesawat Telephone', '3', '3', 'Toriphone', '2002', '1', '196800', '196800', '-196800', NULL, NULL, '0', 'APBN 2002', '', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (293, '024070199465983000KP', '3060201010', 'Facsimile', '1', '1', 'Panasonic', '2002', '1', '3850000', '1735000', '-1735000', NULL, NULL, '0', 'APBN 2002', '', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (294, '024070199465983000KP', '3070102032', 'Bracket Holder', '1', '1', 'Bracket Universal', '2009', '1', '1188000', '1188000', '-831600', '-356400', NULL, '0', 'PT. Complus Sistem Solusi', 'PL.01.01/3/168/2009', 'VGA output/input 15 M', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (295, '024070199465983000KP', '3080141251', 'Stabilizer/UPS', '1', '1', 'LAPLACE', '2010', '1', '1625000', '1625000', '-507813', '-1117189', '2', '0', 'APBN 2010', '118/F/DUKXI/2010', 'CV. DELPINDO UTAMA KARYA', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (296, '024070199465983000KP', '3080141251', 'Stabilizer/UPS', '2', '2', 'LAPLACE', '2010', '1', '1625000', '1625000', '-507813', '-1117189', '2', '0', 'APBN 2010', '118/F/DUKXI/2010', 'CV. DELPINDO UTAMA KARYA', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (297, '024070199465983000KP', '3080141251', 'Stabilizer/UPS', '3', '3', 'LAPLACE', '2010', '1', '1625000', '1625000', '-507813', '-1117189', '2', '0', 'APBN 2010', '118/F/DUKXI/2010', 'CV. DELPINDO UTAMA KARYA', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (298, '024070199465983000KP', '3080141251', 'Stabilizer/UPS', '4', '4', 'LAPLACE', '2010', '1', '1625000', '1625000', '-507813', '-1117189', '2', '0', 'APBN 2010', '118/F/DUKXI/2010', 'CV. DELPINDO UTAMA KARYA', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (299, '024070199465983000KP', '3080305002', 'Uninterupted Power Supply (UPS)', '1', '1', 'UPS', '2013', '1', '2520000', '2520000', NULL, '-1008000', NULL, '1512000', 'APBN 2013', 'CV. Harta Gemilang', 'CV. Harta Gemilang', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (300, '024070199465983000KP', '3090402031', 'Kamera Digital', '1', '1', 'NIKON', '2011', '1', '9992400', '9992400', '-3747150', '-6245250', NULL, '0', 'APBN', 'PL.01.01.05/LCD/13/IX/2011', 'Pembelian 2011', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (301, '024070199465983000KP', '3100102001', 'P.C Unit', '1', '1', 'Compaq', '2002', '1', '12348400', '5565000', '-5565000', NULL, NULL, '0', 'APBN 2002', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (302, '024070199465983000KP', '3100102001', 'P.C Unit', '10', '1', 'IBM', '2004', '1', '12430000', '6888000', '-6888000', NULL, NULL, '0', 'APBN 2004', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (303, '024070199465983000KP', '3100102001', 'P.C Unit', '11', '1', 'IBM', '2005', '1', '12005400', '12005400', '-12005400', NULL, '0', '0', 'DIPA 2005', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (304, '024070199465983000KP', '3100102001', 'P.C Unit', '12', '2', 'IBM', '2005', '1', '12005400', '12005400', '-12005400', NULL, '0', '0', 'DIPA 2005', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (305, '024070199465983000KP', '3100102001', 'P.C Unit', '13', '1', '', '2006', '1', '14750000', '14750000', '-14750000', NULL, NULL, '0', 'BAP barang (CV Bina Utama)', 'YF.01.01.IIA.042/03-', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (306, '024070199465983000KP', '3100102001', 'P.C Unit', '14', '2', '', '2006', '1', '8000000', '8000000', '-8000000', NULL, '0', '0', 'BAP barang (CV Citra Media', 'YF.01.02.IIA.025/30-', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (307, '024070199465983000KP', '3100102001', 'P.C Unit', '15', '3', '', '2006', '1', '8000000', '8000000', '-8000000', NULL, NULL, '0', 'BAP barang (CV Citra Media', 'YF.01.02.IIA.025/30-', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (308, '024070199465983000KP', '3100102001', 'P.C Unit', '16', '1', 'HP COMPAQ DX 2310', '2009', '1', '8419500', '8419500', '-7367063', '-1052437', '0', '0', 'PT. Complus Sistem Solusi', 'PL.01.01/3/168/2009', 'Inter Dual core E2200', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (309, '024070199465983000KP', '3100102001', 'P.C Unit', '17', '2', 'HP COMPAQ DX 2310', '2009', '1', '8419500', '8419500', '-7367063', '-1052437', '0', '0', 'PT. Complus Sistem Solusi', 'PL.01.01/3/168/2009', 'Inter Dual core E2200', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (310, '024070199465983000KP', '3100102001', 'P.C Unit', '18', '3', 'HP COMPAQ DX 2310', '2009', '1', '8419500', '8419500', '-7367063', '-1052437', '0', '0', 'PT. Complus Sistem Solusi', 'PL.01.01/3/168/2009', 'Inter Dual core E2200', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (311, '024070199465983000KP', '3100102001', 'P.C Unit', '19', '1', '200-5314D', '2011', '1', '8965000', '8965000', '-3361875', '-5603125', NULL, '0', 'APBN', 'pl.01.01.05/apbd/04/xi/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (312, '024070199465983000KP', '3100102001', 'P.C Unit', '2', '1', 'Compaq', '2002', '1', '12348400', '5565000', '-5565000', NULL, NULL, '0', 'APBN 2002', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (313, '024070199465983000KP', '3100102001', 'P.C Unit', '20', '1', '200-5314D', '2011', '1', '8965000', '8965000', '-3361875', '-5603125', NULL, '0', 'APBN', 'pl.01.01.05/apbd/04/xi/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (314, '024070199465983000KP', '3100102001', 'P.C Unit', '21', '2', '200-5314D', '2011', '1', '8965000', '8965000', '-3361875', '-5603125', NULL, '0', 'APBN', 'pl.01.01.05/apbd/04/xi/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (315, '024070199465983000KP', '3100102001', 'P.C Unit', '22', '3', '200-5314D', '2011', '1', '8965000', '8965000', '-3361875', '-5603125', NULL, '0', 'APBN', 'pl.01.01.05/apbd/04/xi/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (316, '024070199465983000KP', '3100102001', 'P.C Unit', '23', '4', '200-5314D', '2011', '1', '8965000', '8965000', '-3361875', '-5603125', NULL, '0', 'APBN', 'pl.01.01.05/apbd/04/xi/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (317, '024070199465983000KP', '3100102001', 'P.C Unit', '24', '5', '200-5314D', '2011', '1', '8965000', '8965000', '-3361875', '-5603125', NULL, '0', 'APBN', 'pl.01.01.05/apbd/04/xi/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (318, '024070199465983000KP', '3100102001', 'P.C Unit', '25', '6', '200-5314D', '2011', '1', '8965000', '8965000', '-3361875', '-5603125', NULL, '0', 'APBN', 'pl.01.01.05/apbd/04/xi/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (319, '024070199465983000KP', '3100102001', 'P.C Unit', '26', '7', '200-5314D', '2011', '1', '8965000', '8965000', '-3361875', '-5603125', NULL, '0', 'APBN', 'pl.01.01.05/apbd/04/xi/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (320, '024070199465983000KP', '3100102001', 'P.C Unit', '27', '8', '200-5314D', '2011', '1', '8965000', '8965000', '-3361875', '-5603125', NULL, '0', 'APBN', 'pl.01.01.05/apbd/04/xi/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (321, '024070199465983000KP', '3100102001', 'P.C Unit', '28', '9', '200-5314D', '2011', '1', '8965000', '8965000', '-3361875', '-5603125', NULL, '0', 'APBN', 'pl.01.01.05/apbd/04/xi/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (322, '024070199465983000KP', '3100102001', 'P.C Unit', '29', '10', '200-5314D', '2011', '1', '8965000', '8965000', '-3361875', '-5603125', NULL, '0', 'APBN', 'pl.01.01.05/apbd/04/xi/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (323, '024070199465983000KP', '3100102001', 'P.C Unit', '3', '1', 'Compaq', '2003', '1', '12348400', '5565000', '-5565000', NULL, '0', '0', 'APBN 2002', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (324, '024070199465983000KP', '3100102001', 'P.C Unit', '30', '1', '200-5314D', '2011', '1', '8965000', '8965000', '-3361875', '-5603125', NULL, '0', 'APBN', 'pl.01.01.05/apbd/04/xi/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (325, '024070199465983000KP', '3100102001', 'P.C Unit', '31', '2', '200-5314D', '2011', '1', '8965000', '8965000', '-3361875', '-5603125', NULL, '0', 'APBN', 'pl.01.01.05/apbd/04/xi/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (326, '024070199465983000KP', '3100102001', 'P.C Unit', '32', '3', '200-5314D', '2011', '1', '8965000', '8965000', '-3361875', '-5603125', NULL, '0', 'APBN', 'pl.01.01.05/apbd/04/xi/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (327, '024070199465983000KP', '3100102001', 'P.C Unit', '33', '4', '200-5314D', '2011', '1', '8965000', '8965000', '-3361875', '-5603125', NULL, '0', 'APBN', 'pl.01.01.05/apbd/04/xi/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (328, '024070199465983000KP', '3100102001', 'P.C Unit', '34', '1', 'PAVILION 20-B110D ALL IN', '2013', '1', '8226026', '8226026', NULL, '-8226027', '1', '0', 'APBN 13', 'BN.01.03/05/APD/10/2013', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (329, '024070199465983000KP', '3100102001', 'P.C Unit', '35', '2', 'PAVILION 20-B110D ALL IN', '2013', '1', '8226026', '8226026', NULL, '-8226027', '1', '0', 'APBN 13', 'BN.01.03/05/APD/10/2013', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (330, '024070199465983000KP', '3100102001', 'P.C Unit', '36', '3', 'PAVILION 20-B110D ALL IN', '2013', '1', '8226026', '8226026', NULL, '-8226027', '1', '0', 'APBN 13', 'BN.01.03/05/APD/10/2013', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (331, '024070199465983000KP', '3100102001', 'P.C Unit', '37', '4', 'PAVILION 20-B110D ALL IN', '2013', '1', '8226026', '8226026', NULL, '-8226027', '1', '0', 'APBN 13', 'BN.01.03/05/APD/10/2013', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (332, '024070199465983000KP', '3100102001', 'P.C Unit', '38', '5', 'PAVILION 20-B110D ALL IN', '2013', '1', '8226026', '8226026', NULL, '-8226027', '1', '0', 'APBN 13', 'BN.01.03/05/APD/10/2013', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (333, '024070199465983000KP', '3100102001', 'P.C Unit', '39', '6', 'PAVILION 20-B110D ALL IN', '2013', '1', '8226026', '8226026', NULL, '-8226027', '1', '0', 'APBN 13', 'BN.01.03/05/APD/10/2013', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (334, '024070199465983000KP', '3100102001', 'P.C Unit', '4', '1', 'Compaq', '2003', '1', '12348400', '5565000', '-5565000', NULL, '0', '0', 'APBN 2002', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (335, '024070199465983000KP', '3100102001', 'P.C Unit', '40', '1', 'PAVILION 20-B110D ALL IN', '2013', '1', '8226026', '8226026', NULL, '-8226027', '1', '0', 'APBN 13', 'BN.01.03/05/APD/10/2013', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (336, '024070199465983000KP', '3100102001', 'P.C Unit', '41', '2', 'PAVILION 20-B110D ALL IN', '2013', '1', '8226026', '8226026', NULL, '-8226027', '1', '0', 'APBN 13', 'BN.01.03/05/APD/10/2013', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (337, '024070199465983000KP', '3100102001', 'P.C Unit', '42', '3', 'PAVILION 20-B110D ALL IN', '2013', '1', '8226026', '8226026', NULL, '-8226027', '1', '0', 'APBN 13', 'BN.01.03/05/APD/10/2013', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (338, '024070199465983000KP', '3100102001', 'P.C Unit', '43', '4', 'PAVILION 20-B110D ALL IN', '2013', '1', '8226026', '8226026', NULL, '-8226027', '1', '0', 'APBN 13', 'BN.01.03/05/APD/10/2013', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (339, '024070199465983000KP', '3100102001', 'P.C Unit', '44', '5', 'PAVILION 20-B110D ALL IN', '2013', '1', '8226026', '8226026', NULL, '-8226027', '1', '0', 'APBN 13', 'BN.01.03/05/APD/10/2013', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (340, '024070199465983000KP', '3100102001', 'P.C Unit', '45', '6', 'PAVILION 20-B110D ALL IN', '2013', '1', '8226026', '8226026', NULL, '-8226027', '1', '0', 'APBN 13', 'BN.01.03/05/APD/10/2013', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (341, '024070199465983000KP', '3100102001', 'P.C Unit', '46', '1', 'HP PAVILION 20-a210d ALL-IN', '2014', '1', '8863250', '8863250', NULL, '-8863250', NULL, '0', 'CV.Cahaya Bukit Mas', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (342, '024070199465983000KP', '3100102001', 'P.C Unit', '47', '2', 'HP PAVILION 20-a210d ALL-IN', '2014', '1', '8863250', '8863250', NULL, '-8863250', NULL, '0', 'CV.Cahaya Bukit Mas', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (343, '024070199465983000KP', '3100102001', 'P.C Unit', '48', '3', 'HP PAVILION 20-a210d ALL-IN', '2014', '1', '8863250', '8863250', NULL, '-8863250', NULL, '0', 'CV.Cahaya Bukit Mas', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (344, '024070199465983000KP', '3100102001', 'P.C Unit', '49', '4', 'HP PAVILION 20-a210d ALL-IN', '2014', '1', '8863250', '8863250', NULL, '-8863250', NULL, '0', 'CV.Cahaya Bukit Mas', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (345, '024070199465983000KP', '3100102001', 'P.C Unit', '5', '1', 'Compaq', '2003', '1', '12348400', '5565000', '-5565000', NULL, '0', '0', 'APBN 2002', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (346, '024070199465983000KP', '3100102001', 'P.C Unit', '50', '1', 'HP PAVILION 20-a210d ALL-IN', '2014', '1', '8863250', '8863250', NULL, '-8863250', NULL, '0', 'CV.Cahaya Bukit Mas', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (347, '024070199465983000KP', '3100102001', 'P.C Unit', '51', '2', 'HP PAVILION 20-a210d ALL-IN', '2014', '1', '8863250', '8863250', NULL, '-8863250', NULL, '0', 'CV.Cahaya Bukit Mas', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (348, '024070199465983000KP', '3100102001', 'P.C Unit', '52', '3', 'HP PAVILION 20-a210d ALL-IN', '2014', '1', '8863250', '8863250', NULL, '-8863250', NULL, '0', 'CV.Cahaya Bukit Mas', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (349, '024070199465983000KP', '3100102001', 'P.C Unit', '53', '4', 'HP PAVILION 20-a210d ALL-IN', '2014', '1', '8863250', '8863250', NULL, '-8863250', NULL, '0', 'CV.Cahaya Bukit Mas', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (350, '024070199465983000KP', '3100102001', 'P.C Unit', '54', '5', 'HP PAVILION 20-a210d ALL-IN', '2014', '1', '8863250', '8863250', NULL, '-8863250', NULL, '0', 'CV.Cahaya Bukit Mas', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (351, '024070199465983000KP', '3100102001', 'P.C Unit', '55', '6', 'HP PAVILION 20-a210d ALL-IN', '2014', '1', '8863250', '8863250', NULL, '-8863250', NULL, '0', 'CV.Cahaya Bukit Mas', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (352, '024070199465983000KP', '3100102001', 'P.C Unit', '6', '1', 'Compaq', '2003', '1', '12348400', '5565000', '-5565000', NULL, '0', '0', 'APBN 2002', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (353, '024070199465983000KP', '3100102001', 'P.C Unit', '66', '1', 'HP-ALL IN ONE 20-r023l', '2015', '1', '9498500', '9498500', NULL, '-8311189', '1', '1187312', 'CV. ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (354, '024070199465983000KP', '3100102001', 'P.C Unit', '67', '2', 'HP-ALL IN ONE 20-r023l', '2015', '1', '9498500', '9498500', NULL, '-8311189', '1', '1187312', 'CV. ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (355, '024070199465983000KP', '3100102001', 'P.C Unit', '68', '3', 'HP-ALL IN ONE 20-r023l', '2015', '1', '9498500', '9498500', NULL, '-8311189', '1', '1187312', 'CV. ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (356, '024070199465983000KP', '3100102001', 'P.C Unit', '69', '4', 'HP-ALL IN ONE 20-r023l', '2015', '1', '9498500', '9498500', NULL, '-8311189', '1', '1187312', 'CV. ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (357, '024070199465983000KP', '3100102001', 'P.C Unit', '7', '1', 'Compaq', '2003', '1', '12348400', '5565000', '-5565000', NULL, '0', '0', 'APBN 2002', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (358, '024070199465983000KP', '3100102001', 'P.C Unit', '70', '1', 'HP-ALL IN ONE 20-r023l', '2015', '1', '9498500', '9498500', NULL, '-8311189', '1', '1187312', 'CV. ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (359, '024070199465983000KP', '3100102001', 'P.C Unit', '71', '2', 'HP-ALL IN ONE 20-r023l', '2015', '1', '9498500', '9498500', NULL, '-8311189', '1', '1187312', 'CV. ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (360, '024070199465983000KP', '3100102001', 'P.C Unit', '72', '3', 'HP-ALL IN ONE 20-r023l', '2015', '1', '9498500', '9498500', NULL, '-8311189', '1', '1187312', 'CV. ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (361, '024070199465983000KP', '3100102001', 'P.C Unit', '73', '4', 'HP-ALL IN ONE 20-r023l', '2015', '1', '9498500', '9498500', NULL, '-8311189', '1', '1187312', 'CV. ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (362, '024070199465983000KP', '3100102001', 'P.C Unit', '74', '5', 'HP-ALL IN ONE 20-r023l', '2015', '1', '9498500', '9498500', NULL, '-8311189', '1', '1187312', 'CV. ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (363, '024070199465983000KP', '3100102001', 'P.C Unit', '75', '6', 'HP-ALL IN ONE 20-r023l', '2015', '1', '9498500', '9498500', NULL, '-8311189', '1', '1187312', 'CV. ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (364, '024070199465983000KP', '3100102001', 'P.C Unit', '76', '1', 'Lenovo Think Centre E93-YIF', '2014', '1', '14348500', '14348500', NULL, '-7174251', '-7174249', '0', 'Sesditjen Farmalkes', 'KN.01.01/PK.03/109.2/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (365, '024070199465983000KP', '3100102001', 'P.C Unit', '77', '1', 'ASUS PC. ALL In One', '2016', '1', '14132940', '14132940', NULL, '-8833088', NULL, '5299852', 'PT.AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (366, '024070199465983000KP', '3100102001', 'P.C Unit', '78', '2', 'ASUS PC. ALL In One', '2016', '1', '14132940', '14132940', NULL, '-8833088', NULL, '5299852', 'PT.AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (367, '024070199465983000KP', '3100102001', 'P.C Unit', '79', '3', 'ASUS PC. ALL In One', '2016', '1', '14132940', '14132940', NULL, '-8833088', NULL, '5299852', 'PT.AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (368, '024070199465983000KP', '3100102001', 'P.C Unit', '8', '1', 'Compaq', '2003', '1', '12350000', '6236000', '-6236000', NULL, '0', '0', 'DIP Bagpro POR 03', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (369, '024070199465983000KP', '3100102001', 'P.C Unit', '80', '1', 'ASUS PC. ALL In One', '2016', '1', '14132940', '14132940', NULL, '-8833088', NULL, '5299852', 'PT.AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (370, '024070199465983000KP', '3100102001', 'P.C Unit', '81', '2', 'ASUS PC. ALL In One', '2016', '1', '14132940', '14132940', NULL, '-8833088', NULL, '5299852', 'PT.AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (371, '024070199465983000KP', '3100102001', 'P.C Unit', '82', '3', 'ASUS PC. ALL In One', '2016', '1', '14132940', '14132940', NULL, '-8833088', NULL, '5299852', 'PT.AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (372, '024070199465983000KP', '3100102001', 'P.C Unit', '83', '4', 'ASUS PC. ALL In One', '2016', '1', '14132940', '14132940', NULL, '-8833088', NULL, '5299852', 'PT.AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (373, '024070199465983000KP', '3100102001', 'P.C Unit', '84', '5', 'ASUS PC. ALL In One', '2016', '1', '14132940', '14132940', NULL, '-8833088', NULL, '5299852', 'PT.AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (374, '024070199465983000KP', '3100102001', 'P.C Unit', '85', '6', 'ASUS PC. ALL In One', '2016', '1', '14132940', '14132940', NULL, '-8833088', NULL, '5299852', 'PT.AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (375, '024070199465983000KP', '3100102001', 'P.C Unit', '86', '7', 'ASUS PC. ALL In One', '2016', '1', '14132940', '14132940', NULL, '-8833088', NULL, '5299852', 'PT.AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (376, '024070199465983000KP', '3100102001', 'P.C Unit', '9', '1', 'IBM', '2004', '1', '12430000', '6888000', '-6888000', NULL, NULL, '0', 'APBN 2004', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (377, '024070199465983000KP', '3100102001', 'P.C Unit', '93', '1', 'Lenovo', '2018', '1', '15819100', '15819100', NULL, '-1977388', NULL, '13841712', 'PT. Air Mas Perkasa', 'KN.01.04/I005/PC-AIO/09/2018', 'Pengadaan Desktop All In One', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (378, '024070199465983000KP', '3100102001', 'P.C Unit', '94', '2', 'Lenovo', '2018', '1', '15819100', '15819100', NULL, '-1977388', NULL, '13841712', 'PT. Air Mas Perkasa', 'KN.01.04/I005/PC-AIO/09/2018', 'Pengadaan Desktop All In One', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (379, '024070199465983000KP', '3100102001', 'P.C Unit', '95', '3', 'Lenovo', '2018', '1', '15819100', '15819100', NULL, '-1977388', NULL, '13841712', 'PT. Air Mas Perkasa', 'KN.01.04/I005/PC-AIO/09/2018', 'Pengadaan Desktop All In One', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (380, '024070199465983000KP', '3100102001', 'P.C Unit', '96', '4', 'Lenovo', '2018', '1', '15819100', '15819100', NULL, '-1977388', NULL, '13841712', 'PT. Air Mas Perkasa', 'KN.01.04/I005/PC-AIO/09/2018', 'Pengadaan Desktop All In One', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (381, '024070199465983000KP', '3100102002', 'Lap Top', '1', '1', '', '2007', '1', '19500000', '19500000', '-19500000', NULL, NULL, '0', 'APBN 2007', 'SP2D 774039 TGL09-04', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (382, '024070199465983000KP', '3100102002', 'Lap Top', '10', '1', 'HP ELITE BOOK', '2017', '1', '18346130', '18346130', NULL, '-6879798', NULL, '11466332', '-', '-', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (383, '024070199465983000KP', '3100102002', 'Lap Top', '11', '2', 'HP ELITE BOOK', '2017', '1', '18346130', '18346130', NULL, '-6879798', NULL, '11466332', '-', '-', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (384, '024070199465983000KP', '3100102002', 'Lap Top', '12', '3', 'HP ELITE BOOK', '2017', '1', '18346130', '18346130', NULL, '-6879798', NULL, '11466332', '-', '-', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (385, '024070199465983000KP', '3100102002', 'Lap Top', '13', '4', 'HP ELITE BOOK', '2017', '1', '18346130', '18346130', NULL, '-6879798', NULL, '11466332', '-', '-', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (386, '024070199465983000KP', '3100102002', 'Lap Top', '14', '5', 'HP ELITE BOOK', '2017', '1', '18346130', '18346130', NULL, '-6879798', NULL, '11466332', '-', '-', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (387, '024070199465983000KP', '3100102002', 'Lap Top', '15', '6', 'HP ELITE BOOK', '2017', '1', '18346130', '18346130', NULL, '-6879798', NULL, '11466332', '-', '-', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (388, '024070199465983000KP', '3100102002', 'Lap Top', '16', '7', 'HP ELITE BOOK', '2017', '1', '18346130', '18346130', NULL, '-6879798', NULL, '11466332', '-', '-', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (389, '024070199465983000KP', '3100102002', 'Lap Top', '17', '8', 'HP ELITE BOOK', '2017', '1', '18346130', '18346130', NULL, '-6879798', NULL, '11466332', '-', '-', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (390, '024070199465983000KP', '3100102002', 'Lap Top', '18', '9', 'HP ELITE BOOK', '2017', '1', '18346130', '18346130', NULL, '-6879798', NULL, '11466332', '-', '-', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (391, '024070199465983000KP', '3100102002', 'Lap Top', '19', '10', 'HP ELITE BOOK', '2017', '1', '18346130', '18346130', NULL, '-6879798', NULL, '11466332', '-', '-', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (392, '024070199465983000KP', '3100102002', 'Lap Top', '2', '1', '', '2007', '1', '19800000', '19800000', '-19800000', NULL, NULL, '0', 'APBN 2007', 'SP2D774038F TGL 09-0', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (393, '024070199465983000KP', '3100102002', 'Lap Top', '20', '1', 'HP ELITE BOOK', '2017', '1', '18346130', '18346130', NULL, '-6879798', NULL, '11466332', '-', '-', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (394, '024070199465983000KP', '3100102002', 'Lap Top', '21', '2', 'HP ELITE BOOK', '2017', '1', '18346130', '18346130', NULL, '-6879798', NULL, '11466332', '-', '-', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (395, '024070199465983000KP', '3100102002', 'Lap Top', '3', '1', '', '2007', '1', '19800000', '19800000', '-19800000', NULL, NULL, '0', 'APBN 2007', 'SP2D774038F TGL 09-0', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (396, '024070199465983000KP', '3100102002', 'Lap Top', '4', '1', 'VAIO TAB 11', '2014', '1', '10829500', '10829500', NULL, '-10829502', '2', '0', 'CV.CAHAYA BUKIT MAS', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (397, '024070199465983000KP', '3100102002', 'Lap Top', '5', '2', 'VAIO TAB 11', '2014', '1', '10829500', '10829500', NULL, '-10829502', '2', '0', 'CV.CAHAYA BUKIT MAS', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (398, '024070199465983000KP', '3100102002', 'Lap Top', '6', '3', 'VAIO TAB 11', '2014', '1', '10829500', '10829500', NULL, '-10829502', '2', '0', 'CV.CAHAYA BUKIT MAS', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (399, '024070199465983000KP', '3100102002', 'Lap Top', '7', '1', 'HP ELITE BOOK', '2017', '1', '18346130', '18346130', NULL, '-6879798', NULL, '11466332', '-', '-', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (400, '024070199465983000KP', '3100102002', 'Lap Top', '8', '2', 'HP ELITE BOOK', '2017', '1', '18346130', '18346130', NULL, '-6879798', NULL, '11466332', '-', '-', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (401, '024070199465983000KP', '3100102002', 'Lap Top', '9', '3', 'HP ELITE BOOK', '2017', '1', '18346130', '18346130', NULL, '-6879798', NULL, '11466332', '-', '-', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (402, '024070199465983000KP', '3100102003', 'Note Book', '1', '1', '', '2006', '1', '17800000', '17800000', '-17800000', NULL, '0', '0', 'BAP barang (CV Citra Media', 'YF.01.01.IIA.025/30-', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (403, '024070199465983000KP', '3100102003', 'Note Book', '10', '1', 'Fujitsu', '2010', '1', '14045000', '14045000', '-8778125', '-5266875', NULL, '0', 'APBN 2010', '118/F/DUKXI/2010', 'CV Delpindo Utama Karya', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (404, '024070199465983000KP', '3100102003', 'Note Book', '11', '2', 'Fujitsu', '2010', '1', '14045000', '14045000', '-8778125', '-5266875', NULL, '0', 'APBN 2010', '118/F/DUKXI/2010', 'CV Delpindo Utama Karya', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (405, '024070199465983000KP', '3100102003', 'Note Book', '12', '3', 'Fujitsu', '2010', '1', '14045000', '14045000', '-8778125', '-5266875', NULL, '0', 'APBN 2010', '118/F/DUKXI/2010', 'CV Delpindo Utama Karya', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (406, '024070199465983000KP', '3100102003', 'Note Book', '13', '4', 'Fujitsu', '2010', '1', '14045000', '14045000', '-8778125', '-5266875', NULL, '0', 'APBN 2010', '118/F/DUKXI/2010', 'CV Delpindo Utama Karya', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (407, '024070199465983000KP', '3100102003', 'Note Book', '14', '5', 'Fujitsu', '2010', '1', '14045000', '14045000', '-8778125', '-5266875', NULL, '0', 'APBN 2010', '118/F/DUKXI/2010', 'CV Delpindo Utama Karya', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (408, '024070199465983000KP', '3100102003', 'Note Book', '15', '1', 'fUJITSU', '2011', '1', '15345000', '15345000', '-5754375', '-9590625', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (409, '024070199465983000KP', '3100102003', 'Note Book', '16', '2', 'fUJITSU', '2011', '1', '15345000', '15345000', '-5754375', '-9590625', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (410, '024070199465983000KP', '3100102003', 'Note Book', '17', '3', 'fUJITSU', '2011', '1', '15345000', '15345000', '-5754375', '-9590625', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (411, '024070199465983000KP', '3100102003', 'Note Book', '18', '4', 'fUJITSU', '2011', '1', '15345000', '15345000', '-5754375', '-9590625', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (412, '024070199465983000KP', '3100102003', 'Note Book', '19', '5', 'fUJITSU', '2011', '1', '15345000', '15345000', '-5754375', '-9590625', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (413, '024070199465983000KP', '3100102003', 'Note Book', '2', '1', 'ACER ASP 2930', '2008', '1', '14280752', '14280752', '-14280752', NULL, NULL, '0', 'CV. DELPINDO UTAMA KARYA', 'PL.01.01/3/275/2008', 'CORE DUO T5800', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (414, '024070199465983000KP', '3100102003', 'Note Book', '20', '1', 'fUJITSU', '2011', '1', '15345000', '15345000', '-5754375', '-9590625', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (415, '024070199465983000KP', '3100102003', 'Note Book', '21', '1', 'fujitsu 380 black', '2013', '1', '14264542', '14264542', NULL, '-14264543', '1', '0', 'APBN 13', 'BN.01.03/05/APD/10/2013', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (416, '024070199465983000KP', '3100102003', 'Note Book', '22', '2', 'fujitsu 380 black', '2013', '1', '14264542', '14264542', NULL, '-14264543', '1', '0', 'APBN 13', 'BN.01.03/05/APD/10/2013', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (417, '024070199465983000KP', '3100102003', 'Note Book', '23', '3', 'fujitsu 380 black', '2013', '1', '14264542', '14264542', NULL, '-14264543', '1', '0', 'APBN 13', 'BN.01.03/05/APD/10/2013', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (418, '024070199465983000KP', '3100102003', 'Note Book', '24', '4', 'fujitsu 380 black', '2013', '1', '14264542', '14264542', NULL, '-14264543', '1', '0', 'APBN 13', 'BN.01.03/05/APD/10/2013', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (419, '024070199465983000KP', '3100102003', 'Note Book', '25', '5', 'fujitsu 380 black', '2013', '1', '14264542', '14264542', NULL, '-14264543', '1', '0', 'APBN 13', 'BN.01.03/05/APD/10/2013', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (420, '024070199465983000KP', '3100102003', 'Note Book', '26', '1', 'LENOVO YOGA 500-14IBD', '2015', '1', '12098790', '12098790', NULL, '-10586443', '2', '1512349', 'CV. ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (421, '024070199465983000KP', '3100102003', 'Note Book', '27', '2', 'LENOVO YOGA 500-14IBD', '2015', '1', '12098790', '12098790', NULL, '-10586443', '2', '1512349', 'CV. ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (422, '024070199465983000KP', '3100102003', 'Note Book', '28', '3', 'LENOVO YOGA 500-14IBD', '2015', '1', '12098790', '12098790', NULL, '-10586443', '2', '1512349', 'CV. ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (423, '024070199465983000KP', '3100102003', 'Note Book', '29', '4', 'LENOVO YOGA 500-14IBD', '2015', '1', '12098790', '12098790', NULL, '-10586443', '2', '1512349', 'CV. ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (424, '024070199465983000KP', '3100102003', 'Note Book', '3', '1', 'ACER ASP 2930', '2008', '1', '14280752', '14280752', '-14280752', NULL, NULL, '0', 'CV. DELPINDO UTAMA KARYA', 'PL.01.01/3/275/2008', 'CORE DUO T5800', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (425, '024070199465983000KP', '3100102003', 'Note Book', '30', '1', 'LENOVO YOGA 500-14IBD', '2015', '1', '12098790', '12098790', NULL, '-10586443', '2', '1512349', 'CV. ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (426, '024070199465983000KP', '3100102003', 'Note Book', '31', '1', 'Apple Macbook Air', '2016', '1', '16973000', '16973000', NULL, '-2121625', '-8486500', '6364875', 'Setditjen Farmalkes', 'BAPP KN.01.01/1/0625.5/2016', 'Barang Direktur', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (427, '024070199465983000KP', '3100102003', 'Note Book', '32', '1', 'Dell 7373-i78550-8-256-U W10P', '2018', '1', '20291875', '20291875', NULL, '-2536484', NULL, '17755391', 'Setditjen Farmalkes', 'BAST KN.01.02/PT.I/LAPTOP/030/2018', 'Barang Direktur', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (428, '024070199465983000KP', '3100102003', 'Note Book', '4', '1', 'ACER ASP 2930', '2008', '1', '14280752', '14280752', '-14280752', NULL, NULL, '0', 'CV. DELPINDO UTAMA KARYA', 'PL.01.01/3/275/2008', 'CORE DUO T5800', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (429, '024070199465983000KP', '3100102003', 'Note Book', '5', '2', 'SONY VAIO VGN TZ 160N/B', '2008', '1', '21720000', '21720000', '-21720000', NULL, NULL, '0', 'CV. DELPINDO UTAMA KARYA', 'PL.01.01/3/275/2008', 'CORE 2 DUO U7500', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (430, '024070199465983000KP', '3100102003', 'Note Book', '6', '1', 'ACER 6293', '2009', '1', '13097700', '13097700', '-11460488', '-1637212', '0', '0', 'PT. Complus Sistem Solusi', 'PL.01.01/3/168/2009', 'Intel Core 2 Duo Processor', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (431, '024070199465983000KP', '3100102003', 'Note Book', '7', '2', 'ACER 6293', '2009', '1', '13097700', '13097700', '-11460488', '-1637212', '0', '0', 'PT. Complus Sistem Solusi', 'PL.01.01/3/168/2009', 'Intel Core 2 Duo Processor', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (432, '024070199465983000KP', '3100102003', 'Note Book', '8', '3', 'ACER 6293', '2009', '1', '13097700', '13097700', '-11460488', '-1637212', '0', '0', 'PT. Complus Sistem Solusi', 'PL.01.01/3/168/2009', 'Intel Core 2 Duo Processor', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (433, '024070199465983000KP', '3100102003', 'Note Book', '9', '4', 'ACER 6293', '2009', '1', '13097700', '13097700', '-11460488', '-1637212', '0', '0', 'PT. Complus Sistem Solusi', 'PL.01.01/3/168/2009', 'Intel Core 2 Duo Processor', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (434, '024070199465983000KP', '3100102009', 'Tablet PC', '1', '1', 'Samsung Galaxy Tab S3 9,7\"', '2018', '1', '10629400', '10629400', NULL, '-2657350', NULL, '7972050', 'APBN', 'KN.01.04/IV/005.05/TAB/1348/2018', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (435, '024070199465983000KP', '3100102009', 'Tablet PC', '10', '2', 'Samsung Galaxy Tab S3 9,7\"', '2018', '1', '10629400', '10629400', NULL, '-2657350', NULL, '7972050', 'APBN', 'KN.01.04/IV/005.05/TAB/1348/2018', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (436, '024070199465983000KP', '3100102009', 'Tablet PC', '11', '3', 'Samsung Galaxy Tab S3 9,7\"', '2018', '1', '10629400', '10629400', NULL, '-2657350', NULL, '7972050', 'APBN', 'KN.01.04/IV/005.05/TAB/1348/2018', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (437, '024070199465983000KP', '3100102009', 'Tablet PC', '12', '4', 'Samsung Galaxy Tab S3 9,7\"', '2018', '1', '10629400', '10629400', NULL, '-2657350', NULL, '7972050', 'APBN', 'KN.01.04/IV/005.05/TAB/1348/2018', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (438, '024070199465983000KP', '3100102009', 'Tablet PC', '13', '5', 'Samsung Galaxy Tab S3 9,7\"', '2018', '1', '10629400', '10629400', NULL, '-2657350', NULL, '7972050', 'APBN', 'KN.01.04/IV/005.05/TAB/1348/2018', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (439, '024070199465983000KP', '3100102009', 'Tablet PC', '14', '6', 'Samsung Galaxy Tab S3 9,7\"', '2018', '1', '10629400', '10629400', NULL, '-2657350', NULL, '7972050', 'APBN', 'KN.01.04/IV/005.05/TAB/1348/2018', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (440, '024070199465983000KP', '3100102009', 'Tablet PC', '15', '7', 'Samsung Galaxy Tab S3 9,7\"', '2018', '1', '10629400', '10629400', NULL, '-2657350', NULL, '7972050', 'APBN', 'KN.01.04/IV/005.05/TAB/1348/2018', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (441, '024070199465983000KP', '3100102009', 'Tablet PC', '16', '8', 'Samsung Galaxy Tab S3 9,7\"', '2018', '1', '10629400', '10629400', NULL, '-2657350', NULL, '7972050', 'APBN', 'KN.01.04/IV/005.05/TAB/1348/2018', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (442, '024070199465983000KP', '3100102009', 'Tablet PC', '17', '9', 'Samsung Galaxy Tab S3 9,7\"', '2018', '1', '10629400', '10629400', NULL, '-2657350', NULL, '7972050', 'APBN', 'KN.01.04/IV/005.05/TAB/1348/2018', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (443, '024070199465983000KP', '3100102009', 'Tablet PC', '18', '10', 'Samsung Galaxy Tab S3 9,7\"', '2018', '1', '10629400', '10629400', NULL, '-2657350', NULL, '7972050', 'APBN', 'KN.01.04/IV/005.05/TAB/1348/2018', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (444, '024070199465983000KP', '3100102009', 'Tablet PC', '19', '11', 'Samsung Galaxy Tab S3', '2018', '1', '10500000', '10500000', NULL, '-1312500', NULL, '9187500', 'Setditjen Farmalkes', 'BAST KN.01.02/PT.I/TABS3/026/2018', 'Barang Direktur', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (445, '024070199465983000KP', '3100102009', 'Tablet PC', '2', '12', 'Samsung Galaxy Tab S3 9,7\"', '2018', '1', '10629400', '10629400', NULL, '-2657350', NULL, '7972050', 'APBN', 'KN.01.04/IV/005.05/TAB/1348/2018', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (446, '024070199465983000KP', '3100102009', 'Tablet PC', '3', '13', 'Samsung Galaxy Tab S3 9,7\"', '2018', '1', '10629400', '10629400', NULL, '-2657350', NULL, '7972050', 'APBN', 'KN.01.04/IV/005.05/TAB/1348/2018', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (447, '024070199465983000KP', '3100102009', 'Tablet PC', '4', '14', 'Samsung Galaxy Tab S3 9,7\"', '2018', '1', '10629400', '10629400', NULL, '-2657350', NULL, '7972050', 'APBN', 'KN.01.04/IV/005.05/TAB/1348/2018', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (448, '024070199465983000KP', '3100102009', 'Tablet PC', '5', '15', 'Samsung Galaxy Tab S3 9,7\"', '2018', '1', '10629400', '10629400', NULL, '-2657350', NULL, '7972050', 'APBN', 'KN.01.04/IV/005.05/TAB/1348/2018', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (449, '024070199465983000KP', '3100102009', 'Tablet PC', '6', '16', 'Samsung Galaxy Tab S3 9,7\"', '2018', '1', '10629400', '10629400', NULL, '-2657350', NULL, '7972050', 'APBN', 'KN.01.04/IV/005.05/TAB/1348/2018', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (450, '024070199465983000KP', '3100102009', 'Tablet PC', '7', '17', 'Samsung Galaxy Tab S3 9,7\"', '2018', '1', '10629400', '10629400', NULL, '-2657350', NULL, '7972050', 'APBN', 'KN.01.04/IV/005.05/TAB/1348/2018', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (451, '024070199465983000KP', '3100102009', 'Tablet PC', '8', '18', 'Samsung Galaxy Tab S3 9,7\"', '2018', '1', '10629400', '10629400', NULL, '-2657350', NULL, '7972050', 'APBN', 'KN.01.04/IV/005.05/TAB/1348/2018', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (452, '024070199465983000KP', '3100102009', 'Tablet PC', '9', '19', 'Samsung Galaxy Tab S3 9,7\"', '2018', '1', '10629400', '10629400', NULL, '-2657350', NULL, '7972050', 'APBN', 'KN.01.04/IV/005.05/TAB/1348/2018', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (453, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '1', '1', 'HP Laserjet 1000', '2002', '1', '3723920', '1679000', '-1679000', NULL, NULL, '0', 'APBN 2002', '', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (454, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '10', '1', 'HP LaserJet P1006', '2009', '1', '1389960', '1389960', '-1216215', '-173745', NULL, '0', 'PT. Complus Sistem Solusi', 'PL.01.01/3/168/2009', 'Memory 8 MB RAM', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (455, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '100', '1', 'HP LJM203', '2018', '1', '3325100', '3325100', NULL, '-415638', NULL, '2909462', 'PT. Air Mas Perkasa', 'KN.01.04/IV/005/OLDAT.1/3504/2018', 'Alat Pendukung Perkantoran Paket PKM-P1809-1199673', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (456, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '101', '2', 'HP LJM203', '2018', '1', '3325100', '3325100', NULL, '-415638', NULL, '2909462', 'PT. Air Mas Perkasa', 'KN.01.04/IV/005/OLDAT.1/3504/2018', 'Alat Pendukung Perkantoran Paket PKM-P1809-1199673', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (457, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '11', '1', 'HP LaserJet P1006', '2009', '1', '1389960', '1389960', '-1216215', '-173745', NULL, '0', 'PT. Complus Sistem Solusi', 'PL.01.01/3/168/2009', 'Memory 8 MB RAM', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (458, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '12', '2', 'HP OfficeJet 470B', '2009', '1', '2844072', '2844072', '-2488563', '-355509', NULL, '0', 'PT. Complus Sistem Solusi', 'PL.01.01/3/168/2009', 'Portable Printer', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (459, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '13', '3', 'HP M 1319F MFP', '2009', '1', '2681910', '2681910', '-2346671', '-335239', '0', '0', 'PT. Complus Sistem Solusi', 'PL.01.01/3/168/2009', 'Printer Multifungsi', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (460, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '14', '1', 'HP Portable printer', '2010', '1', '3255000', '3255000', '-2034375', '-1220625', NULL, '0', 'APBN 2010', '118/F/DUKXI/2010', 'CV. Delpindo Utama Karya', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (461, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '15', '2', 'HP Portable printer', '2010', '1', '3255000', '3255000', '-2034375', '-1220625', NULL, '0', 'APBN 2010', '118/F/DUKXI/2010', 'CV. Delpindo Utama Karya', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (462, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '16', '3', 'HP multifungsi', '2010', '1', '4200000', '4200000', '-2625000', '-1575000', NULL, '0', 'APBN 2010', '118/F/DUKXI/2010', 'CV Delpindo Utama Karya', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (463, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '17', '4', 'HP 1102', '2010', '1', '2850000', '2850000', '-1781250', '-1068750', NULL, '0', 'APBN 2010', '118/F/DUKXI/2010', 'CV Delpindo Utama Karya', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (464, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '18', '5', 'HP 1102', '2010', '1', '2850000', '2850000', '-1781250', '-1068750', NULL, '0', 'APBN 2010', '118/F/DUKXI/2010', 'CV Delpindo Utama Karya', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (465, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '19', '6', 'HP COLOUR PRINTER', '2010', '1', '3205000', '3205000', '-2003125', '-1201875', NULL, '0', 'APBN 2010', '118/F/DUKXI/2010', 'BUSINESS INKJET', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (466, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '2', '1', 'HP Laserjet 1000', '2002', '1', '3723920', '1679000', '-1679000', NULL, NULL, '0', 'APBN 2002', '', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (467, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '20', '1', 'HP COLOUR PRINTER', '2010', '1', '3205000', '3205000', '-2003125', '-1201875', NULL, '0', 'APBN 2010', '118/F/DUKXI/2010', 'BUSINESS INKJET', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (468, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '21', '2', 'HP COLOUR PRINTER', '2010', '1', '3205000', '3205000', '-2003125', '-1201875', NULL, '0', 'APBN 2010', '118/F/DUKXI/2010', 'BUSINESS INKJET', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (469, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '22', '1', 'hp.L.m1132', '2011', '1', '1925000', '1925000', '-721875', '-1203125', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (470, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '23', '2', 'hp.L.m1132', '2011', '1', '1925000', '1925000', '-721875', '-1203125', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (471, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '24', '3', 'hp.L.m1132', '2011', '1', '1925000', '1925000', '-721875', '-1203125', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (472, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '25', '4', 'hp.L.m1132', '2011', '1', '1925000', '1925000', '-721875', '-1203125', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (473, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '26', '5', 'hp.L.m1132', '2011', '1', '1925000', '1925000', '-721875', '-1203125', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (474, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '27', '6', 'P1102', '2011', '1', '1155000', '1155000', '-433125', '-721875', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (475, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '28', '7', 'P1102', '2011', '1', '1155000', '1155000', '-433125', '-721875', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (476, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '29', '8', 'P1102', '2011', '1', '1155000', '1155000', '-433125', '-721875', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (477, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '3', '1', 'Epson LQ 2180', '2002', '1', '7534400', '957000', '-957000', NULL, NULL, '0', 'APBN 2002', '', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (478, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '30', '1', 'P1102', '2011', '1', '1155000', '1155000', '-433125', '-721875', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (479, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '31', '2', 'P1102', '2011', '1', '1155000', '1155000', '-433125', '-721875', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (480, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '32', '3', 'P1102', '2011', '1', '1155000', '1155000', '-433125', '-721875', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (481, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '33', '4', 'P1102', '2011', '1', '1155000', '1155000', '-433125', '-721875', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (482, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '34', '5', 'P1102', '2011', '1', '1155000', '1155000', '-433125', '-721875', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (483, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '35', '6', 'P1102', '2011', '1', '1155000', '1155000', '-433125', '-721875', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (484, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '36', '7', 'P1102', '2011', '1', '1155000', '1155000', '-433125', '-721875', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (485, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '37', '8', 'P1102', '2011', '1', '1155000', '1155000', '-433125', '-721875', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (486, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '38', '9', 'P1102', '2011', '1', '1155000', '1155000', '-433125', '-721875', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (487, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '39', '10', 'P1102', '2011', '1', '1155000', '1155000', '-433125', '-721875', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (488, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '4', '1', 'HP Ink jet', '2002', '1', '2844800', '1282000', '-1282000', NULL, NULL, '0', 'APBN 2002', '', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (489, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '40', '1', 'P1102', '2011', '1', '1155000', '1155000', '-433125', '-721875', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (490, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '41', '2', 'P1102', '2011', '1', '1155000', '1155000', '-433125', '-721875', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (491, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '42', '3', 'P1102', '2011', '1', '1155000', '1155000', '-433125', '-721875', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (492, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '43', '4', 'LX300', '2011', '1', '2530000', '2530000', '-948750', '-1581250', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (493, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '44', '5', 'LX300', '2011', '1', '2530000', '2530000', '-948750', '-1581250', '0', '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (494, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '45', '1', 'HP P1102 W', '2014', '1', '1488300', '1488300', NULL, '-1488302', '2', '0', 'CV.CAHAYA BUKIT MAS', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (495, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '46', '2', 'HP P1102 W', '2014', '1', '1488300', '1488300', NULL, '-1488302', '2', '0', 'CV.CAHAYA BUKIT MAS', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (496, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '47', '3', 'HP P1102 W', '2014', '1', '1488300', '1488300', NULL, '-1488302', '2', '0', 'CV.CAHAYA BUKIT MAS', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (497, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '48', '4', 'HP P1102 W', '2014', '1', '1488300', '1488300', NULL, '-1488302', '2', '0', 'CV.CAHAYA BUKIT MAS', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (498, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '49', '5', 'HP P1102 W', '2014', '1', '1488300', '1488300', NULL, '-1488302', '2', '0', 'CV.CAHAYA BUKIT MAS', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (499, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '5', '1', 'HP Laserjet 1160', '2005', '1', '3366000', '3366000', '-3366000', NULL, NULL, '0', 'DIPA 2005', '', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (500, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '50', '1', 'HP P1102 W', '2014', '1', '1488300', '1488300', NULL, '-1488302', '2', '0', 'CV.CAHAYA BUKIT MAS', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (501, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '51', '2', 'HP P1102 W', '2014', '1', '1488300', '1488300', NULL, '-1488302', '2', '0', 'CV.CAHAYA BUKIT MAS', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (502, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '52', '3', 'HP P1102 W', '2014', '1', '1488300', '1488300', NULL, '-1488302', '2', '0', 'CV.CAHAYA BUKIT MAS', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (503, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '53', '4', 'HP P1102 W', '2014', '1', '1488300', '1488300', NULL, '-1488302', '2', '0', 'CV.CAHAYA BUKIT MAS', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (504, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '54', '5', 'HP P1102 W', '2014', '1', '1488300', '1488300', NULL, '-1488302', '2', '0', 'CV.CAHAYA BUKIT MAS', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (505, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '6', '1', '', '2006', '1', '2300000', '2300000', '-2300000', NULL, NULL, '0', 'BAp barang (CV Citra Media', 'YF.01.01.IIA.025/30-', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (506, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '65', '1', 'Brother MFC-L2700DW', '2015', '1', '3628790', '3628790', NULL, '-3175193', '2', '453599', 'CV.ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (507, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '66', '2', 'Brother MFC-L2700DW', '2015', '1', '3628790', '3628790', NULL, '-3175193', '2', '453599', 'CV.ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (508, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '67', '3', 'Brother MFC-L2700DW', '2015', '1', '3628790', '3628790', NULL, '-3175193', '2', '453599', 'CV.ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (509, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '68', '4', 'Brother MFC-L2700DW', '2015', '1', '3628790', '3628790', NULL, '-3175193', '2', '453599', 'CV.ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (510, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '69', '5', 'Brother MFC-L2700DW', '2015', '1', '3628790', '3628790', NULL, '-3175193', '2', '453599', 'CV.ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (511, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '7', '1', '', '2006', '1', '2300000', '2300000', '-2300000', NULL, NULL, '0', 'BAp barang (CV Citra Media', 'YF.01.01.IIA.025/30-', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (512, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '70', '1', 'Canon', '2016', '1', '1402500', '1402500', NULL, '-876563', NULL, '525937', 'PT AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (513, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '71', '2', 'Canon', '2016', '1', '1402500', '1402500', NULL, '-876563', NULL, '525937', 'PT AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (514, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '72', '3', 'Canon', '2016', '1', '1402500', '1402500', NULL, '-876563', NULL, '525937', 'PT AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (515, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '73', '4', 'Canon', '2016', '1', '1402500', '1402500', NULL, '-876563', NULL, '525937', 'PT AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (516, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '74', '5', 'Canon', '2016', '1', '1402500', '1402500', NULL, '-876563', NULL, '525937', 'PT AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (517, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '75', '6', 'Canon', '2016', '1', '1402500', '1402500', NULL, '-876563', NULL, '525937', 'PT AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (518, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '76', '7', 'Canon', '2016', '1', '1402500', '1402500', NULL, '-876563', NULL, '525937', 'PT AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (519, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '77', '8', 'Canon', '2016', '1', '1402500', '1402500', NULL, '-876563', NULL, '525937', 'PT AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (520, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '78', '9', 'Canon', '2016', '1', '1402500', '1402500', NULL, '-876563', NULL, '525937', 'PT AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (521, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '79', '10', 'Canon', '2016', '1', '1402500', '1402500', NULL, '-876563', NULL, '525937', 'PT AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (522, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '8', '1', '', '2006', '1', '2300000', '2300000', '-2300000', NULL, NULL, '0', 'BAp barang (CV Citra Media', 'YF.01.01.IIA.025/30-', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (523, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '80', '1', 'Canon', '2016', '1', '1402500', '1402500', NULL, '-876563', NULL, '525937', 'PT AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (524, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '81', '2', 'Canon', '2016', '1', '1402500', '1402500', NULL, '-876563', NULL, '525937', 'PT AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (525, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '82', '3', 'Canon', '2016', '1', '1402500', '1402500', NULL, '-876563', NULL, '525937', 'PT AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (526, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '83', '4', 'Canon', '2016', '1', '1402500', '1402500', NULL, '-876563', NULL, '525937', 'PT AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (527, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '84', '5', 'Canon', '2016', '1', '1402500', '1402500', NULL, '-876563', NULL, '525937', 'PT AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (528, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '9', '1', '', '2006', '1', '2300000', '2300000', '-2300000', NULL, NULL, '0', 'BAp barang (CV Citra Media', 'YF.01.01.IIA.025/30-', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (529, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '91', '1', 'HP LJM426', '2018', '1', '8797600', '8797600', NULL, '-1099700', NULL, '7697900', 'PT. Air Mas Perkasa', 'KN.01.04/IV/005/OLDAT.1/3504/2018', 'Alat Pendukung Perkantoran Paket PKM-P1809-1199673', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (530, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '92', '2', 'HP LJM426', '2018', '1', '8797600', '8797600', NULL, '-1099700', NULL, '7697900', 'PT. Air Mas Perkasa', 'KN.01.04/IV/005/OLDAT.1/3504/2018', 'Alat Pendukung Perkantoran Paket PKM-P1809-1199673', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (531, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '93', '3', 'HP LJM426', '2018', '1', '8797600', '8797600', NULL, '-1099700', NULL, '7697900', 'PT. Air Mas Perkasa', 'KN.01.04/IV/005/OLDAT.1/3504/2018', 'Alat Pendukung Perkantoran Paket PKM-P1809-1199673', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (532, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '94', '4', 'HP LJM426', '2018', '1', '8797600', '8797600', NULL, '-1099700', NULL, '7697900', 'PT. Air Mas Perkasa', 'KN.01.04/IV/005/OLDAT.1/3504/2018', 'Alat Pendukung Perkantoran Paket PKM-P1809-1199673', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (533, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '95', '5', 'HP LJM203', '2018', '1', '3325100', '3325100', NULL, '-415638', NULL, '2909462', 'PT. Air Mas Perkasa', 'KN.01.04/IV/005/OLDAT.1/3504/2018', 'Alat Pendukung Perkantoran Paket PKM-P1809-1199673', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (534, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '96', '6', 'HP LJM203', '2018', '1', '3325100', '3325100', NULL, '-415638', NULL, '2909462', 'PT. Air Mas Perkasa', 'KN.01.04/IV/005/OLDAT.1/3504/2018', 'Alat Pendukung Perkantoran Paket PKM-P1809-1199673', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (535, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '97', '7', 'HP LJM203', '2018', '1', '3325100', '3325100', NULL, '-415638', NULL, '2909462', 'PT. Air Mas Perkasa', 'KN.01.04/IV/005/OLDAT.1/3504/2018', 'Alat Pendukung Perkantoran Paket PKM-P1809-1199673', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (536, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '98', '8', 'HP LJM203', '2018', '1', '3325100', '3325100', NULL, '-415638', NULL, '2909462', 'PT. Air Mas Perkasa', 'KN.01.04/IV/005/OLDAT.1/3504/2018', 'Alat Pendukung Perkantoran Paket PKM-P1809-1199673', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (537, '024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '99', '9', 'HP LJM203', '2018', '1', '3325100', '3325100', NULL, '-415638', NULL, '2909462', 'PT. Air Mas Perkasa', 'KN.01.04/IV/005/OLDAT.1/3504/2018', 'Alat Pendukung Perkantoran Paket PKM-P1809-1199673', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (538, '024070199465983000KP', '3100203004', 'Scanner (Peralatan Personal Komputer)', '1', '1', '', '2006', '1', '1300000', '1300000', '-1300000', NULL, '0', '0', 'BAP barang (CV Citra Media', 'YF.01.02.IIA.025/30-', '', 'DBR', '3', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (539, '024070199465983000KP', '3100203004', 'Scanner (Peralatan Personal Komputer)', '2', '1', 'Panasonic KV-S1028Y', '2018', '1', '16890000', '16890000', NULL, '-2111250', NULL, '14778750', 'PT. Air Mas Perkasa', 'KN.01.04/IV/005/OLDAT.1/3504/2018', 'Alat Pendukung Perkantoran Paket PKM-P1809-1199673', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (540, '024070199465983000KP', '3100203004', 'Scanner (Peralatan Personal Komputer)', '3', '2', 'Panasonic KV-S1028Y', '2018', '1', '16890000', '16890000', NULL, '-2111250', NULL, '14778750', 'PT. Air Mas Perkasa', 'KN.01.04/IV/005/OLDAT.1/3504/2018', 'Alat Pendukung Perkantoran Paket PKM-P1809-1199673', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (541, '024070199465983000KP', '3100203004', 'Scanner (Peralatan Personal Komputer)', '4', '3', 'Panasonic KV-S1028Y', '2018', '1', '16890000', '16890000', NULL, '-2111250', NULL, '14778750', 'PT. Air Mas Perkasa', 'KN.01.04/IV/005/OLDAT.1/3504/2018', 'Alat Pendukung Perkantoran Paket PKM-P1809-1199673', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (542, '024070199465983000KP', '3100203017', 'External/ Portable Hardisk', '1', '1', 'Lokal', '2013', '1', '2600000', '2600000', NULL, '-2600000', NULL, '0', 'APBN 2013', '001/F/HG/II/2013', 'Baru', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (543, '024070199465983000KP', '3100203017', 'External/ Portable Hardisk', '2', '2', 'Lokal', '2013', '1', '2600000', '2600000', NULL, '-2600000', NULL, '0', 'APBN 2013', '001/F/HG/II/2013', 'Baru', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (544, '024070199465983000KP', '3100203017', 'External/ Portable Hardisk', '3', '3', 'Lokal', '2013', '1', '2600000', '2600000', NULL, '-2600000', NULL, '0', 'APBN 2013', '001/F/HG/II/2013', 'Baru', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (545, '024070199465983000KP', '3100203017', 'External/ Portable Hardisk', '4', '4', 'Lokal', '2013', '1', '2600000', '2600000', NULL, '-2600000', NULL, '0', 'APBN 2013', '001/F/HG/II/2013', 'Baru', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (546, '024070199465983000KP', '3100203017', 'External/ Portable Hardisk', '5', '5', 'Lokal', '2013', '1', '2600000', '2600000', NULL, '-2600000', NULL, '0', 'APBN 2013', '001/F/HG/II/2013', 'Baru', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (547, '024070199465983000KP', '3100204001', 'Server', '1', '1', 'intel', '2012', '1', '63499700', '63499700', '-7937463', '-55562237', '0', '0', 'APBN', '201482y', 'pembelian', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (548, '024070199465983000KP', '3100204001', 'Server', '2', '1', 'IBM', '2013', '1', '341165000', '341165000', NULL, '-341165000', NULL, '0', 'APBN 2013', 'BA.KN.01.02/PT.V/STR.1/2013', 'Pusat Data', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (549, '024070199465983000KP', '3100204001', 'Server', '3', '1', 'DELL', '2014', '1', '66000000', '66000000', NULL, '-66000000', NULL, '0', 'CV.Sumber Setia', 'KN.01.02/PI.V/SERVER.1/2014', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (550, '024070199465983000KP', '3100204001', 'Server', '4', '2', 'DELL', '2014', '1', '66000000', '66000000', NULL, '-66000000', NULL, '0', 'CV.Sumber Setia', 'KN.01.02/PI.V/SERVER.1/2014', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (551, '024070199465983000KP', '3100204001', 'Server', '5', '3', 'DELL', '2014', '1', '66000000', '66000000', NULL, '-66000000', NULL, '0', 'CV.Sumber Setia', 'KN.01.02/PI.V/SERVER.1/2014', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (552, '024070199465983000KP', '6070301001', 'Gedung dan Bangunan Dalam Renovasi', '2', '1', '-', '2018', '1', '162329200', '162329200', NULL, NULL, NULL, '162329200', '', 'KN.01.03/IV/TATA.RKA/10/2018', 'Ruang Direktur', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (553, '024070199465983000KP', '8010101001', 'Software Komputer', '1', '1', '-', '2014', '1', '154962500', '154962500', '-58110938', '-96851562', NULL, '0', 'PT.EDI INDONESIA', 'IR.01.03/05/E-PBF/11/2014', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (554, '024070199465983000KP', '8010101001', 'Software Komputer', '2', '2', '-', '2014', '1', '89980000', '89980000', '-33742500', '-56237500', NULL, '0', 'PT.EDI', 'IR.01.03/05/LINK/11/2014', 'INTEGRATED SYSTEM', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (555, '024070199465983000KP', '8010101001', 'Software Komputer', '3', '3', '', '2014', '1', '84920000', '84920000', '-31845000', '-53075000', NULL, '0', 'PT. EDI', 'IR.01.03/05/PRODIS-NPP/11/2014', '', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (556, '024070199465983000KP', '8010101001', 'Software Komputer', '4', '1', '', '2016', '1', '89980000', '89980000', NULL, '-56237500', '0', '33742500', 'PT EDII', 'IR.01.03/05/DASHBOARD/11/2016', 'Dashboard Profar', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (557, '024070199465983000KP', '8010101001', 'Software Komputer', '5', '1', '-', '2012', '1', '345000040', '345000040', NULL, NULL, '-345000040', '0', '-', 'KN.02.04/05/1994/2017', 'Sofware', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (558, '024070199465983000KP', '8010101001', 'Software Komputer', '7', '1', '', '2018', '1', '179377000', '179377000', NULL, '-22422125', '0', '156954875', '-', 'KN.01.03/ IV/SIS.REPRO/11/2018', 'Sistem Realisasi Produksi', 'DBR', '1', NULL, 'img/no_picture.png');
INSERT INTO `barang` VALUES (559, '024070199465983000KP', '8010101001', 'Software Komputer', '8', '2', '', '2018', '1', '76890000', '76890000', NULL, '-9611250', '0', '67278750', '', 'KN.01.03/IV/ SIS.GOTIK/10/2018', 'SiGotik', 'DBR', '1', NULL, 'img/no_picture.png');

-- ----------------------------
-- Table structure for barang_upload
-- ----------------------------
DROP TABLE IF EXISTS `barang_upload`;
CREATE TABLE `barang_upload`  (
  `kd_lokasi` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `kd_brg` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ur_sskel` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `no_aset` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `merk_type` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `thn_perlh` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kuantitas` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `rph_perlh` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `rph_akhir` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `p_pertama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `p_reguler` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `p_transaksi` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `rph_buku` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `asal_perlh` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `no_bukti` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `keterangan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tercatat` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kond` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `transaksi` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`kd_lokasi`, `kd_brg`, `no_aset`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of barang_upload
-- ----------------------------
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3020101001', 'Sedan', '1', 'TOYOTA ALTIS', '2013', '1', '362268600', '362268600', NULL, '-25876329', '-258763284', '77628987', 'Sesditjen Kefarmasian dan Alat Kesehatan', 'KR.003/SPK/MOBIL-ESELON I', 'BAST TK KR4 Setditjen Ke Prodisfar', 'KIB', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3020102003', 'Mini Bus ( Penumpang 14 Orang Kebawah )', '3', '', '2012', '1', '235330000', '235330000', '-16809286', '-201711431', '3', '16809286', '', '', '', 'KIB', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3020102003', 'Mini Bus ( Penumpang 14 Orang Kebawah )', '4', '', '2012', '1', '235330000', '235330000', '-16809286', '-201711431', '3', '16809286', '', '', '', 'KIB', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3020102003', 'Mini Bus ( Penumpang 14 Orang Kebawah )', '5', 'Toyota Kijang Inova', '2013', '1', '172170000', '172170000', NULL, '-144060613', '-3513672', '24595715', 'Setditjen', 'KN.02.03/4/782/2013', '', 'KIB', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3020104001', 'Sepeda Motor', '1', 'Honda Vario CBS', '2011', '1', '15800000', '15800000', NULL, '-7900002', '-7899998', '0', '', '', 'Perubahan Kode Barang', 'KIB', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3020104001', 'Sepeda Motor', '2', 'Honda Vario CBS', '2011', '1', '15800000', '15800000', NULL, '-7900002', '-7899998', '0', '', '', 'Perubahan Kode Barang', 'KIB', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3020104001', 'Sepeda Motor', '3', 'Honda Vario CBS', '2011', '1', '15800000', '15800000', NULL, '-7900002', '-7899998', '0', '', '', 'Perubahan Kode Barang', 'KIB', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3020104001', 'Sepeda Motor', '4', 'Honda Vario CBS', '2011', '1', '15800000', '15800000', NULL, '-7900002', '-7899998', '0', '', '', 'Perubahan Kode Barang', 'KIB', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3020104001', 'Sepeda Motor', '5', 'Honda Vario CBS', '2011', '1', '15800000', '15800000', NULL, '-7900002', '-7899998', '0', '', '', 'Perubahan Kode Barang', 'KIB', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3020201004', 'Lori Dorong', '1', 'Amtrong', '2003', '1', '415000', '318000', '-318000', NULL, NULL, '0', 'APBN 2003', '', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050101002', 'Mesin Ketik Manual Standard (14-16 Inci)', '1', 'oliveti14-16 inci', '2002', '1', '207500', '207500', '-207500', NULL, '0', '0', 'Hibah dari Yanmed', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050101003', 'Mesin Ketik Manual Langewagon (18-27 Inci)', '1', 'oliveti', '2004', '1', '3650000', '3138000', '-3138000', NULL, '0', '0', 'APBN 2004', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050101008', 'Mesin Ketik Elektronik/Selektrik', '1', 'GX-6750', '2011', '1', '1980000', '1980000', '-594000', '-1386000', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050101008', 'Mesin Ketik Elektronik/Selektrik', '2', 'GX-6750', '2011', '1', '1980000', '1980000', '-594000', '-1386000', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050102002', 'Mesin Hitung Listrik', '1', 'Cacio', '2004', '1', '2000000', '1545000', '-1545000', NULL, NULL, '0', 'APBN tahun 2004', '', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050102003', 'Mesin Hitung Elektronik/Calculator', '1', 'Citisen', '2004', '1', '200000', '200000', '-200000', NULL, NULL, '0', 'APBN TH. 2004', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050102003', 'Mesin Hitung Elektronik/Calculator', '2', 'Citizen', '2004', '1', '200000', '200000', '-200000', NULL, NULL, '0', 'APBN 2004', '', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050102007', 'Mesin Penghitung Uang', '1', '', '2016', '1', '1510000', '1510000', NULL, '-906000', NULL, '604000', 'CV. Cahaya Bukit Mas', 'BN.01.02/05/AP-KANTOR/03/2016', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050103009', 'Mesin Fotocopy Electronic', '1', 'CANON', '2010', '1', '9500000', '9500000', '-4750000', '-4750000', NULL, '0', 'APBN 2010', '118/F/DUKXI/2010', 'MONOCHROME LASER', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050103009', 'Mesin Fotocopy Electronic', '2', 'HP LASERJET M1536dnf', '2013', '1', '4205251', '4205251', NULL, '-4205251', NULL, '0', 'APBN 13', 'BN.01.03/05/APD/10/2013', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050103009', 'Mesin Fotocopy Electronic', '3', 'HP LASERJET M1536dnf', '2013', '1', '4205251', '4205251', NULL, '-4205251', NULL, '0', 'APBN 13', 'BN.01.03/05/APD/10/2013', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050103009', 'Mesin Fotocopy Electronic', '4', 'HP LASERJET M1536dnf', '2013', '1', '4205251', '4205251', NULL, '-4205251', NULL, '0', 'APBN 13', 'BN.01.03/05/APD/10/2013', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050103009', 'Mesin Fotocopy Electronic', '5', 'HP LASERJET M1536dnf', '2013', '1', '4205251', '4205251', NULL, '-4205251', NULL, '0', 'APBN 13', 'BN.01.03/05/APD/10/2013', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050103011', 'Mesin Fotocopy Lainnya', '1', 'FUJI', '2016', '1', '19795000', '19795000', NULL, '-9897500', NULL, '9897500', 'PT.AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050104001', 'Lemari Besi/Metal', '1', 'Yunika (Pendek)', '2002', '1', '855000', '723000', '-723000', NULL, '0', '0', 'DIK 2002', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050104002', 'Lemari Kayu', '1', 'Lokal', '2002', '1', '1050000', '441000', '-441000', NULL, NULL, '0', 'Hibah Yanmed', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050104002', 'Lemari Kayu', '10', 'Lokal', '2002', '1', '1590000', '1223000', '-1223000', NULL, NULL, '0', 'DIPA 2002', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050104002', 'Lemari Kayu', '11', 'Lokal', '2002', '1', '1585000', '1219000', '-1219000', NULL, NULL, '0', 'DIPA 2002', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050104002', 'Lemari Kayu', '12', 'Lokal', '2002', '1', '1478000', '1137000', '-1137000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050104002', 'Lemari Kayu', '13', 'Lokal', '2002', '1', '1478000', '1137000', '-1137000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050104002', 'Lemari Kayu', '14', 'Lokal', '2002', '1', '1478000', '1137000', '-1137000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050104002', 'Lemari Kayu', '15', 'Lokal', '2002', '1', '1478000', '1137000', '-1137000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050104002', 'Lemari Kayu', '16', 'Lokal', '2002', '1', '1478000', '1137000', '-1137000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050104002', 'Lemari Kayu', '17', 'Lokal', '2002', '1', '1478000', '1137000', '-1137000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050104002', 'Lemari Kayu', '18', 'Lokal', '2002', '1', '1478000', '1137000', '-1137000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050104002', 'Lemari Kayu', '19', 'Lokal', '2004', '1', '1750000', '1428000', '-1428000', NULL, NULL, '0', 'DIK 2004', '', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050104002', 'Lemari Kayu', '2', 'Lokal', '2002', '1', '1050000', '441000', '-441000', NULL, NULL, '0', 'Hibah Yanmed', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050104002', 'Lemari Kayu', '3', '4 Pintu (160x40x210)', '2005', '1', '5351500', '5351500', '-5351500', NULL, NULL, '0', 'DIPA 2005', '', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050104002', 'Lemari Kayu', '4', '4 Pintu (160x40x210)', '2005', '1', '5351500', '5351500', '-5351500', NULL, NULL, '0', 'DIPA 2005', '', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050104002', 'Lemari Kayu', '5', '4 Pintu (160x40x210)', '2005', '1', '5351500', '5351500', '-5351500', NULL, NULL, '0', 'DIPA 2005', '', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050104002', 'Lemari Kayu', '6', '4 Pintu (160x40x210)', '2005', '1', '5351500', '5351500', '-5351500', NULL, NULL, '0', 'DIPA 2005', '', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050104002', 'Lemari Kayu', '7', '4 Pintu (160x40x210)', '2005', '1', '5351500', '5351500', '-5351500', NULL, NULL, '0', 'DIPA 2005', '', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050104002', 'Lemari Kayu', '8', '4 Pintu (160x40x210)', '2005', '1', '5351500', '5351500', '-5351500', NULL, NULL, '0', 'DIPA 2005', '', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050104002', 'Lemari Kayu', '9', 'Lokal', '2005', '1', '3795000', '3795000', '-3795000', NULL, NULL, '0', 'DIPA 2005', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050104005', 'Filing Cabinet Besi', '1', 'Lokal', '2002', '1', '207500', '207500', '-207500', NULL, NULL, '0', 'Hibah Yanmed', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050104005', 'Filing Cabinet Besi', '2', 'Lokal', '2002', '1', '207500', '207500', '-207500', NULL, NULL, '0', 'Hibah Yanmed', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050104005', 'Filing Cabinet Besi', '3', 'Lokal', '2002', '1', '207500', '207500', '-207500', NULL, NULL, '0', 'Hibah Yanmed', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050104005', 'Filing Cabinet Besi', '4', 'Lokal', '2002', '1', '1586000', '1342000', '-1342000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050104005', 'Filing Cabinet Besi', '5', 'Lokal', '2002', '1', '1586000', '1342000', '-1342000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050104005', 'Filing Cabinet Besi', '6', 'Lokal', '2002', '1', '1586000', '1342000', '-1342000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050104005', 'Filing Cabinet Besi', '7', 'Lokal', '2002', '1', '1586000', '1342000', '-1342000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050104005', 'Filing Cabinet Besi', '8', 'Lokal', '2003', '1', '1586000', '1342000', '-1342000', NULL, '0', '0', 'DIK 2002', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050104005', 'Filing Cabinet Besi', '9', 'Lokal', '2003', '1', '1586000', '1352000', '-1352000', NULL, '0', '0', 'DIK 2003', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050104007', 'Brandkas', '1', 'Lokal', '2002', '1', '12485000', '10557000', '-10557000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050104013', 'Buffet', '1', 'Lokal', '2005', '1', '3905000', '3905000', '-3905000', NULL, NULL, '0', 'DIPA 2005', '', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050104014', 'Mobile File', '1', 'Lokal', '2013', '1', '28800000', '28800000', NULL, '-28800000', NULL, '0', 'APBN 2013', '051/FT/BDJ/X/2013', 'Type I', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050104014', 'Mobile File', '2', 'Lokal', '2013', '1', '38400000', '38400000', NULL, '-38400000', NULL, '0', 'APBN 2013', '051/FT/BDJ/X/2013', 'Type II', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050104014', 'Mobile File', '3', 'Lokal', '2013', '1', '31800000', '31800000', NULL, '-31800000', NULL, '0', 'APBN 2013', '051/FT/BDJ/X/2013', 'Type III', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050104014', 'Mobile File', '4', 'Lokal', '2013', '1', '28800000', '28800000', NULL, '-28800000', NULL, '0', 'APBN 2013', '051/FT/BDJ/X/2013', 'Type IV', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050104014', 'Mobile File', '5', 'Lokal', '2013', '1', '28800000', '28800000', NULL, '-28800000', NULL, '0', 'APBN 2013', '051/FT/BDJ/X/2013', 'Type IV', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050104014', 'Mobile File', '6', 'Lokal', '2013', '1', '28800000', '28800000', NULL, '-28800000', NULL, '0', 'APBN 2013', '051/FT/BDJ/X/2013', 'Type V', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050105015', 'Alat Penghancur Kertas', '1', 'HP AURORA', '2010', '1', '3150000', '3150000', '-1575000', '-1575000', '0', '0', 'APBN 2010', '118/F/DUKXI/2010', 'CV. Delpindo Utama Karya', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050105015', 'Alat Penghancur Kertas', '2', '', '2016', '1', '3920000', '3920000', NULL, '-2352000', NULL, '1568000', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/AP-KANTOR/03/2016', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050105015', 'Alat Penghancur Kertas', '3', 'IDEAL', '2018', '1', '23713700', '23713700', NULL, '-2371370', NULL, '21342330', 'PT. Air Mas Perkasa', 'KN.01.04/IV/005/OLDAT.1/3504/2018', 'Alat Penghancur Kertas', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050105039', 'Display', '1', '', '2006', '1', '4550000', '4550000', '-4550000', NULL, NULL, '0', 'BAP barang (CV. Bina Utama)', 'YF.01.01.IIA.042/03-', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050105039', 'Display', '2', '', '2006', '1', '4550000', '4550000', '-4550000', NULL, NULL, '0', 'BAP barang (CV. Bina Utama)', 'YF.01.01.IIA.042/03-', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050105039', 'Display', '3', '', '2006', '1', '4550000', '4550000', '-4550000', NULL, NULL, '0', 'BAP barang (CV. Bina Utama)', 'YF.01.01.IIA.042/03-', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050105039', 'Display', '4', '', '2007', '1', '7500000', '7500000', '-7500000', NULL, NULL, '0', 'APBN 2007', 'SP2D 774039F TGL 09-', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050105039', 'Display', '5', 'Sony', '2013', '1', '74415000', '74415000', NULL, '-74415000', NULL, '0', 'APBN 2013', '30810', 'Baru', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050105048', 'LCD Projector/Infocus', '1', 'ACER P 1165', '2008', '1', '10872744', '10872744', '-9785470', '-1087274', '0', '0', 'CV. DELPINDO UTAMA KARYA', 'PL.01.01/3/275/2008', 'DLP, NATIVE XGA', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050105048', 'LCD Projector/Infocus', '10', 'Projector EPSON EB-1771W', '2015', '1', '12947000', '12947000', NULL, '-9062900', NULL, '3884100', 'CV. ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050105048', 'LCD Projector/Infocus', '11', 'Projector EPSON EB-1771W', '2015', '1', '12947000', '12947000', NULL, '-9062900', NULL, '3884100', 'CV. ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050105048', 'LCD Projector/Infocus', '12', 'Panasonic VW540', '2018', '1', '22666500', '22666500', NULL, '-2266650', NULL, '20399850', 'PT. Air Mas Perkasa', 'Kuitansi No.PSI-AMP-1812-00922', 'Pengadaan Proyektor', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050105048', 'LCD Projector/Infocus', '2', 'ACER P1266I', '2009', '1', '13409550', '13409550', '-9386685', '-4022865', NULL, '0', 'PT. Complus Sistem Solusi', 'PL.01.01/3/168/2009', 'Project System DLP', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050105048', 'LCD Projector/Infocus', '3', 'ACER P1266I', '2009', '1', '13409550', '13409550', '-9386685', '-4022865', NULL, '0', 'PT. Complus Sistem Solusi', 'PL.01.01/3/168/2009', 'Project System DLP', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050105048', 'LCD Projector/Infocus', '4', 'SONY', '2010', '1', '12850000', '12850000', '-6425000', '-6425000', NULL, '0', 'APBN 2010', '118/F/DUKXI/2010', '3LCD TECHNOLOGY', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050105048', 'LCD Projector/Infocus', '5', 'Panasonic', '2011', '1', '17806800', '17806800', '-5342040', '-12464760', NULL, '0', 'APBN', 'PL.01.01.05/LCD/13/IX/2011', 'Pembelian 2011', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050105048', 'LCD Projector/Infocus', '6', 'Sony', '2011', '1', '12196800', '12196800', '-3659040', '-8537760', NULL, '0', 'APBN', 'PL.01.01.05/LCD/13/IX/2011', 'Pembelian 2011', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050105048', 'LCD Projector/Infocus', '7', 'SONY Type EW 130', '2012', '1', '14746600', '14746600', '-1474660', '-13271940', NULL, '0', 'APBN', '905228A', '2012', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050105048', 'LCD Projector/Infocus', '8', 'Sony', '2013', '1', '11000000', '11000000', NULL, '-11000000', NULL, '0', 'APBN 2013', '30810', 'Infocus', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050105048', 'LCD Projector/Infocus', '9', 'EPSON EB-1751', '2014', '1', '11995000', '11995000', NULL, '-10795500', NULL, '1199500', 'CV.CAHAYA BUKIT MAS', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050105057', 'Pintu Elektrik (yang Memakai Akses)', '1', 'SOLUTION X304', '2015', '1', '4101000', '4101000', NULL, '-2460600', '-820200', '820200', 'Sesditjen farmalkes', 'BAPP KN.01.01/4/0296/2015', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050105058', 'Focusing Screen/Layar LCD Projector', '1', 'APOLLO', '2010', '1', '2150000', '2150000', '-1075000', '-1075000', '0', '0', 'APBN 2010', '118/F/DUKXI/2010', '100\" DIAGONAL SIZE', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050105058', 'Focusing Screen/Layar LCD Projector', '2', 'Infocus', '2013', '1', '9350000', '9350000', NULL, '-9350000', NULL, '0', 'APBN 2013', '30810', 'Baru', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050199999', 'Alat Kantor Lainnya', '1', '', '2016', '1', '18159680', '18159680', NULL, '-10895808', NULL, '7263872', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050199999', 'Alat Kantor Lainnya', '2', '-', '2016', '1', '1500000', '1500000', NULL, '-750000', NULL, '750000', '-', '-', 'Kabel HDMI', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201001', 'Meja Kerja Besi/Metal', '1', 'Vinoti Glass Coffee Table', '2016', '1', '4950000', '4950000', NULL, '-1485000', '-990000', '2475000', 'Sesditjen Farmalkes', 'BN.06.02/3/4387/2016', 'TK PM Meubelair TA 2016', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '1', '1/2 Biro', '2002', '1', '78250', '78250', '-78250', NULL, '0', '0', 'Hibah Itjen', 'BAST  NO.PL.00.07.1.', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '10', '1 Biro Eselon III', '2002', '1', '95000', '95000', '-95000', NULL, NULL, '0', 'Hibah Itjen', 'No.01T.PS.19.03.214.', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '11', '1 Biro Eselon III', '2002', '1', '95000', '95000', '-95000', NULL, NULL, '0', 'Hibah Itjen', 'No.01T.PS.19.03.214.', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '12', '1 Biro Eselon III', '2002', '1', '95000', '95000', '-95000', NULL, NULL, '0', 'Hibah Itjen', 'No.01T.PS.19.03.214.', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '13', '1 Biro Eselon III', '2002', '1', '95000', '95000', '-95000', NULL, NULL, '0', 'Hibah Itjen', 'No.01T.PS.19.03.214.', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '14', 'Meja Eselon III', '2003', '1', '1445000', '1140000', '-1140000', NULL, NULL, '0', 'APBN 2003', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '15', 'Meja Eselon III', '2003', '1', '1445000', '1140000', '-1140000', NULL, NULL, '0', 'APBN 2003', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '16', 'Meja Eselon III', '2003', '1', '1445000', '1140000', '-1140000', NULL, NULL, '0', 'APBN 2003', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '17', 'Meja Es.IV', '2002', '1', '1622500', '1324000', '-1324000', NULL, '0', '0', 'APBN 2004', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '18', 'Meja Es.IV', '2002', '1', '1622500', '1324000', '-1324000', NULL, '0', '0', 'APBN 2004', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '19', 'Meja Es.IV', '2002', '1', '1622500', '1324000', '-1324000', NULL, '0', '0', 'APBN 2004', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '2', '1/2 Biro', '2002', '1', '78250', '78250', '-78250', NULL, '0', '0', 'Hibah Itjen', 'BAST  NO.PL.00.07.1.', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '20', 'Meja Ess IV', '2002', '1', '1705000', '1705000', '-1705000', NULL, '0', '0', 'DIPA 2005', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '21', 'Meja Ess IV', '2002', '1', '1705000', '1705000', '-1705000', NULL, '0', '0', 'DIPA 2005', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '22', 'Meja Ess IV', '2002', '1', '1705000', '1705000', '-1705000', NULL, '0', '0', 'DIPA 2005', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '23', 'Meja Ess IV', '2002', '1', '1705000', '1705000', '-1705000', NULL, '0', '0', 'DIPA 2005', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '24', 'Meja Ess IV', '2002', '1', '1705000', '1705000', '-1705000', NULL, '0', '0', 'DIPA 2005', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '25', 'Meja Ess IV', '2002', '1', '1705000', '1705000', '-1705000', NULL, '0', '0', 'DIPA 2005', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '26', 'Meja Ess IV', '2002', '1', '1705000', '1705000', '-1705000', NULL, '0', '0', 'DIPA 2005', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '27', 'Meja Ess.II', '2002', '1', '95000', '95000', '-95000', NULL, '0', '0', 'Hibah Itjen', 'No.01T.PS 19.03.214.', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '28', 'Meja Ess.II', '2002', '1', '1720000', '1323000', '-1323000', NULL, '0', '0', 'APBN 2002', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '29', 'lokal', '2013', '1', '3261500', '3261500', NULL, '-3131040', '-130460', '0', 'setditjen', 'KN.02.03/4/782/2013', 'Meja Kerja Direktur', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '3', '1/2 Biro', '2002', '1', '78250', '78250', '-78250', NULL, '0', '0', 'Hibah Itjen', 'BAST  NO.PL.00.07.1.', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '30', 'lokal', '2013', '1', '2255000', '2255000', NULL, '-2164800', '-90200', '0', 'setditjen', 'KN.02.03/4/782/2013', 'Meja Kerja Direktur', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '31', 'lokal', '2013', '1', '2255000', '2255000', NULL, '-2164800', '-90200', '0', 'setditjen', 'KN.02.03/4/782/2013', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '32', 'lokal', '2013', '1', '2255000', '2255000', NULL, '-2164800', '-90200', '0', 'setditjen', 'KN.02.03/4/782/2013', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '33', 'lokal', '2013', '1', '2255000', '2255000', NULL, '-2164800', '-90200', '0', 'setditjen', 'KN.02.03/4/782/2013', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '34', 'lokal', '2013', '1', '2255000', '2255000', NULL, '-2164800', '-90200', '0', 'setditjen', 'KN.02.03/4/782/2013', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '35', 'lokal', '2013', '1', '2255000', '2255000', NULL, '-2164800', '-90200', '0', 'setditjen', 'KN.02.03/4/782/2013', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '4', '1/2 Biro', '2002', '1', '78250', '78250', '-78250', NULL, '0', '0', 'Hibah Itjen', 'BAST  NO.PL.00.07.1.', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '5', '1/2 Biro', '2002', '1', '78250', '78250', '-78250', NULL, '0', '0', 'Hibah Itjen', 'BAST  NO.PL.00.07.1.', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '6', '1/2 Biro', '2002', '1', '78250', '78250', '-78250', NULL, '0', '0', 'Hibah Itjen', 'BAST  NO.PL.00.07.1.', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '7', '1/2 Biro', '2002', '1', '78250', '78250', '-78250', NULL, '0', '0', 'Hibah Itjen', 'BAST  NO.PL.00.07.1.', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '8', '1/2 Biro', '2002', '1', '78250', '78250', '-78250', NULL, '0', '0', 'Hibah Itjen', 'BAST  NO.PL.00.07.1.', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201002', 'Meja Kerja Kayu', '9', '1/2 Biro', '2002', '1', '78250', '78250', '-78250', NULL, '0', '0', 'Hibah Itjen', 'BAST  NO.PL.00.07.1.', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '1', 'RAKUDA', '2005', '1', '660000', '660000', '-660000', NULL, NULL, '0', 'APBN', '', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '10', 'YESTINE', '2003', '1', '727000', '620000', '-620000', NULL, NULL, '0', 'DIK 2003', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '100', 'Kursi Rapat (K1)', '2016', '1', '1435000', '1435000', NULL, '-430500', '-287000', '717500', 'Sesditjen Farmalkes', 'BN.06.02/3/4387/2016', 'TK PM Meubelair TA 2016', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '101', 'Kursi Rapat (K1)', '2016', '1', '1435000', '1435000', NULL, '-430500', '-287000', '717500', 'Sesditjen Farmalkes', 'BN.06.02/3/4387/2016', 'TK PM Meubelair TA 2016', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '102', 'Kursi Rapat (K1)', '2016', '1', '1435000', '1435000', NULL, '-430500', '-287000', '717500', 'Sesditjen Farmalkes', 'BN.06.02/3/4387/2016', 'TK PM Meubelair TA 2016', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '103', 'Kursi Rapat (K1)', '2016', '1', '1435000', '1435000', NULL, '-430500', '-287000', '717500', 'Sesditjen Farmalkes', 'BN.06.02/3/4387/2016', 'TK PM Meubelair TA 2016', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '104', 'Kursi Rapat (K1)', '2016', '1', '1435000', '1435000', NULL, '-430500', '-287000', '717500', 'Sesditjen Farmalkes', 'BN.06.02/3/4387/2016', 'TK PM Meubelair TA 2016', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '105', 'Kursi Rapat (K1)', '2016', '1', '1435000', '1435000', NULL, '-430500', '-287000', '717500', 'Sesditjen Farmalkes', 'BN.06.02/3/4387/2016', 'TK PM Meubelair TA 2016', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '106', 'Kursi Rapat (K1)', '2016', '1', '1435000', '1435000', NULL, '-430500', '-287000', '717500', 'Sesditjen Farmalkes', 'BN.06.02/3/4387/2016', 'TK PM Meubelair TA 2016', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '107', 'Kursi Rapat (K1)', '2016', '1', '1435000', '1435000', NULL, '-430500', '-287000', '717500', 'Sesditjen Farmalkes', 'BN.06.02/3/4387/2016', 'TK PM Meubelair TA 2016', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '11', 'ERGOTEC', '2004', '1', '900000', '780000', '-780000', NULL, NULL, '0', 'APBN 2004', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '12', 'ERGOTEC', '2004', '1', '900000', '780000', '-780000', NULL, NULL, '0', 'APBN 2004', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '13', 'ERGOTEC', '2004', '1', '900000', '780000', '-780000', NULL, NULL, '0', 'APBN 2004', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '14', 'ERGOTEC', '2004', '1', '900000', '780000', '-780000', NULL, NULL, '0', 'APBN 2004', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '15', 'RAKUDA', '2002', '1', '1100000', '1100000', '-1100000', NULL, '0', '0', 'APBN 2005', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '16', 'RAKUDA', '2002', '1', '1100000', '1100000', '-1100000', NULL, '0', '0', 'APBN 2005', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '17', 'RAKUDA', '2002', '1', '1100000', '1100000', '-1100000', NULL, '0', '0', 'APBN 2005', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '18', 'Rakuda', '2002', '1', '46000', '46000', '-46000', NULL, '0', '0', 'Hibah Itjen', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '19', 'Rakuda', '2002', '1', '46000', '46000', '-46000', NULL, NULL, '0', 'Hibah Itjen', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '2', 'RAKUDA', '2005', '1', '660000', '660000', '-660000', NULL, NULL, '0', 'APBN 2005', '', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '20', 'Rakuda', '2002', '1', '46000', '46000', '-46000', NULL, NULL, '0', 'Hibah Itjen', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '21', 'Rakuda', '2002', '1', '46000', '46000', '-46000', NULL, NULL, '0', 'Hibah Itjen', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '22', 'Rakuda', '2002', '1', '46000', '46000', '-46000', NULL, NULL, '0', 'Hibah Itjen', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '23', 'Rakuda', '2002', '1', '46000', '46000', '-46000', NULL, NULL, '0', 'Hibah Itjen', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '24', 'Rakuda', '2002', '1', '46000', '46000', '-46000', NULL, NULL, '0', 'Hibah Itjen', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '25', 'Rakuda', '2002', '1', '46000', '46000', '-46000', NULL, NULL, '0', 'Hibah Itjen', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '26', 'Rakuda', '2002', '1', '46000', '46000', '-46000', NULL, NULL, '0', 'Hibah Itjen', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '27', 'Rakuda', '2002', '1', '46000', '46000', '-46000', NULL, NULL, '0', 'Hibah Itjen', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '28', 'Rakuda', '2002', '1', '46000', '46000', '-46000', NULL, NULL, '0', 'Hibah Itjen', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '29', 'Rakuda', '2002', '1', '46000', '46000', '-46000', NULL, NULL, '0', 'Hibah Itjen', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '3', 'RAKUDA', '2005', '1', '660000', '660000', '-660000', NULL, '0', '0', 'APBN 2005', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '30', 'Rakuda', '2002', '1', '46000', '46000', '-46000', NULL, NULL, '0', 'Hibah Itjen', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '31', 'Kingdom/Staf', '2004', '1', '497500', '432000', '-432000', NULL, '0', '0', 'APBN 2004', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '32', 'Kingdom/Staf', '2004', '1', '497500', '432000', '-432000', NULL, '0', '0', 'APBN 2004', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '33', 'Kingdom/Staf', '2004', '1', '497500', '432000', '-432000', NULL, NULL, '0', 'APBN 2004', '', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '34', 'Kingdom/Staf', '2004', '1', '497500', '432000', '-432000', NULL, NULL, '0', 'APBN 2004', '', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '35', 'Kingdom/Staf', '2004', '1', '497500', '432000', '-432000', NULL, NULL, '0', 'APBN 2004', '', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '36', 'Kingdom/Staf', '2004', '1', '497500', '432000', '-432000', NULL, NULL, '0', 'APBN 2004', '', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '37', 'Kingdom/Staf', '2004', '1', '497500', '432000', '-432000', NULL, NULL, '0', 'APBN 2004', '', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '38', 'Kingdom/Staf', '2004', '1', '497500', '432000', '-432000', NULL, NULL, '0', 'APBN 2004', '', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '39', 'Kingdom/Staf', '2004', '1', '497500', '432000', '-432000', NULL, NULL, '0', 'APBN 2004', '', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '4', 'RAKUDA', '2005', '1', '660000', '660000', '-660000', NULL, '0', '0', 'APBN 2005', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '40', 'Kingdom/Staf', '2004', '1', '497500', '432000', '-432000', NULL, NULL, '0', 'APBN 2004', '', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '41', 'Kursi Rapat/Futura', '2002', '1', '147000', '147000', '-147000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '42', 'Kursi Rapat/Futura', '2002', '1', '147000', '147000', '-147000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '43', 'Kursi Rapat/Futura', '2002', '1', '147000', '147000', '-147000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '44', 'Kursi Rapat/Futura', '2002', '1', '147000', '147000', '-147000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '45', 'Kursi Rapat/Futura', '2002', '1', '147000', '147000', '-147000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '46', 'Kursi Rapat/Futura', '2002', '1', '147000', '147000', '-147000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '47', 'Kursi Rapat/Futura', '2002', '1', '147000', '147000', '-147000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '48', 'Kursi Rapat/Futura', '2002', '1', '147000', '147000', '-147000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '49', 'Kursi Rapat/Futura', '2002', '1', '147000', '147000', '-147000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '5', 'ICHIKO', '2002', '1', '475000', '402000', '-402000', NULL, NULL, '0', 'APBN 2002', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '50', 'Kursi Rapat/Futura', '2002', '1', '147000', '147000', '-147000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '51', 'Lokal', '2013', '1', '1166000', '1166000', NULL, '-1119360', '-46640', '0', 'setditjen', 'KN.02.03/4/782/2013', 'Kursi Model A/Rapat', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '52', 'Lokal', '2013', '1', '1166000', '1166000', NULL, '-1119360', '-46640', '0', 'setditjen', 'KN.02.03/4/782/2013', 'Kursi Model A/Rapat', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '53', 'Lokal', '2013', '1', '1166000', '1166000', NULL, '-1119360', '-46640', '0', 'setditjen', 'KN.02.03/4/782/2013', 'Kursi Model A/Rapat', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '54', 'Lokal', '2013', '1', '1166000', '1166000', NULL, '-1119360', '-46640', '0', 'setditjen', 'KN.02.03/4/782/2013', 'Kursi Model A/Rapat', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '55', 'Lokal', '2013', '1', '1166000', '1166000', NULL, '-1119360', '-46640', '0', 'setditjen', 'KN.02.03/4/782/2013', 'Kursi Model A/Rapat', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '56', 'Lokal', '2013', '1', '1166000', '1166000', NULL, '-1119360', '-46640', '0', 'setditjen', 'KN.02.03/4/782/2013', 'Kursi Model A/Rapat', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '57', 'Lokal', '2013', '1', '1166000', '1166000', NULL, '-1119360', '-46640', '0', 'setditjen', 'KN.02.03/4/782/2013', 'Kursi Model A/Rapat', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '58', 'Lokal', '2013', '1', '1166000', '1166000', NULL, '-1119360', '-46640', '0', 'setditjen', 'KN.02.03/4/782/2013', 'Kursi Model A/Rapat', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '59', 'Lokal', '2013', '1', '1485000', '1485000', NULL, '-1425600', '-59400', '0', 'setditjen', 'KN.02.03/4/782/2013', 'Kursi Model M/Rapat Es.II', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '6', 'ICHIKO', '2002', '1', '475000', '402000', '-402000', NULL, NULL, '0', 'APBN 2002', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '60', 'Lokal', '2013', '1', '1485000', '1485000', NULL, '-1425600', '-59400', '0', 'setditjen', 'KN.02.03/4/782/2013', 'Kursi Model M/Rapat Es.II', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '61', 'Lokal', '2013', '1', '1485000', '1485000', NULL, '-1425600', '-59400', '0', 'setditjen', 'KN.02.03/4/782/2013', 'Kursi Model M/Rapat Es.II', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '62', 'Lokal', '2013', '1', '1485000', '1485000', NULL, '-1425600', '-59400', '0', 'setditjen', 'KN.02.03/4/782/2013', 'Kursi Model M/Rapat Es.II', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '63', 'Lokal', '2013', '1', '1485000', '1485000', NULL, '-1425600', '-59400', '0', 'setditjen', 'KN.02.03/4/782/2013', 'Kursi Model M/Rapat Es.II', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '64', 'Lokal', '2013', '1', '1485000', '1485000', NULL, '-1425600', '-59400', '0', 'setditjen', 'KN.02.03/4/782/2013', 'Kursi Model M/Rapat Es.II', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '65', 'Lokal', '2013', '1', '1485000', '1485000', NULL, '-1425600', '-59400', '0', 'setditjen', 'KN.02.03/4/782/2013', 'Kursi Model M/Rapat Es.II', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '66', 'lokal', '2013', '1', '1650000', '1650000', NULL, '-1584000', '-66000', '0', 'setditjen', 'KN.02.03/4/782/2013', 'Kursi Model C / Es.II', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '67', 'lokal', '2013', '1', '632500', '632500', NULL, '-607200', '-25300', '0', 'setditjen', 'KN.02.03/4/782/2013', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '68', 'lokal', '2013', '1', '632500', '632500', NULL, '-607200', '-25300', '0', 'setditjen', 'KN.02.03/4/782/2013', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '69', 'lokal', '2013', '1', '632500', '632500', NULL, '-607200', '-25300', '0', 'setditjen', 'KN.02.03/4/782/2013', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '7', 'YESTINE', '2002', '1', '1240000', '1049000', '-1049000', NULL, NULL, '0', 'APBN 2002', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '70', 'lokal', '2013', '1', '632500', '632500', NULL, '-607200', '-25300', '0', 'setditjen', 'KN.02.03/4/782/2013', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '71', 'lokal', '2013', '1', '632500', '632500', NULL, '-607200', '-25300', '0', 'setditjen', 'KN.02.03/4/782/2013', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '72', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '73', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '74', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '75', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '76', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '77', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '78', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '79', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '8', 'YESTINE', '2003', '1', '727000', '620000', '-620000', NULL, NULL, '0', 'DIK 2003', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '80', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '81', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '82', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '83', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '84', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '85', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '86', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '87', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '88', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '89', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '9', 'YESTINE', '2003', '1', '727000', '620000', '-620000', NULL, NULL, '0', 'DIK 2003', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '90', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-651252', '-431808', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '91', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-651252', '-431808', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '92', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-651252', '-431808', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '93', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-651252', '-431808', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '94', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '95', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '96', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '97', 'ERGOTEC : 837 S', '2014', '1', '1203400', '1203400', NULL, '-722040', '-361020', '120340', 'Sesditjen farmalkes', 'Kontrak KN0101/PK.03/172.3/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '98', 'Kursi Rapat (K1)', '2016', '1', '1435000', '1435000', NULL, '-430500', '-287000', '717500', 'Sesditjen Farmalkes', 'BN.06.02/3/4387/2016', 'TK PM Meubelair TA 2016', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201003', 'Kursi Besi/Metal', '99', 'Kursi Rapat (K1)', '2016', '1', '1435000', '1435000', NULL, '-430500', '-287000', '717500', 'Sesditjen Farmalkes', 'BN.06.02/3/4387/2016', 'TK PM Meubelair TA 2016', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201005', 'Sice', '1', 'Morales', '2002', '1', '4980000', '3829000', '-3829000', NULL, NULL, '0', 'DIK 2002', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201005', 'Sice', '2', 'Ess.III', '2005', '1', '2332000', '2332000', '-2332000', NULL, NULL, '0', 'DIPA 2005', '', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201005', 'Sice', '3', 'Ess.III', '2005', '1', '2332000', '2332000', '-2332000', NULL, NULL, '0', 'DIPA 2005', '', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201005', 'Sice', '4', 'Ess.III', '2005', '1', '2332000', '2332000', '-2332000', NULL, NULL, '0', 'DIPA 2005', '', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201005', 'Sice', '5', 'Vinoti', '2016', '1', '9900000', '9900000', NULL, '-2970000', '-1980000', '4950000', 'Sesditjen Farmalkes', 'BN.06.02/3/4387/2016', 'TK PM Meubelair TA 2016', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201005', 'Sice', '6', 'Vinoti', '2016', '1', '5450000', '5450000', NULL, '-1635000', '-1090000', '2725000', 'Sesditjen Farmalkes', 'BN.06.02/3/4387/2016', 'TK PM Meubelair TA 2016', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201008', 'Meja Rapat', '1', '', '2005', '1', '4510000', '4510000', '-4510000', NULL, NULL, '0', 'DIPA 2005', '', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201008', 'Meja Rapat', '2', 'lokal', '2013', '1', '5720000', '5720000', NULL, '-5491200', '-228800', '0', 'setditjen', 'KN.02.03/4/782/2013', 'Meja Rapat C/Panjang', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201008', 'Meja Rapat', '3', 'lokal', '2013', '1', '2491500', '2491500', NULL, '-2391840', '-99660', '0', 'setditjen', 'KN.02.03/4/782/2013', 'Meja Rapat Model D/ Bulat', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201028', 'Workstation', '1', 'Work Station (WS4)', '2016', '1', '13400000', '13400000', NULL, '-4020000', '-2680000', '6700000', 'Sesditjen Farmalkes', 'BN.06.02/3/4387/2016', 'TK PM Meubelair TA 2016', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050201999', 'Meubelair Lainnya', '1', '-', '2018', '1', '42900000', '42900000', NULL, '-4290000', NULL, '38610000', 'CV.Putra Bathuphat Mandiri', 'Kuitansi No.018/KW-PBM/XII/2018', 'Mebeler Ruang Direktur', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050202003', 'Jam Elektronik', '1', 'Lokal', '2002', '1', '85000', '85000', '-85000', NULL, '0', '0', 'Hibah Biro Umum', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050202003', 'Jam Elektronik', '2', 'Lokal', '2002', '1', '85000', '85000', '-85000', NULL, '0', '0', 'Hibah Biro Umum', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050202003', 'Jam Elektronik', '3', '', '2004', '1', '375000', '244000', '-244000', NULL, NULL, '0', 'DIK 2004', '', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050202003', 'Jam Elektronik', '4', '', '2004', '1', '375000', '244000', '-244000', NULL, NULL, '0', 'DIK 2004', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050204001', 'Lemari Es', '1', 'Sanyo 1 Pintu', '2005', '1', '1870000', '1870000', '-1870000', NULL, NULL, '0', 'DIPA 2005', '', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050204004', 'A.C. Split', '1', 'LG', '2002', '1', '6904000', '3889000', '-3889000', NULL, NULL, '0', 'APBN 2002', '', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050206002', 'Televisi', '1', 'Sharp', '2005', '1', '4485000', '4485000', '-4485000', NULL, NULL, '0', 'DIPA 2005', '', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050206002', 'Televisi', '2', 'LG Standart', '2013', '1', '10450000', '10450000', NULL, '-10450000', NULL, '0', 'APBN 2013', '30810', 'Baru', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050206002', 'Televisi', '3', 'Samsung', '2014', '1', '8371000', '8371000', NULL, '-5022600', '-2511300', '837100', 'Sesditjen Farmalkes', 'BAPHP KN.01.01/4/0475/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050206002', 'Televisi', '4', 'Samsung 65\"', '2018', '1', '32717000', '32717000', NULL, '-3271700', NULL, '29445300', 'PT. Mitsindo Visual Pratama', 'KN.01.04/005/OLDAT.2/3506/2018;\'17-10-2018', 'Tv samsung di ruang direktur', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050206002', 'Televisi', '5', 'Samsung 65\"', '2018', '1', '32717000', '32717000', NULL, '-3271700', NULL, '29445300', 'PT. Mitsindo Visual Pratama', 'KN.01.04/005/OLDAT.2/3506/2018;\'17-10-2018', 'Tv samsung di ruang direktur', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050206004', 'Tape Recorder (Alat Rumah Tangga Lainnya ( Home Use ))', '1', 'Polytron', '2005', '1', '625000', '625000', '-625000', NULL, '0', '0', 'DIPA 2005', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050206046', 'Handy Cam', '1', 'Mini DV Kamera Recorder', '2006', '1', '43700000', '43700000', '-43700000', NULL, '0', '0', 'BAP barang (CV Citra Media', 'YF.01.01.IIA.023', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050206046', 'Handy Cam', '2', '', '2007', '1', '7700000', '7700000', '-7700000', NULL, NULL, '0', 'APBN 2007', 'SP2D 774039F TGL 09-', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050206056', 'Karpet', '1', 'Lokal', '2002', '1', '7266000', '5586000', '-5586000', NULL, NULL, '0', 'APBN 2002', '', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050206056', 'Karpet', '2', '', '2016', '1', '44179520', '44179520', NULL, '-26507712', NULL, '17671808', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050206058', 'Gordyin/Kray', '1', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050206058', 'Gordyin/Kray', '10', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050206058', 'Gordyin/Kray', '11', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050206058', 'Gordyin/Kray', '12', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050206058', 'Gordyin/Kray', '13', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050206058', 'Gordyin/Kray', '14', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050206058', 'Gordyin/Kray', '15', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050206058', 'Gordyin/Kray', '16', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050206058', 'Gordyin/Kray', '17', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050206058', 'Gordyin/Kray', '18', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050206058', 'Gordyin/Kray', '19', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050206058', 'Gordyin/Kray', '2', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050206058', 'Gordyin/Kray', '20', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050206058', 'Gordyin/Kray', '21', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050206058', 'Gordyin/Kray', '22', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050206058', 'Gordyin/Kray', '23', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050206058', 'Gordyin/Kray', '24', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050206058', 'Gordyin/Kray', '25', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050206058', 'Gordyin/Kray', '26', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050206058', 'Gordyin/Kray', '27', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050206058', 'Gordyin/Kray', '28', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050206058', 'Gordyin/Kray', '29', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050206058', 'Gordyin/Kray', '3', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050206058', 'Gordyin/Kray', '30', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050206058', 'Gordyin/Kray', '31', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050206058', 'Gordyin/Kray', '4', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050206058', 'Gordyin/Kray', '5', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050206058', 'Gordyin/Kray', '6', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050206058', 'Gordyin/Kray', '7', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050206058', 'Gordyin/Kray', '8', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3050206058', 'Gordyin/Kray', '9', '', '2016', '1', '4295500', '4295500', NULL, '-2577300', NULL, '1718200', 'CV.Cahaya Bukit Mas', 'BN.01.02/05/SARANA-KANTOR/10/2016', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3060102003', 'Camera Electronic', '1', '', '2007', '1', '4700000', '4700000', '-4700000', NULL, '0', '0', 'APBN 2007', 'SP2D 774039F TGL 09-', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3060102012', 'Video Monitor', '1', 'LG 75\" With VIA Campus KRAMER', '2018', '1', '165593862', '165593862', NULL, '-16559386', NULL, '149034476', 'PT. Mitsindo Visual Pratama', 'KN.01.04/005/VWALL/3505/2018', 'Video Wall', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3060201003', 'Pesawat Telephone', '1', 'Toriphone', '2002', '1', '196800', '196800', '-196800', NULL, NULL, '0', 'APBN 2002', '', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3060201003', 'Pesawat Telephone', '2', 'Toriphone', '2002', '1', '196800', '196800', '-196800', NULL, NULL, '0', 'APBN 2002', '', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3060201003', 'Pesawat Telephone', '3', 'Toriphone', '2002', '1', '196800', '196800', '-196800', NULL, NULL, '0', 'APBN 2002', '', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3060201010', 'Facsimile', '1', 'Panasonic', '2002', '1', '3850000', '1735000', '-1735000', NULL, NULL, '0', 'APBN 2002', '', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3070102032', 'Bracket Holder', '1', 'Bracket Universal', '2009', '1', '1188000', '1188000', '-831600', '-356400', NULL, '0', 'PT. Complus Sistem Solusi', 'PL.01.01/3/168/2009', 'VGA output/input 15 M', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3080141251', 'Stabilizer/UPS', '1', 'LAPLACE', '2010', '1', '1625000', '1625000', '-507813', '-1117189', '2', '0', 'APBN 2010', '118/F/DUKXI/2010', 'CV. DELPINDO UTAMA KARYA', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3080141251', 'Stabilizer/UPS', '2', 'LAPLACE', '2010', '1', '1625000', '1625000', '-507813', '-1117189', '2', '0', 'APBN 2010', '118/F/DUKXI/2010', 'CV. DELPINDO UTAMA KARYA', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3080141251', 'Stabilizer/UPS', '3', 'LAPLACE', '2010', '1', '1625000', '1625000', '-507813', '-1117189', '2', '0', 'APBN 2010', '118/F/DUKXI/2010', 'CV. DELPINDO UTAMA KARYA', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3080141251', 'Stabilizer/UPS', '4', 'LAPLACE', '2010', '1', '1625000', '1625000', '-507813', '-1117189', '2', '0', 'APBN 2010', '118/F/DUKXI/2010', 'CV. DELPINDO UTAMA KARYA', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3080305002', 'Uninterupted Power Supply (UPS)', '1', 'UPS', '2013', '1', '2520000', '2520000', NULL, '-1008000', NULL, '1512000', 'APBN 2013', 'CV. Harta Gemilang', 'CV. Harta Gemilang', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3090402031', 'Kamera Digital', '1', 'NIKON', '2011', '1', '9992400', '9992400', '-3747150', '-6245250', NULL, '0', 'APBN', 'PL.01.01.05/LCD/13/IX/2011', 'Pembelian 2011', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '1', 'Compaq', '2002', '1', '12348400', '5565000', '-5565000', NULL, NULL, '0', 'APBN 2002', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '10', 'IBM', '2004', '1', '12430000', '6888000', '-6888000', NULL, NULL, '0', 'APBN 2004', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '11', 'IBM', '2005', '1', '12005400', '12005400', '-12005400', NULL, '0', '0', 'DIPA 2005', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '12', 'IBM', '2005', '1', '12005400', '12005400', '-12005400', NULL, '0', '0', 'DIPA 2005', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '13', '', '2006', '1', '14750000', '14750000', '-14750000', NULL, NULL, '0', 'BAP barang (CV Bina Utama)', 'YF.01.01.IIA.042/03-', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '14', '', '2006', '1', '8000000', '8000000', '-8000000', NULL, '0', '0', 'BAP barang (CV Citra Media', 'YF.01.02.IIA.025/30-', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '15', '', '2006', '1', '8000000', '8000000', '-8000000', NULL, NULL, '0', 'BAP barang (CV Citra Media', 'YF.01.02.IIA.025/30-', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '16', 'HP COMPAQ DX 2310', '2009', '1', '8419500', '8419500', '-7367063', '-1052437', '0', '0', 'PT. Complus Sistem Solusi', 'PL.01.01/3/168/2009', 'Inter Dual core E2200', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '17', 'HP COMPAQ DX 2310', '2009', '1', '8419500', '8419500', '-7367063', '-1052437', '0', '0', 'PT. Complus Sistem Solusi', 'PL.01.01/3/168/2009', 'Inter Dual core E2200', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '18', 'HP COMPAQ DX 2310', '2009', '1', '8419500', '8419500', '-7367063', '-1052437', '0', '0', 'PT. Complus Sistem Solusi', 'PL.01.01/3/168/2009', 'Inter Dual core E2200', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '19', '200-5314D', '2011', '1', '8965000', '8965000', '-3361875', '-5603125', NULL, '0', 'APBN', 'pl.01.01.05/apbd/04/xi/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '2', 'Compaq', '2002', '1', '12348400', '5565000', '-5565000', NULL, NULL, '0', 'APBN 2002', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '20', '200-5314D', '2011', '1', '8965000', '8965000', '-3361875', '-5603125', NULL, '0', 'APBN', 'pl.01.01.05/apbd/04/xi/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '21', '200-5314D', '2011', '1', '8965000', '8965000', '-3361875', '-5603125', NULL, '0', 'APBN', 'pl.01.01.05/apbd/04/xi/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '22', '200-5314D', '2011', '1', '8965000', '8965000', '-3361875', '-5603125', NULL, '0', 'APBN', 'pl.01.01.05/apbd/04/xi/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '23', '200-5314D', '2011', '1', '8965000', '8965000', '-3361875', '-5603125', NULL, '0', 'APBN', 'pl.01.01.05/apbd/04/xi/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '24', '200-5314D', '2011', '1', '8965000', '8965000', '-3361875', '-5603125', NULL, '0', 'APBN', 'pl.01.01.05/apbd/04/xi/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '25', '200-5314D', '2011', '1', '8965000', '8965000', '-3361875', '-5603125', NULL, '0', 'APBN', 'pl.01.01.05/apbd/04/xi/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '26', '200-5314D', '2011', '1', '8965000', '8965000', '-3361875', '-5603125', NULL, '0', 'APBN', 'pl.01.01.05/apbd/04/xi/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '27', '200-5314D', '2011', '1', '8965000', '8965000', '-3361875', '-5603125', NULL, '0', 'APBN', 'pl.01.01.05/apbd/04/xi/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '28', '200-5314D', '2011', '1', '8965000', '8965000', '-3361875', '-5603125', NULL, '0', 'APBN', 'pl.01.01.05/apbd/04/xi/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '29', '200-5314D', '2011', '1', '8965000', '8965000', '-3361875', '-5603125', NULL, '0', 'APBN', 'pl.01.01.05/apbd/04/xi/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '3', 'Compaq', '2003', '1', '12348400', '5565000', '-5565000', NULL, '0', '0', 'APBN 2002', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '30', '200-5314D', '2011', '1', '8965000', '8965000', '-3361875', '-5603125', NULL, '0', 'APBN', 'pl.01.01.05/apbd/04/xi/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '31', '200-5314D', '2011', '1', '8965000', '8965000', '-3361875', '-5603125', NULL, '0', 'APBN', 'pl.01.01.05/apbd/04/xi/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '32', '200-5314D', '2011', '1', '8965000', '8965000', '-3361875', '-5603125', NULL, '0', 'APBN', 'pl.01.01.05/apbd/04/xi/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '33', '200-5314D', '2011', '1', '8965000', '8965000', '-3361875', '-5603125', NULL, '0', 'APBN', 'pl.01.01.05/apbd/04/xi/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '34', 'PAVILION 20-B110D ALL IN', '2013', '1', '8226026', '8226026', NULL, '-8226027', '1', '0', 'APBN 13', 'BN.01.03/05/APD/10/2013', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '35', 'PAVILION 20-B110D ALL IN', '2013', '1', '8226026', '8226026', NULL, '-8226027', '1', '0', 'APBN 13', 'BN.01.03/05/APD/10/2013', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '36', 'PAVILION 20-B110D ALL IN', '2013', '1', '8226026', '8226026', NULL, '-8226027', '1', '0', 'APBN 13', 'BN.01.03/05/APD/10/2013', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '37', 'PAVILION 20-B110D ALL IN', '2013', '1', '8226026', '8226026', NULL, '-8226027', '1', '0', 'APBN 13', 'BN.01.03/05/APD/10/2013', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '38', 'PAVILION 20-B110D ALL IN', '2013', '1', '8226026', '8226026', NULL, '-8226027', '1', '0', 'APBN 13', 'BN.01.03/05/APD/10/2013', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '39', 'PAVILION 20-B110D ALL IN', '2013', '1', '8226026', '8226026', NULL, '-8226027', '1', '0', 'APBN 13', 'BN.01.03/05/APD/10/2013', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '4', 'Compaq', '2003', '1', '12348400', '5565000', '-5565000', NULL, '0', '0', 'APBN 2002', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '40', 'PAVILION 20-B110D ALL IN', '2013', '1', '8226026', '8226026', NULL, '-8226027', '1', '0', 'APBN 13', 'BN.01.03/05/APD/10/2013', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '41', 'PAVILION 20-B110D ALL IN', '2013', '1', '8226026', '8226026', NULL, '-8226027', '1', '0', 'APBN 13', 'BN.01.03/05/APD/10/2013', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '42', 'PAVILION 20-B110D ALL IN', '2013', '1', '8226026', '8226026', NULL, '-8226027', '1', '0', 'APBN 13', 'BN.01.03/05/APD/10/2013', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '43', 'PAVILION 20-B110D ALL IN', '2013', '1', '8226026', '8226026', NULL, '-8226027', '1', '0', 'APBN 13', 'BN.01.03/05/APD/10/2013', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '44', 'PAVILION 20-B110D ALL IN', '2013', '1', '8226026', '8226026', NULL, '-8226027', '1', '0', 'APBN 13', 'BN.01.03/05/APD/10/2013', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '45', 'PAVILION 20-B110D ALL IN', '2013', '1', '8226026', '8226026', NULL, '-8226027', '1', '0', 'APBN 13', 'BN.01.03/05/APD/10/2013', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '46', 'HP PAVILION 20-a210d ALL-IN', '2014', '1', '8863250', '8863250', NULL, '-8863250', NULL, '0', 'CV.Cahaya Bukit Mas', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '47', 'HP PAVILION 20-a210d ALL-IN', '2014', '1', '8863250', '8863250', NULL, '-8863250', NULL, '0', 'CV.Cahaya Bukit Mas', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '48', 'HP PAVILION 20-a210d ALL-IN', '2014', '1', '8863250', '8863250', NULL, '-8863250', NULL, '0', 'CV.Cahaya Bukit Mas', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '49', 'HP PAVILION 20-a210d ALL-IN', '2014', '1', '8863250', '8863250', NULL, '-8863250', NULL, '0', 'CV.Cahaya Bukit Mas', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '5', 'Compaq', '2003', '1', '12348400', '5565000', '-5565000', NULL, '0', '0', 'APBN 2002', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '50', 'HP PAVILION 20-a210d ALL-IN', '2014', '1', '8863250', '8863250', NULL, '-8863250', NULL, '0', 'CV.Cahaya Bukit Mas', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '51', 'HP PAVILION 20-a210d ALL-IN', '2014', '1', '8863250', '8863250', NULL, '-8863250', NULL, '0', 'CV.Cahaya Bukit Mas', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '52', 'HP PAVILION 20-a210d ALL-IN', '2014', '1', '8863250', '8863250', NULL, '-8863250', NULL, '0', 'CV.Cahaya Bukit Mas', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '53', 'HP PAVILION 20-a210d ALL-IN', '2014', '1', '8863250', '8863250', NULL, '-8863250', NULL, '0', 'CV.Cahaya Bukit Mas', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '54', 'HP PAVILION 20-a210d ALL-IN', '2014', '1', '8863250', '8863250', NULL, '-8863250', NULL, '0', 'CV.Cahaya Bukit Mas', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '55', 'HP PAVILION 20-a210d ALL-IN', '2014', '1', '8863250', '8863250', NULL, '-8863250', NULL, '0', 'CV.Cahaya Bukit Mas', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '6', 'Compaq', '2003', '1', '12348400', '5565000', '-5565000', NULL, '0', '0', 'APBN 2002', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '66', 'HP-ALL IN ONE 20-r023l', '2015', '1', '9498500', '9498500', NULL, '-8311189', '1', '1187312', 'CV. ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '67', 'HP-ALL IN ONE 20-r023l', '2015', '1', '9498500', '9498500', NULL, '-8311189', '1', '1187312', 'CV. ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '68', 'HP-ALL IN ONE 20-r023l', '2015', '1', '9498500', '9498500', NULL, '-8311189', '1', '1187312', 'CV. ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '69', 'HP-ALL IN ONE 20-r023l', '2015', '1', '9498500', '9498500', NULL, '-8311189', '1', '1187312', 'CV. ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '7', 'Compaq', '2003', '1', '12348400', '5565000', '-5565000', NULL, '0', '0', 'APBN 2002', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '70', 'HP-ALL IN ONE 20-r023l', '2015', '1', '9498500', '9498500', NULL, '-8311189', '1', '1187312', 'CV. ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '71', 'HP-ALL IN ONE 20-r023l', '2015', '1', '9498500', '9498500', NULL, '-8311189', '1', '1187312', 'CV. ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '72', 'HP-ALL IN ONE 20-r023l', '2015', '1', '9498500', '9498500', NULL, '-8311189', '1', '1187312', 'CV. ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '73', 'HP-ALL IN ONE 20-r023l', '2015', '1', '9498500', '9498500', NULL, '-8311189', '1', '1187312', 'CV. ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '74', 'HP-ALL IN ONE 20-r023l', '2015', '1', '9498500', '9498500', NULL, '-8311189', '1', '1187312', 'CV. ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '75', 'HP-ALL IN ONE 20-r023l', '2015', '1', '9498500', '9498500', NULL, '-8311189', '1', '1187312', 'CV. ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '76', 'Lenovo Think Centre E93-YIF', '2014', '1', '14348500', '14348500', NULL, '-7174251', '-7174249', '0', 'Sesditjen Farmalkes', 'KN.01.01/PK.03/109.2/2014', 'BMN Set Farmalkes - Dit Prodisfar', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '77', 'ASUS PC. ALL In One', '2016', '1', '14132940', '14132940', NULL, '-8833088', NULL, '5299852', 'PT.AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '78', 'ASUS PC. ALL In One', '2016', '1', '14132940', '14132940', NULL, '-8833088', NULL, '5299852', 'PT.AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '79', 'ASUS PC. ALL In One', '2016', '1', '14132940', '14132940', NULL, '-8833088', NULL, '5299852', 'PT.AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '8', 'Compaq', '2003', '1', '12350000', '6236000', '-6236000', NULL, '0', '0', 'DIP Bagpro POR 03', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '80', 'ASUS PC. ALL In One', '2016', '1', '14132940', '14132940', NULL, '-8833088', NULL, '5299852', 'PT.AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '81', 'ASUS PC. ALL In One', '2016', '1', '14132940', '14132940', NULL, '-8833088', NULL, '5299852', 'PT.AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '82', 'ASUS PC. ALL In One', '2016', '1', '14132940', '14132940', NULL, '-8833088', NULL, '5299852', 'PT.AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '83', 'ASUS PC. ALL In One', '2016', '1', '14132940', '14132940', NULL, '-8833088', NULL, '5299852', 'PT.AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '84', 'ASUS PC. ALL In One', '2016', '1', '14132940', '14132940', NULL, '-8833088', NULL, '5299852', 'PT.AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '85', 'ASUS PC. ALL In One', '2016', '1', '14132940', '14132940', NULL, '-8833088', NULL, '5299852', 'PT.AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '86', 'ASUS PC. ALL In One', '2016', '1', '14132940', '14132940', NULL, '-8833088', NULL, '5299852', 'PT.AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '9', 'IBM', '2004', '1', '12430000', '6888000', '-6888000', NULL, NULL, '0', 'APBN 2004', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '93', 'Lenovo', '2018', '1', '15819100', '15819100', NULL, '-1977388', NULL, '13841712', 'PT. Air Mas Perkasa', 'KN.01.04/I005/PC-AIO/09/2018', 'Pengadaan Desktop All In One', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '94', 'Lenovo', '2018', '1', '15819100', '15819100', NULL, '-1977388', NULL, '13841712', 'PT. Air Mas Perkasa', 'KN.01.04/I005/PC-AIO/09/2018', 'Pengadaan Desktop All In One', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '95', 'Lenovo', '2018', '1', '15819100', '15819100', NULL, '-1977388', NULL, '13841712', 'PT. Air Mas Perkasa', 'KN.01.04/I005/PC-AIO/09/2018', 'Pengadaan Desktop All In One', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102001', 'P.C Unit', '96', 'Lenovo', '2018', '1', '15819100', '15819100', NULL, '-1977388', NULL, '13841712', 'PT. Air Mas Perkasa', 'KN.01.04/I005/PC-AIO/09/2018', 'Pengadaan Desktop All In One', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102002', 'Lap Top', '1', '', '2007', '1', '19500000', '19500000', '-19500000', NULL, NULL, '0', 'APBN 2007', 'SP2D 774039 TGL09-04', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102002', 'Lap Top', '10', 'HP ELITE BOOK', '2017', '1', '18346130', '18346130', NULL, '-6879798', NULL, '11466332', '-', '-', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102002', 'Lap Top', '11', 'HP ELITE BOOK', '2017', '1', '18346130', '18346130', NULL, '-6879798', NULL, '11466332', '-', '-', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102002', 'Lap Top', '12', 'HP ELITE BOOK', '2017', '1', '18346130', '18346130', NULL, '-6879798', NULL, '11466332', '-', '-', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102002', 'Lap Top', '13', 'HP ELITE BOOK', '2017', '1', '18346130', '18346130', NULL, '-6879798', NULL, '11466332', '-', '-', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102002', 'Lap Top', '14', 'HP ELITE BOOK', '2017', '1', '18346130', '18346130', NULL, '-6879798', NULL, '11466332', '-', '-', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102002', 'Lap Top', '15', 'HP ELITE BOOK', '2017', '1', '18346130', '18346130', NULL, '-6879798', NULL, '11466332', '-', '-', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102002', 'Lap Top', '16', 'HP ELITE BOOK', '2017', '1', '18346130', '18346130', NULL, '-6879798', NULL, '11466332', '-', '-', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102002', 'Lap Top', '17', 'HP ELITE BOOK', '2017', '1', '18346130', '18346130', NULL, '-6879798', NULL, '11466332', '-', '-', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102002', 'Lap Top', '18', 'HP ELITE BOOK', '2017', '1', '18346130', '18346130', NULL, '-6879798', NULL, '11466332', '-', '-', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102002', 'Lap Top', '19', 'HP ELITE BOOK', '2017', '1', '18346130', '18346130', NULL, '-6879798', NULL, '11466332', '-', '-', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102002', 'Lap Top', '2', '', '2007', '1', '19800000', '19800000', '-19800000', NULL, NULL, '0', 'APBN 2007', 'SP2D774038F TGL 09-0', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102002', 'Lap Top', '20', 'HP ELITE BOOK', '2017', '1', '18346130', '18346130', NULL, '-6879798', NULL, '11466332', '-', '-', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102002', 'Lap Top', '21', 'HP ELITE BOOK', '2017', '1', '18346130', '18346130', NULL, '-6879798', NULL, '11466332', '-', '-', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102002', 'Lap Top', '3', '', '2007', '1', '19800000', '19800000', '-19800000', NULL, NULL, '0', 'APBN 2007', 'SP2D774038F TGL 09-0', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102002', 'Lap Top', '4', 'VAIO TAB 11', '2014', '1', '10829500', '10829500', NULL, '-10829502', '2', '0', 'CV.CAHAYA BUKIT MAS', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102002', 'Lap Top', '5', 'VAIO TAB 11', '2014', '1', '10829500', '10829500', NULL, '-10829502', '2', '0', 'CV.CAHAYA BUKIT MAS', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102002', 'Lap Top', '6', 'VAIO TAB 11', '2014', '1', '10829500', '10829500', NULL, '-10829502', '2', '0', 'CV.CAHAYA BUKIT MAS', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102002', 'Lap Top', '7', 'HP ELITE BOOK', '2017', '1', '18346130', '18346130', NULL, '-6879798', NULL, '11466332', '-', '-', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102002', 'Lap Top', '8', 'HP ELITE BOOK', '2017', '1', '18346130', '18346130', NULL, '-6879798', NULL, '11466332', '-', '-', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102002', 'Lap Top', '9', 'HP ELITE BOOK', '2017', '1', '18346130', '18346130', NULL, '-6879798', NULL, '11466332', '-', '-', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102003', 'Note Book', '1', '', '2006', '1', '17800000', '17800000', '-17800000', NULL, '0', '0', 'BAP barang (CV Citra Media', 'YF.01.01.IIA.025/30-', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102003', 'Note Book', '10', 'Fujitsu', '2010', '1', '14045000', '14045000', '-8778125', '-5266875', NULL, '0', 'APBN 2010', '118/F/DUKXI/2010', 'CV Delpindo Utama Karya', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102003', 'Note Book', '11', 'Fujitsu', '2010', '1', '14045000', '14045000', '-8778125', '-5266875', NULL, '0', 'APBN 2010', '118/F/DUKXI/2010', 'CV Delpindo Utama Karya', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102003', 'Note Book', '12', 'Fujitsu', '2010', '1', '14045000', '14045000', '-8778125', '-5266875', NULL, '0', 'APBN 2010', '118/F/DUKXI/2010', 'CV Delpindo Utama Karya', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102003', 'Note Book', '13', 'Fujitsu', '2010', '1', '14045000', '14045000', '-8778125', '-5266875', NULL, '0', 'APBN 2010', '118/F/DUKXI/2010', 'CV Delpindo Utama Karya', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102003', 'Note Book', '14', 'Fujitsu', '2010', '1', '14045000', '14045000', '-8778125', '-5266875', NULL, '0', 'APBN 2010', '118/F/DUKXI/2010', 'CV Delpindo Utama Karya', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102003', 'Note Book', '15', 'fUJITSU', '2011', '1', '15345000', '15345000', '-5754375', '-9590625', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102003', 'Note Book', '16', 'fUJITSU', '2011', '1', '15345000', '15345000', '-5754375', '-9590625', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102003', 'Note Book', '17', 'fUJITSU', '2011', '1', '15345000', '15345000', '-5754375', '-9590625', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102003', 'Note Book', '18', 'fUJITSU', '2011', '1', '15345000', '15345000', '-5754375', '-9590625', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102003', 'Note Book', '19', 'fUJITSU', '2011', '1', '15345000', '15345000', '-5754375', '-9590625', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102003', 'Note Book', '2', 'ACER ASP 2930', '2008', '1', '14280752', '14280752', '-14280752', NULL, NULL, '0', 'CV. DELPINDO UTAMA KARYA', 'PL.01.01/3/275/2008', 'CORE DUO T5800', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102003', 'Note Book', '20', 'fUJITSU', '2011', '1', '15345000', '15345000', '-5754375', '-9590625', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102003', 'Note Book', '21', 'fujitsu 380 black', '2013', '1', '14264542', '14264542', NULL, '-14264543', '1', '0', 'APBN 13', 'BN.01.03/05/APD/10/2013', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102003', 'Note Book', '22', 'fujitsu 380 black', '2013', '1', '14264542', '14264542', NULL, '-14264543', '1', '0', 'APBN 13', 'BN.01.03/05/APD/10/2013', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102003', 'Note Book', '23', 'fujitsu 380 black', '2013', '1', '14264542', '14264542', NULL, '-14264543', '1', '0', 'APBN 13', 'BN.01.03/05/APD/10/2013', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102003', 'Note Book', '24', 'fujitsu 380 black', '2013', '1', '14264542', '14264542', NULL, '-14264543', '1', '0', 'APBN 13', 'BN.01.03/05/APD/10/2013', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102003', 'Note Book', '25', 'fujitsu 380 black', '2013', '1', '14264542', '14264542', NULL, '-14264543', '1', '0', 'APBN 13', 'BN.01.03/05/APD/10/2013', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102003', 'Note Book', '26', 'LENOVO YOGA 500-14IBD', '2015', '1', '12098790', '12098790', NULL, '-10586443', '2', '1512349', 'CV. ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102003', 'Note Book', '27', 'LENOVO YOGA 500-14IBD', '2015', '1', '12098790', '12098790', NULL, '-10586443', '2', '1512349', 'CV. ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102003', 'Note Book', '28', 'LENOVO YOGA 500-14IBD', '2015', '1', '12098790', '12098790', NULL, '-10586443', '2', '1512349', 'CV. ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102003', 'Note Book', '29', 'LENOVO YOGA 500-14IBD', '2015', '1', '12098790', '12098790', NULL, '-10586443', '2', '1512349', 'CV. ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102003', 'Note Book', '3', 'ACER ASP 2930', '2008', '1', '14280752', '14280752', '-14280752', NULL, NULL, '0', 'CV. DELPINDO UTAMA KARYA', 'PL.01.01/3/275/2008', 'CORE DUO T5800', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102003', 'Note Book', '30', 'LENOVO YOGA 500-14IBD', '2015', '1', '12098790', '12098790', NULL, '-10586443', '2', '1512349', 'CV. ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102003', 'Note Book', '31', 'Apple Macbook Air', '2016', '1', '16973000', '16973000', NULL, '-2121625', '-8486500', '6364875', 'Setditjen Farmalkes', 'BAPP KN.01.01/1/0625.5/2016', 'Barang Direktur', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102003', 'Note Book', '32', 'Dell 7373-i78550-8-256-U W10P', '2018', '1', '20291875', '20291875', NULL, '-2536484', NULL, '17755391', 'Setditjen Farmalkes', 'BAST KN.01.02/PT.I/LAPTOP/030/2018', 'Barang Direktur', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102003', 'Note Book', '4', 'ACER ASP 2930', '2008', '1', '14280752', '14280752', '-14280752', NULL, NULL, '0', 'CV. DELPINDO UTAMA KARYA', 'PL.01.01/3/275/2008', 'CORE DUO T5800', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102003', 'Note Book', '5', 'SONY VAIO VGN TZ 160N/B', '2008', '1', '21720000', '21720000', '-21720000', NULL, NULL, '0', 'CV. DELPINDO UTAMA KARYA', 'PL.01.01/3/275/2008', 'CORE 2 DUO U7500', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102003', 'Note Book', '6', 'ACER 6293', '2009', '1', '13097700', '13097700', '-11460488', '-1637212', '0', '0', 'PT. Complus Sistem Solusi', 'PL.01.01/3/168/2009', 'Intel Core 2 Duo Processor', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102003', 'Note Book', '7', 'ACER 6293', '2009', '1', '13097700', '13097700', '-11460488', '-1637212', '0', '0', 'PT. Complus Sistem Solusi', 'PL.01.01/3/168/2009', 'Intel Core 2 Duo Processor', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102003', 'Note Book', '8', 'ACER 6293', '2009', '1', '13097700', '13097700', '-11460488', '-1637212', '0', '0', 'PT. Complus Sistem Solusi', 'PL.01.01/3/168/2009', 'Intel Core 2 Duo Processor', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102003', 'Note Book', '9', 'ACER 6293', '2009', '1', '13097700', '13097700', '-11460488', '-1637212', '0', '0', 'PT. Complus Sistem Solusi', 'PL.01.01/3/168/2009', 'Intel Core 2 Duo Processor', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102009', 'Tablet PC', '1', 'Samsung Galaxy Tab S3 9,7\"', '2018', '1', '10629400', '10629400', NULL, '-2657350', NULL, '7972050', 'APBN', 'KN.01.04/IV/005.05/TAB/1348/2018', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102009', 'Tablet PC', '10', 'Samsung Galaxy Tab S3 9,7\"', '2018', '1', '10629400', '10629400', NULL, '-2657350', NULL, '7972050', 'APBN', 'KN.01.04/IV/005.05/TAB/1348/2018', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102009', 'Tablet PC', '11', 'Samsung Galaxy Tab S3 9,7\"', '2018', '1', '10629400', '10629400', NULL, '-2657350', NULL, '7972050', 'APBN', 'KN.01.04/IV/005.05/TAB/1348/2018', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102009', 'Tablet PC', '12', 'Samsung Galaxy Tab S3 9,7\"', '2018', '1', '10629400', '10629400', NULL, '-2657350', NULL, '7972050', 'APBN', 'KN.01.04/IV/005.05/TAB/1348/2018', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102009', 'Tablet PC', '13', 'Samsung Galaxy Tab S3 9,7\"', '2018', '1', '10629400', '10629400', NULL, '-2657350', NULL, '7972050', 'APBN', 'KN.01.04/IV/005.05/TAB/1348/2018', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102009', 'Tablet PC', '14', 'Samsung Galaxy Tab S3 9,7\"', '2018', '1', '10629400', '10629400', NULL, '-2657350', NULL, '7972050', 'APBN', 'KN.01.04/IV/005.05/TAB/1348/2018', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102009', 'Tablet PC', '15', 'Samsung Galaxy Tab S3 9,7\"', '2018', '1', '10629400', '10629400', NULL, '-2657350', NULL, '7972050', 'APBN', 'KN.01.04/IV/005.05/TAB/1348/2018', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102009', 'Tablet PC', '16', 'Samsung Galaxy Tab S3 9,7\"', '2018', '1', '10629400', '10629400', NULL, '-2657350', NULL, '7972050', 'APBN', 'KN.01.04/IV/005.05/TAB/1348/2018', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102009', 'Tablet PC', '17', 'Samsung Galaxy Tab S3 9,7\"', '2018', '1', '10629400', '10629400', NULL, '-2657350', NULL, '7972050', 'APBN', 'KN.01.04/IV/005.05/TAB/1348/2018', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102009', 'Tablet PC', '18', 'Samsung Galaxy Tab S3 9,7\"', '2018', '1', '10629400', '10629400', NULL, '-2657350', NULL, '7972050', 'APBN', 'KN.01.04/IV/005.05/TAB/1348/2018', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102009', 'Tablet PC', '19', 'Samsung Galaxy Tab S3', '2018', '1', '10500000', '10500000', NULL, '-1312500', NULL, '9187500', 'Setditjen Farmalkes', 'BAST KN.01.02/PT.I/TABS3/026/2018', 'Barang Direktur', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102009', 'Tablet PC', '2', 'Samsung Galaxy Tab S3 9,7\"', '2018', '1', '10629400', '10629400', NULL, '-2657350', NULL, '7972050', 'APBN', 'KN.01.04/IV/005.05/TAB/1348/2018', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102009', 'Tablet PC', '3', 'Samsung Galaxy Tab S3 9,7\"', '2018', '1', '10629400', '10629400', NULL, '-2657350', NULL, '7972050', 'APBN', 'KN.01.04/IV/005.05/TAB/1348/2018', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102009', 'Tablet PC', '4', 'Samsung Galaxy Tab S3 9,7\"', '2018', '1', '10629400', '10629400', NULL, '-2657350', NULL, '7972050', 'APBN', 'KN.01.04/IV/005.05/TAB/1348/2018', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102009', 'Tablet PC', '5', 'Samsung Galaxy Tab S3 9,7\"', '2018', '1', '10629400', '10629400', NULL, '-2657350', NULL, '7972050', 'APBN', 'KN.01.04/IV/005.05/TAB/1348/2018', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102009', 'Tablet PC', '6', 'Samsung Galaxy Tab S3 9,7\"', '2018', '1', '10629400', '10629400', NULL, '-2657350', NULL, '7972050', 'APBN', 'KN.01.04/IV/005.05/TAB/1348/2018', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102009', 'Tablet PC', '7', 'Samsung Galaxy Tab S3 9,7\"', '2018', '1', '10629400', '10629400', NULL, '-2657350', NULL, '7972050', 'APBN', 'KN.01.04/IV/005.05/TAB/1348/2018', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102009', 'Tablet PC', '8', 'Samsung Galaxy Tab S3 9,7\"', '2018', '1', '10629400', '10629400', NULL, '-2657350', NULL, '7972050', 'APBN', 'KN.01.04/IV/005.05/TAB/1348/2018', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100102009', 'Tablet PC', '9', 'Samsung Galaxy Tab S3 9,7\"', '2018', '1', '10629400', '10629400', NULL, '-2657350', NULL, '7972050', 'APBN', 'KN.01.04/IV/005.05/TAB/1348/2018', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '1', 'HP Laserjet 1000', '2002', '1', '3723920', '1679000', '-1679000', NULL, NULL, '0', 'APBN 2002', '', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '10', 'HP LaserJet P1006', '2009', '1', '1389960', '1389960', '-1216215', '-173745', NULL, '0', 'PT. Complus Sistem Solusi', 'PL.01.01/3/168/2009', 'Memory 8 MB RAM', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '100', 'HP LJM203', '2018', '1', '3325100', '3325100', NULL, '-415638', NULL, '2909462', 'PT. Air Mas Perkasa', 'KN.01.04/IV/005/OLDAT.1/3504/2018', 'Alat Pendukung Perkantoran Paket PKM-P1809-1199673', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '101', 'HP LJM203', '2018', '1', '3325100', '3325100', NULL, '-415638', NULL, '2909462', 'PT. Air Mas Perkasa', 'KN.01.04/IV/005/OLDAT.1/3504/2018', 'Alat Pendukung Perkantoran Paket PKM-P1809-1199673', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '11', 'HP LaserJet P1006', '2009', '1', '1389960', '1389960', '-1216215', '-173745', NULL, '0', 'PT. Complus Sistem Solusi', 'PL.01.01/3/168/2009', 'Memory 8 MB RAM', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '12', 'HP OfficeJet 470B', '2009', '1', '2844072', '2844072', '-2488563', '-355509', NULL, '0', 'PT. Complus Sistem Solusi', 'PL.01.01/3/168/2009', 'Portable Printer', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '13', 'HP M 1319F MFP', '2009', '1', '2681910', '2681910', '-2346671', '-335239', '0', '0', 'PT. Complus Sistem Solusi', 'PL.01.01/3/168/2009', 'Printer Multifungsi', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '14', 'HP Portable printer', '2010', '1', '3255000', '3255000', '-2034375', '-1220625', NULL, '0', 'APBN 2010', '118/F/DUKXI/2010', 'CV. Delpindo Utama Karya', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '15', 'HP Portable printer', '2010', '1', '3255000', '3255000', '-2034375', '-1220625', NULL, '0', 'APBN 2010', '118/F/DUKXI/2010', 'CV. Delpindo Utama Karya', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '16', 'HP multifungsi', '2010', '1', '4200000', '4200000', '-2625000', '-1575000', NULL, '0', 'APBN 2010', '118/F/DUKXI/2010', 'CV Delpindo Utama Karya', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '17', 'HP 1102', '2010', '1', '2850000', '2850000', '-1781250', '-1068750', NULL, '0', 'APBN 2010', '118/F/DUKXI/2010', 'CV Delpindo Utama Karya', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '18', 'HP 1102', '2010', '1', '2850000', '2850000', '-1781250', '-1068750', NULL, '0', 'APBN 2010', '118/F/DUKXI/2010', 'CV Delpindo Utama Karya', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '19', 'HP COLOUR PRINTER', '2010', '1', '3205000', '3205000', '-2003125', '-1201875', NULL, '0', 'APBN 2010', '118/F/DUKXI/2010', 'BUSINESS INKJET', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '2', 'HP Laserjet 1000', '2002', '1', '3723920', '1679000', '-1679000', NULL, NULL, '0', 'APBN 2002', '', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '20', 'HP COLOUR PRINTER', '2010', '1', '3205000', '3205000', '-2003125', '-1201875', NULL, '0', 'APBN 2010', '118/F/DUKXI/2010', 'BUSINESS INKJET', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '21', 'HP COLOUR PRINTER', '2010', '1', '3205000', '3205000', '-2003125', '-1201875', NULL, '0', 'APBN 2010', '118/F/DUKXI/2010', 'BUSINESS INKJET', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '22', 'hp.L.m1132', '2011', '1', '1925000', '1925000', '-721875', '-1203125', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '23', 'hp.L.m1132', '2011', '1', '1925000', '1925000', '-721875', '-1203125', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '24', 'hp.L.m1132', '2011', '1', '1925000', '1925000', '-721875', '-1203125', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '25', 'hp.L.m1132', '2011', '1', '1925000', '1925000', '-721875', '-1203125', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '26', 'hp.L.m1132', '2011', '1', '1925000', '1925000', '-721875', '-1203125', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '27', 'P1102', '2011', '1', '1155000', '1155000', '-433125', '-721875', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '28', 'P1102', '2011', '1', '1155000', '1155000', '-433125', '-721875', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '29', 'P1102', '2011', '1', '1155000', '1155000', '-433125', '-721875', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '3', 'Epson LQ 2180', '2002', '1', '7534400', '957000', '-957000', NULL, NULL, '0', 'APBN 2002', '', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '30', 'P1102', '2011', '1', '1155000', '1155000', '-433125', '-721875', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '31', 'P1102', '2011', '1', '1155000', '1155000', '-433125', '-721875', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '32', 'P1102', '2011', '1', '1155000', '1155000', '-433125', '-721875', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '33', 'P1102', '2011', '1', '1155000', '1155000', '-433125', '-721875', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '34', 'P1102', '2011', '1', '1155000', '1155000', '-433125', '-721875', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '35', 'P1102', '2011', '1', '1155000', '1155000', '-433125', '-721875', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '36', 'P1102', '2011', '1', '1155000', '1155000', '-433125', '-721875', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '37', 'P1102', '2011', '1', '1155000', '1155000', '-433125', '-721875', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '38', 'P1102', '2011', '1', '1155000', '1155000', '-433125', '-721875', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '39', 'P1102', '2011', '1', '1155000', '1155000', '-433125', '-721875', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '4', 'HP Ink jet', '2002', '1', '2844800', '1282000', '-1282000', NULL, NULL, '0', 'APBN 2002', '', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '40', 'P1102', '2011', '1', '1155000', '1155000', '-433125', '-721875', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '41', 'P1102', '2011', '1', '1155000', '1155000', '-433125', '-721875', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '42', 'P1102', '2011', '1', '1155000', '1155000', '-433125', '-721875', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '43', 'LX300', '2011', '1', '2530000', '2530000', '-948750', '-1581250', NULL, '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '44', 'LX300', '2011', '1', '2530000', '2530000', '-948750', '-1581250', '0', '0', 'APBN', 'PL.01.01.05/APD/04/XI/2011', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '45', 'HP P1102 W', '2014', '1', '1488300', '1488300', NULL, '-1488302', '2', '0', 'CV.CAHAYA BUKIT MAS', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '46', 'HP P1102 W', '2014', '1', '1488300', '1488300', NULL, '-1488302', '2', '0', 'CV.CAHAYA BUKIT MAS', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '47', 'HP P1102 W', '2014', '1', '1488300', '1488300', NULL, '-1488302', '2', '0', 'CV.CAHAYA BUKIT MAS', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '48', 'HP P1102 W', '2014', '1', '1488300', '1488300', NULL, '-1488302', '2', '0', 'CV.CAHAYA BUKIT MAS', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '49', 'HP P1102 W', '2014', '1', '1488300', '1488300', NULL, '-1488302', '2', '0', 'CV.CAHAYA BUKIT MAS', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '5', 'HP Laserjet 1160', '2005', '1', '3366000', '3366000', '-3366000', NULL, NULL, '0', 'DIPA 2005', '', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '50', 'HP P1102 W', '2014', '1', '1488300', '1488300', NULL, '-1488302', '2', '0', 'CV.CAHAYA BUKIT MAS', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '51', 'HP P1102 W', '2014', '1', '1488300', '1488300', NULL, '-1488302', '2', '0', 'CV.CAHAYA BUKIT MAS', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '52', 'HP P1102 W', '2014', '1', '1488300', '1488300', NULL, '-1488302', '2', '0', 'CV.CAHAYA BUKIT MAS', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '53', 'HP P1102 W', '2014', '1', '1488300', '1488300', NULL, '-1488302', '2', '0', 'CV.CAHAYA BUKIT MAS', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '54', 'HP P1102 W', '2014', '1', '1488300', '1488300', NULL, '-1488302', '2', '0', 'CV.CAHAYA BUKIT MAS', 'BN.01.03/05/4/E-LIC/11/2014', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '6', '', '2006', '1', '2300000', '2300000', '-2300000', NULL, NULL, '0', 'BAp barang (CV Citra Media', 'YF.01.01.IIA.025/30-', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '65', 'Brother MFC-L2700DW', '2015', '1', '3628790', '3628790', NULL, '-3175193', '2', '453599', 'CV.ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '66', 'Brother MFC-L2700DW', '2015', '1', '3628790', '3628790', NULL, '-3175193', '2', '453599', 'CV.ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '67', 'Brother MFC-L2700DW', '2015', '1', '3628790', '3628790', NULL, '-3175193', '2', '453599', 'CV.ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '68', 'Brother MFC-L2700DW', '2015', '1', '3628790', '3628790', NULL, '-3175193', '2', '453599', 'CV.ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '69', 'Brother MFC-L2700DW', '2015', '1', '3628790', '3628790', NULL, '-3175193', '2', '453599', 'CV.ADHI LUCKY MERITAMA', 'TU.03.02/05/AP-ELIC/11/2015', 'ALAT PENUNJANG E LICENSING', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '7', '', '2006', '1', '2300000', '2300000', '-2300000', NULL, NULL, '0', 'BAp barang (CV Citra Media', 'YF.01.01.IIA.025/30-', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '70', 'Canon', '2016', '1', '1402500', '1402500', NULL, '-876563', NULL, '525937', 'PT AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '71', 'Canon', '2016', '1', '1402500', '1402500', NULL, '-876563', NULL, '525937', 'PT AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '72', 'Canon', '2016', '1', '1402500', '1402500', NULL, '-876563', NULL, '525937', 'PT AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '73', 'Canon', '2016', '1', '1402500', '1402500', NULL, '-876563', NULL, '525937', 'PT AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '74', 'Canon', '2016', '1', '1402500', '1402500', NULL, '-876563', NULL, '525937', 'PT AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '75', 'Canon', '2016', '1', '1402500', '1402500', NULL, '-876563', NULL, '525937', 'PT AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '76', 'Canon', '2016', '1', '1402500', '1402500', NULL, '-876563', NULL, '525937', 'PT AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '77', 'Canon', '2016', '1', '1402500', '1402500', NULL, '-876563', NULL, '525937', 'PT AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '78', 'Canon', '2016', '1', '1402500', '1402500', NULL, '-876563', NULL, '525937', 'PT AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '79', 'Canon', '2016', '1', '1402500', '1402500', NULL, '-876563', NULL, '525937', 'PT AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '8', '', '2006', '1', '2300000', '2300000', '-2300000', NULL, NULL, '0', 'BAp barang (CV Citra Media', 'YF.01.01.IIA.025/30-', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '80', 'Canon', '2016', '1', '1402500', '1402500', NULL, '-876563', NULL, '525937', 'PT AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '81', 'Canon', '2016', '1', '1402500', '1402500', NULL, '-876563', NULL, '525937', 'PT AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '82', 'Canon', '2016', '1', '1402500', '1402500', NULL, '-876563', NULL, '525937', 'PT AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '83', 'Canon', '2016', '1', '1402500', '1402500', NULL, '-876563', NULL, '525937', 'PT AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '84', 'Canon', '2016', '1', '1402500', '1402500', NULL, '-876563', NULL, '525937', 'PT AIRMAS PERKASA', 'BN.01.02/05/AP-IZIN/05/2016', '-', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '9', '', '2006', '1', '2300000', '2300000', '-2300000', NULL, NULL, '0', 'BAp barang (CV Citra Media', 'YF.01.01.IIA.025/30-', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '91', 'HP LJM426', '2018', '1', '8797600', '8797600', NULL, '-1099700', NULL, '7697900', 'PT. Air Mas Perkasa', 'KN.01.04/IV/005/OLDAT.1/3504/2018', 'Alat Pendukung Perkantoran Paket PKM-P1809-1199673', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '92', 'HP LJM426', '2018', '1', '8797600', '8797600', NULL, '-1099700', NULL, '7697900', 'PT. Air Mas Perkasa', 'KN.01.04/IV/005/OLDAT.1/3504/2018', 'Alat Pendukung Perkantoran Paket PKM-P1809-1199673', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '93', 'HP LJM426', '2018', '1', '8797600', '8797600', NULL, '-1099700', NULL, '7697900', 'PT. Air Mas Perkasa', 'KN.01.04/IV/005/OLDAT.1/3504/2018', 'Alat Pendukung Perkantoran Paket PKM-P1809-1199673', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '94', 'HP LJM426', '2018', '1', '8797600', '8797600', NULL, '-1099700', NULL, '7697900', 'PT. Air Mas Perkasa', 'KN.01.04/IV/005/OLDAT.1/3504/2018', 'Alat Pendukung Perkantoran Paket PKM-P1809-1199673', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '95', 'HP LJM203', '2018', '1', '3325100', '3325100', NULL, '-415638', NULL, '2909462', 'PT. Air Mas Perkasa', 'KN.01.04/IV/005/OLDAT.1/3504/2018', 'Alat Pendukung Perkantoran Paket PKM-P1809-1199673', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '96', 'HP LJM203', '2018', '1', '3325100', '3325100', NULL, '-415638', NULL, '2909462', 'PT. Air Mas Perkasa', 'KN.01.04/IV/005/OLDAT.1/3504/2018', 'Alat Pendukung Perkantoran Paket PKM-P1809-1199673', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '97', 'HP LJM203', '2018', '1', '3325100', '3325100', NULL, '-415638', NULL, '2909462', 'PT. Air Mas Perkasa', 'KN.01.04/IV/005/OLDAT.1/3504/2018', 'Alat Pendukung Perkantoran Paket PKM-P1809-1199673', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '98', 'HP LJM203', '2018', '1', '3325100', '3325100', NULL, '-415638', NULL, '2909462', 'PT. Air Mas Perkasa', 'KN.01.04/IV/005/OLDAT.1/3504/2018', 'Alat Pendukung Perkantoran Paket PKM-P1809-1199673', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203003', 'Printer (Peralatan Personal Komputer)', '99', 'HP LJM203', '2018', '1', '3325100', '3325100', NULL, '-415638', NULL, '2909462', 'PT. Air Mas Perkasa', 'KN.01.04/IV/005/OLDAT.1/3504/2018', 'Alat Pendukung Perkantoran Paket PKM-P1809-1199673', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203004', 'Scanner (Peralatan Personal Komputer)', '1', '', '2006', '1', '1300000', '1300000', '-1300000', NULL, '0', '0', 'BAP barang (CV Citra Media', 'YF.01.02.IIA.025/30-', '', 'DBR', '3', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203004', 'Scanner (Peralatan Personal Komputer)', '2', 'Panasonic KV-S1028Y', '2018', '1', '16890000', '16890000', NULL, '-2111250', NULL, '14778750', 'PT. Air Mas Perkasa', 'KN.01.04/IV/005/OLDAT.1/3504/2018', 'Alat Pendukung Perkantoran Paket PKM-P1809-1199673', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203004', 'Scanner (Peralatan Personal Komputer)', '3', 'Panasonic KV-S1028Y', '2018', '1', '16890000', '16890000', NULL, '-2111250', NULL, '14778750', 'PT. Air Mas Perkasa', 'KN.01.04/IV/005/OLDAT.1/3504/2018', 'Alat Pendukung Perkantoran Paket PKM-P1809-1199673', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203004', 'Scanner (Peralatan Personal Komputer)', '4', 'Panasonic KV-S1028Y', '2018', '1', '16890000', '16890000', NULL, '-2111250', NULL, '14778750', 'PT. Air Mas Perkasa', 'KN.01.04/IV/005/OLDAT.1/3504/2018', 'Alat Pendukung Perkantoran Paket PKM-P1809-1199673', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203017', 'External/ Portable Hardisk', '1', 'Lokal', '2013', '1', '2600000', '2600000', NULL, '-2600000', NULL, '0', 'APBN 2013', '001/F/HG/II/2013', 'Baru', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203017', 'External/ Portable Hardisk', '2', 'Lokal', '2013', '1', '2600000', '2600000', NULL, '-2600000', NULL, '0', 'APBN 2013', '001/F/HG/II/2013', 'Baru', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203017', 'External/ Portable Hardisk', '3', 'Lokal', '2013', '1', '2600000', '2600000', NULL, '-2600000', NULL, '0', 'APBN 2013', '001/F/HG/II/2013', 'Baru', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203017', 'External/ Portable Hardisk', '4', 'Lokal', '2013', '1', '2600000', '2600000', NULL, '-2600000', NULL, '0', 'APBN 2013', '001/F/HG/II/2013', 'Baru', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100203017', 'External/ Portable Hardisk', '5', 'Lokal', '2013', '1', '2600000', '2600000', NULL, '-2600000', NULL, '0', 'APBN 2013', '001/F/HG/II/2013', 'Baru', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100204001', 'Server', '1', 'intel', '2012', '1', '63499700', '63499700', '-7937463', '-55562237', '0', '0', 'APBN', '201482y', 'pembelian', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100204001', 'Server', '2', 'IBM', '2013', '1', '341165000', '341165000', NULL, '-341165000', NULL, '0', 'APBN 2013', 'BA.KN.01.02/PT.V/STR.1/2013', 'Pusat Data', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100204001', 'Server', '3', 'DELL', '2014', '1', '66000000', '66000000', NULL, '-66000000', NULL, '0', 'CV.Sumber Setia', 'KN.01.02/PI.V/SERVER.1/2014', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100204001', 'Server', '4', 'DELL', '2014', '1', '66000000', '66000000', NULL, '-66000000', NULL, '0', 'CV.Sumber Setia', 'KN.01.02/PI.V/SERVER.1/2014', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '3100204001', 'Server', '5', 'DELL', '2014', '1', '66000000', '66000000', NULL, '-66000000', NULL, '0', 'CV.Sumber Setia', 'KN.01.02/PI.V/SERVER.1/2014', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '6070301001', 'Gedung dan Bangunan Dalam Renovasi', '2', '-', '2018', '1', '162329200', '162329200', NULL, NULL, NULL, '162329200', '', 'KN.01.03/IV/TATA.RKA/10/2018', 'Ruang Direktur', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '8010101001', 'Software Komputer', '1', '-', '2014', '1', '154962500', '154962500', '-58110938', '-96851562', NULL, '0', 'PT.EDI INDONESIA', 'IR.01.03/05/E-PBF/11/2014', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '8010101001', 'Software Komputer', '2', '-', '2014', '1', '89980000', '89980000', '-33742500', '-56237500', NULL, '0', 'PT.EDI', 'IR.01.03/05/LINK/11/2014', 'INTEGRATED SYSTEM', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '8010101001', 'Software Komputer', '3', '', '2014', '1', '84920000', '84920000', '-31845000', '-53075000', NULL, '0', 'PT. EDI', 'IR.01.03/05/PRODIS-NPP/11/2014', '', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '8010101001', 'Software Komputer', '4', '', '2016', '1', '89980000', '89980000', NULL, '-56237500', '0', '33742500', 'PT EDII', 'IR.01.03/05/DASHBOARD/11/2016', 'Dashboard Profar', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '8010101001', 'Software Komputer', '5', '-', '2012', '1', '345000040', '345000040', NULL, NULL, '-345000040', '0', '-', 'KN.02.04/05/1994/2017', 'Sofware', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '8010101001', 'Software Komputer', '7', '', '2018', '1', '179377000', '179377000', NULL, '-22422125', '0', '156954875', '-', 'KN.01.03/ IV/SIS.REPRO/11/2018', 'Sistem Realisasi Produksi', 'DBR', '1', NULL);
INSERT INTO `barang_upload` VALUES ('024070199465983000KP', '8010101001', 'Software Komputer', '8', '', '2018', '1', '76890000', '76890000', NULL, '-9611250', '0', '67278750', '', 'KN.01.03/IV/ SIS.GOTIK/10/2018', 'SiGotik', 'DBR', '1', NULL);

-- ----------------------------
-- Table structure for bast_penggunaan
-- ----------------------------
DROP TABLE IF EXISTS `bast_penggunaan`;
CREATE TABLE `bast_penggunaan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomor` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tanggal` date NULL DEFAULT NULL,
  `nama_pihak_1` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nip_pihak_1` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jabatan_pihak_1` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nama_pihak_2` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nip_pihak_2` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jabatan_pihak_2` varchar(225) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `berupa` varchar(225) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `kondisi` varchar(225) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cms_apicustom
-- ----------------------------
DROP TABLE IF EXISTS `cms_apicustom`;
CREATE TABLE `cms_apicustom`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `permalink` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `tabel` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `aksi` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `kolom` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `orderby` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `sub_query_1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `sql_where` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `parameter` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `method_type` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `parameters` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `responses` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cms_apikey
-- ----------------------------
DROP TABLE IF EXISTS `cms_apikey`;
CREATE TABLE `cms_apikey`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `screetkey` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `hit` int(11) NULL DEFAULT NULL,
  `status` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cms_dashboard
-- ----------------------------
DROP TABLE IF EXISTS `cms_dashboard`;
CREATE TABLE `cms_dashboard`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `id_cms_privileges` int(11) NULL DEFAULT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cms_email_queues
-- ----------------------------
DROP TABLE IF EXISTS `cms_email_queues`;
CREATE TABLE `cms_email_queues`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `send_at` datetime(0) NULL DEFAULT NULL,
  `email_recipient` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email_from_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email_from_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email_cc_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email_subject` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `email_attachments` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `is_sent` tinyint(1) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cms_email_templates
-- ----------------------------
DROP TABLE IF EXISTS `cms_email_templates`;
CREATE TABLE `cms_email_templates`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `subject` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `from_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `from_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `cc_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of cms_email_templates
-- ----------------------------
INSERT INTO `cms_email_templates` VALUES (1, 'Email Template Forgot Password Backend', 'forgot_password_backend', NULL, '<p>Hi,</p><p>Someone requested forgot password, here is your new password : </p><p>[password]</p><p><br></p><p>--</p><p>Regards,</p><p>Admin</p>', '[password]', 'System', 'system@crudbooster.com', NULL, '2019-02-19 04:09:13', NULL);

-- ----------------------------
-- Table structure for cms_logs
-- ----------------------------
DROP TABLE IF EXISTS `cms_logs`;
CREATE TABLE `cms_logs`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ipaddress` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `useragent` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `id_cms_users` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 40 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of cms_logs
-- ----------------------------
INSERT INTO `cms_logs` VALUES (1, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2019-02-19 04:10:04', NULL);
INSERT INTO `cms_logs` VALUES (2, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36', 'http://127.0.0.1:8000/admin/module_generator/delete/12', 'Delete data Barang at Module Generator', '', 1, '2019-02-19 04:17:17', NULL);
INSERT INTO `cms_logs` VALUES (3, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2019-02-19 04:23:45', NULL);
INSERT INTO `cms_logs` VALUES (4, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2019-02-22 02:27:02', NULL);
INSERT INTO `cms_logs` VALUES (5, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2019-02-25 00:51:15', NULL);
INSERT INTO `cms_logs` VALUES (6, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2019-02-25 02:46:26', NULL);
INSERT INTO `cms_logs` VALUES (7, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36', 'http://127.0.0.1:8000/admin/logout', 'admin@crudbooster.com logout', '', 1, '2019-02-25 02:47:12', NULL);
INSERT INTO `cms_logs` VALUES (8, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2019-02-25 02:52:28', NULL);
INSERT INTO `cms_logs` VALUES (9, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36', 'http://127.0.0.1:8000/admin/logout', 'admin@crudbooster.com logout', '', 1, '2019-02-25 02:53:34', NULL);
INSERT INTO `cms_logs` VALUES (10, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2019-02-25 02:57:15', NULL);
INSERT INTO `cms_logs` VALUES (11, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0', 'http://127.0.0.1:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2019-02-25 03:03:55', NULL);
INSERT INTO `cms_logs` VALUES (12, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0', 'http://127.0.0.1:8000/admin/users/add-save', 'Add New Data Admin at Users Management', '', 1, '2019-02-25 03:06:59', NULL);
INSERT INTO `cms_logs` VALUES (13, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0', 'http://127.0.0.1:8000/admin/logout', 'admin@crudbooster.com logout', '', 1, '2019-02-25 03:07:21', NULL);
INSERT INTO `cms_logs` VALUES (14, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0', 'http://127.0.0.1:8000/admin/login', 'admin@easset.com login with IP Address 127.0.0.1', '', 2, '2019-02-25 03:07:29', NULL);
INSERT INTO `cms_logs` VALUES (15, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0', 'http://127.0.0.1:8000/admin/logout', 'admin@easset.com logout', '', 2, '2019-02-25 03:07:37', NULL);
INSERT INTO `cms_logs` VALUES (16, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0', 'http://127.0.0.1:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2019-02-25 03:08:11', NULL);
INSERT INTO `cms_logs` VALUES (17, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0', 'http://127.0.0.1:8000/admin/menu_management/edit-save/2', 'Update data Barang at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr></tbody></table>', 1, '2019-02-25 03:08:29', NULL);
INSERT INTO `cms_logs` VALUES (18, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0', 'http://127.0.0.1:8000/admin/logout', 'admin@crudbooster.com logout', '', 1, '2019-02-25 03:08:42', NULL);
INSERT INTO `cms_logs` VALUES (19, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0', 'http://127.0.0.1:8000/admin/login', 'admin@easset.com login with IP Address 127.0.0.1', '', 2, '2019-02-25 03:08:48', NULL);
INSERT INTO `cms_logs` VALUES (20, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36', 'http://127.0.0.1:8000/admin/logout', 'admin@crudbooster.com logout', '', 1, '2019-02-25 03:11:11', NULL);
INSERT INTO `cms_logs` VALUES (21, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'admin@easset.com login with IP Address 127.0.0.1', '', 2, '2019-02-25 03:11:32', NULL);
INSERT INTO `cms_logs` VALUES (22, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'admin@easset.com login with IP Address 127.0.0.1', '', 2, '2019-02-26 04:38:20', NULL);
INSERT INTO `cms_logs` VALUES (23, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'admin@easset.com login with IP Address 127.0.0.1', '', 2, '2019-03-01 10:59:19', NULL);
INSERT INTO `cms_logs` VALUES (24, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'admin@easset.com login with IP Address 127.0.0.1', '', 2, '2019-03-05 00:21:55', NULL);
INSERT INTO `cms_logs` VALUES (25, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'admin@easset.com login with IP Address 127.0.0.1', '', 2, '2019-03-05 00:32:42', NULL);
INSERT INTO `cms_logs` VALUES (26, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', 'http://127.0.0.1:8000/admin/logout', 'admin@easset.com logout', '', 2, '2019-03-05 00:37:12', NULL);
INSERT INTO `cms_logs` VALUES (27, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', 'http://127.0.0.1:8000/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2019-03-05 00:37:18', NULL);
INSERT INTO `cms_logs` VALUES (28, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', 'http://127.0.0.1:8000/admin/menu_management/edit-save/2', 'Update data List Barang at Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>name</td><td>Barang</td><td>List Barang</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-th-list</td></tr></tbody></table>', 1, '2019-03-05 00:37:58', NULL);
INSERT INTO `cms_logs` VALUES (29, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', 'http://127.0.0.1:8000/admin/menu_management/add-save', 'Add New Data Barang at Menu Management', '', 1, '2019-03-05 00:38:18', NULL);
INSERT INTO `cms_logs` VALUES (30, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', 'http://127.0.0.1:8000/admin/menu_management/add-save', 'Add New Data Upload Barang at Menu Management', '', 1, '2019-03-05 00:38:40', NULL);
INSERT INTO `cms_logs` VALUES (31, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', 'http://127.0.0.1:8000/admin/menu_management/add-save', 'Add New Data Sensus at Menu Management', '', 1, '2019-03-05 00:39:10', NULL);
INSERT INTO `cms_logs` VALUES (32, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', 'http://127.0.0.1:8000/admin/menu_management/add-save', 'Add New Data Pendataan at Menu Management', '', 1, '2019-03-05 00:40:34', NULL);
INSERT INTO `cms_logs` VALUES (33, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', 'http://127.0.0.1:8000/admin/menu_management/add-save', 'Add New Data BAST at Menu Management', '', 1, '2019-03-05 00:41:07', NULL);
INSERT INTO `cms_logs` VALUES (34, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', 'http://127.0.0.1:8000/admin/menu_management/add-save', 'Add New Data Penggunaan at Menu Management', '', 1, '2019-03-05 00:41:31', NULL);
INSERT INTO `cms_logs` VALUES (35, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', 'http://127.0.0.1:8000/admin/menu_management/add-save', 'Add New Data Master at Menu Management', '', 1, '2019-03-05 00:41:50', NULL);
INSERT INTO `cms_logs` VALUES (36, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', 'http://127.0.0.1:8000/admin/menu_management/add-save', 'Add New Data Pegawai at Menu Management', '', 1, '2019-03-05 00:42:14', NULL);
INSERT INTO `cms_logs` VALUES (37, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', 'http://127.0.0.1:8000/admin/menu_management/add-save', 'Add New Data Jabatan at Menu Management', '', 1, '2019-03-05 00:42:36', NULL);
INSERT INTO `cms_logs` VALUES (38, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', 'http://127.0.0.1:8000/admin/menu_management/add-save', 'Add New Data Bagian at Menu Management', '', 1, '2019-03-05 00:43:51', NULL);
INSERT INTO `cms_logs` VALUES (39, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36', 'http://127.0.0.1:8000/admin/menu_management/add-save', 'Add New Data Ruang at Menu Management', '', 1, '2019-03-05 00:44:09', NULL);

-- ----------------------------
-- Table structure for cms_menus
-- ----------------------------
DROP TABLE IF EXISTS `cms_menus`;
CREATE TABLE `cms_menus`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'url',
  `path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `color` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `parent_id` int(11) NULL DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `is_dashboard` tinyint(1) NOT NULL DEFAULT 0,
  `id_cms_privileges` int(11) NULL DEFAULT NULL,
  `sorting` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of cms_menus
-- ----------------------------
INSERT INTO `cms_menus` VALUES (2, 'List Barang', 'Route', 'AdminBarang13ControllerGetIndex', 'normal', 'fa fa-th-list', 3, 1, 0, 1, 1, '2019-02-19 04:17:28', '2019-03-05 00:37:57');
INSERT INTO `cms_menus` VALUES (3, 'Barang', 'URL', '#', 'normal', 'fa fa-th', 0, 1, 0, 1, 1, '2019-03-05 00:38:17', NULL);
INSERT INTO `cms_menus` VALUES (4, 'Upload Barang', 'URL', '#', 'normal', 'fa fa-th-list', 3, 1, 0, 1, 2, '2019-03-05 00:38:40', NULL);
INSERT INTO `cms_menus` VALUES (5, 'Sensus', 'URL', '#', 'normal', 'fa fa-th', 0, 1, 0, 1, 2, '2019-03-05 00:39:10', NULL);
INSERT INTO `cms_menus` VALUES (6, 'Pendataan', 'URL', '#', 'normal', 'fa fa-th-list', 5, 1, 0, 1, 1, '2019-03-05 00:40:34', NULL);
INSERT INTO `cms_menus` VALUES (7, 'BAST', 'URL', '#', 'normal', 'fa fa-th', 0, 1, 0, 1, 3, '2019-03-05 00:41:07', NULL);
INSERT INTO `cms_menus` VALUES (8, 'Penggunaan', 'URL', '#', 'normal', 'fa fa-th-list', 7, 1, 0, 1, 1, '2019-03-05 00:41:31', NULL);
INSERT INTO `cms_menus` VALUES (9, 'Master', 'URL', '#', 'normal', 'fa fa-th', 0, 1, 0, 1, 4, '2019-03-05 00:41:49', NULL);
INSERT INTO `cms_menus` VALUES (10, 'Pegawai', 'URL', '#', 'normal', 'fa fa-th-list', 9, 1, 0, 1, 2, '2019-03-05 00:42:13', NULL);
INSERT INTO `cms_menus` VALUES (11, 'Jabatan', 'URL', '#', 'normal', 'fa fa-th-list', 9, 1, 0, 1, 1, '2019-03-05 00:42:35', NULL);
INSERT INTO `cms_menus` VALUES (12, 'Bagian', 'URL', '#', 'normal', 'fa fa-th-list', 9, 1, 0, 1, 3, '2019-03-05 00:43:51', NULL);
INSERT INTO `cms_menus` VALUES (13, 'Ruang', 'URL', '#', 'normal', 'fa fa-th-list', 9, 1, 0, 1, 4, '2019-03-05 00:44:08', NULL);

-- ----------------------------
-- Table structure for cms_menus_privileges
-- ----------------------------
DROP TABLE IF EXISTS `cms_menus_privileges`;
CREATE TABLE `cms_menus_privileges`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_cms_menus` int(11) NULL DEFAULT NULL,
  `id_cms_privileges` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 29 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of cms_menus_privileges
-- ----------------------------
INSERT INTO `cms_menus_privileges` VALUES (1, 1, 1);
INSERT INTO `cms_menus_privileges` VALUES (5, 2, 2);
INSERT INTO `cms_menus_privileges` VALUES (6, 2, 1);
INSERT INTO `cms_menus_privileges` VALUES (7, 3, 2);
INSERT INTO `cms_menus_privileges` VALUES (8, 3, 1);
INSERT INTO `cms_menus_privileges` VALUES (9, 4, 2);
INSERT INTO `cms_menus_privileges` VALUES (10, 4, 1);
INSERT INTO `cms_menus_privileges` VALUES (11, 5, 2);
INSERT INTO `cms_menus_privileges` VALUES (12, 5, 1);
INSERT INTO `cms_menus_privileges` VALUES (13, 6, 2);
INSERT INTO `cms_menus_privileges` VALUES (14, 6, 1);
INSERT INTO `cms_menus_privileges` VALUES (15, 7, 2);
INSERT INTO `cms_menus_privileges` VALUES (16, 7, 1);
INSERT INTO `cms_menus_privileges` VALUES (17, 8, 2);
INSERT INTO `cms_menus_privileges` VALUES (18, 8, 1);
INSERT INTO `cms_menus_privileges` VALUES (19, 9, 2);
INSERT INTO `cms_menus_privileges` VALUES (20, 9, 1);
INSERT INTO `cms_menus_privileges` VALUES (21, 10, 2);
INSERT INTO `cms_menus_privileges` VALUES (22, 10, 1);
INSERT INTO `cms_menus_privileges` VALUES (23, 11, 2);
INSERT INTO `cms_menus_privileges` VALUES (24, 11, 1);
INSERT INTO `cms_menus_privileges` VALUES (25, 12, 2);
INSERT INTO `cms_menus_privileges` VALUES (26, 12, 1);
INSERT INTO `cms_menus_privileges` VALUES (27, 13, 2);
INSERT INTO `cms_menus_privileges` VALUES (28, 13, 1);

-- ----------------------------
-- Table structure for cms_moduls
-- ----------------------------
DROP TABLE IF EXISTS `cms_moduls`;
CREATE TABLE `cms_moduls`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `table_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `controller` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `is_protected` tinyint(1) NOT NULL DEFAULT 0,
  `is_active` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of cms_moduls
-- ----------------------------
INSERT INTO `cms_moduls` VALUES (1, 'Notifications', 'fa fa-cog', 'notifications', 'cms_notifications', 'NotificationsController', 1, 1, '2019-02-19 04:09:12', NULL, NULL);
INSERT INTO `cms_moduls` VALUES (2, 'Privileges', 'fa fa-cog', 'privileges', 'cms_privileges', 'PrivilegesController', 1, 1, '2019-02-19 04:09:12', NULL, NULL);
INSERT INTO `cms_moduls` VALUES (3, 'Privileges Roles', 'fa fa-cog', 'privileges_roles', 'cms_privileges_roles', 'PrivilegesRolesController', 1, 1, '2019-02-19 04:09:12', NULL, NULL);
INSERT INTO `cms_moduls` VALUES (4, 'Users Management', 'fa fa-users', 'users', 'cms_users', 'AdminCmsUsersController', 0, 1, '2019-02-19 04:09:12', NULL, NULL);
INSERT INTO `cms_moduls` VALUES (5, 'Settings', 'fa fa-cog', 'settings', 'cms_settings', 'SettingsController', 1, 1, '2019-02-19 04:09:12', NULL, NULL);
INSERT INTO `cms_moduls` VALUES (6, 'Module Generator', 'fa fa-database', 'module_generator', 'cms_moduls', 'ModulsController', 1, 1, '2019-02-19 04:09:12', NULL, NULL);
INSERT INTO `cms_moduls` VALUES (7, 'Menu Management', 'fa fa-bars', 'menu_management', 'cms_menus', 'MenusController', 1, 1, '2019-02-19 04:09:12', NULL, NULL);
INSERT INTO `cms_moduls` VALUES (8, 'Email Templates', 'fa fa-envelope-o', 'email_templates', 'cms_email_templates', 'EmailTemplatesController', 1, 1, '2019-02-19 04:09:12', NULL, NULL);
INSERT INTO `cms_moduls` VALUES (9, 'Statistic Builder', 'fa fa-dashboard', 'statistic_builder', 'cms_statistics', 'StatisticBuilderController', 1, 1, '2019-02-19 04:09:12', NULL, NULL);
INSERT INTO `cms_moduls` VALUES (10, 'API Generator', 'fa fa-cloud-download', 'api_generator', '', 'ApiCustomController', 1, 1, '2019-02-19 04:09:12', NULL, NULL);
INSERT INTO `cms_moduls` VALUES (11, 'Log User Access', 'fa fa-flag-o', 'logs', 'cms_logs', 'LogsController', 1, 1, '2019-02-19 04:09:12', NULL, NULL);
INSERT INTO `cms_moduls` VALUES (12, 'Barang', 'fa fa-gear', 'barang', 'barang', 'AdminBarangController', 0, 0, '2019-02-19 04:10:47', NULL, '2019-02-19 04:17:18');
INSERT INTO `cms_moduls` VALUES (13, 'Barang', 'fa fa-glass', 'barang13', 'barang', 'AdminBarang13Controller', 0, 0, '2019-02-19 04:17:28', NULL, NULL);

-- ----------------------------
-- Table structure for cms_notifications
-- ----------------------------
DROP TABLE IF EXISTS `cms_notifications`;
CREATE TABLE `cms_notifications`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_cms_users` int(11) NULL DEFAULT NULL,
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `is_read` tinyint(1) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cms_privileges
-- ----------------------------
DROP TABLE IF EXISTS `cms_privileges`;
CREATE TABLE `cms_privileges`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `is_superadmin` tinyint(1) NULL DEFAULT NULL,
  `theme_color` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of cms_privileges
-- ----------------------------
INSERT INTO `cms_privileges` VALUES (1, 'Super Administrator', 1, 'skin-red', '2019-02-19 04:09:12', NULL);
INSERT INTO `cms_privileges` VALUES (2, 'Admin', 0, 'skin-blue', NULL, NULL);

-- ----------------------------
-- Table structure for cms_privileges_roles
-- ----------------------------
DROP TABLE IF EXISTS `cms_privileges_roles`;
CREATE TABLE `cms_privileges_roles`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `is_visible` tinyint(1) NULL DEFAULT NULL,
  `is_create` tinyint(1) NULL DEFAULT NULL,
  `is_read` tinyint(1) NULL DEFAULT NULL,
  `is_edit` tinyint(1) NULL DEFAULT NULL,
  `is_delete` tinyint(1) NULL DEFAULT NULL,
  `id_cms_privileges` int(11) NULL DEFAULT NULL,
  `id_cms_moduls` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of cms_privileges_roles
-- ----------------------------
INSERT INTO `cms_privileges_roles` VALUES (1, 1, 0, 0, 0, 0, 1, 1, '2019-02-19 04:09:12', NULL);
INSERT INTO `cms_privileges_roles` VALUES (2, 1, 1, 1, 1, 1, 1, 2, '2019-02-19 04:09:12', NULL);
INSERT INTO `cms_privileges_roles` VALUES (3, 0, 1, 1, 1, 1, 1, 3, '2019-02-19 04:09:12', NULL);
INSERT INTO `cms_privileges_roles` VALUES (4, 1, 1, 1, 1, 1, 1, 4, '2019-02-19 04:09:12', NULL);
INSERT INTO `cms_privileges_roles` VALUES (5, 1, 1, 1, 1, 1, 1, 5, '2019-02-19 04:09:12', NULL);
INSERT INTO `cms_privileges_roles` VALUES (6, 1, 1, 1, 1, 1, 1, 6, '2019-02-19 04:09:12', NULL);
INSERT INTO `cms_privileges_roles` VALUES (7, 1, 1, 1, 1, 1, 1, 7, '2019-02-19 04:09:12', NULL);
INSERT INTO `cms_privileges_roles` VALUES (8, 1, 1, 1, 1, 1, 1, 8, '2019-02-19 04:09:12', NULL);
INSERT INTO `cms_privileges_roles` VALUES (9, 1, 1, 1, 1, 1, 1, 9, '2019-02-19 04:09:12', NULL);
INSERT INTO `cms_privileges_roles` VALUES (10, 1, 1, 1, 1, 1, 1, 10, '2019-02-19 04:09:12', NULL);
INSERT INTO `cms_privileges_roles` VALUES (11, 1, 0, 1, 0, 1, 1, 11, '2019-02-19 04:09:12', NULL);
INSERT INTO `cms_privileges_roles` VALUES (12, 1, 1, 1, 1, 1, 1, 12, NULL, NULL);
INSERT INTO `cms_privileges_roles` VALUES (13, 1, 1, 1, 1, 1, 1, 13, NULL, NULL);
INSERT INTO `cms_privileges_roles` VALUES (14, 1, 1, 1, 1, 1, 2, 13, NULL, NULL);
INSERT INTO `cms_privileges_roles` VALUES (15, 1, 1, 1, 1, 1, 2, 4, NULL, NULL);

-- ----------------------------
-- Table structure for cms_settings
-- ----------------------------
DROP TABLE IF EXISTS `cms_settings`;
CREATE TABLE `cms_settings`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `content_input_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `dataenum` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `helper` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `group_setting` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `label` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of cms_settings
-- ----------------------------
INSERT INTO `cms_settings` VALUES (1, 'login_background_color', NULL, 'text', NULL, 'Input hexacode', '2019-02-19 04:09:12', NULL, 'Login Register Style', 'Login Background Color');
INSERT INTO `cms_settings` VALUES (2, 'login_font_color', NULL, 'text', NULL, 'Input hexacode', '2019-02-19 04:09:12', NULL, 'Login Register Style', 'Login Font Color');
INSERT INTO `cms_settings` VALUES (3, 'login_background_image', 'uploads/2019-02/93853d69d83f2eab55a71de09e6ae61b.png', 'upload_image', NULL, NULL, '2019-02-19 04:09:12', NULL, 'Login Register Style', 'Login Background Image');
INSERT INTO `cms_settings` VALUES (4, 'email_sender', 'support@crudbooster.com', 'text', NULL, NULL, '2019-02-19 04:09:12', NULL, 'Email Setting', 'Email Sender');
INSERT INTO `cms_settings` VALUES (5, 'smtp_driver', 'mail', 'select', 'smtp,mail,sendmail', NULL, '2019-02-19 04:09:12', NULL, 'Email Setting', 'Mail Driver');
INSERT INTO `cms_settings` VALUES (6, 'smtp_host', '', 'text', NULL, NULL, '2019-02-19 04:09:12', NULL, 'Email Setting', 'SMTP Host');
INSERT INTO `cms_settings` VALUES (7, 'smtp_port', '25', 'text', NULL, 'default 25', '2019-02-19 04:09:12', NULL, 'Email Setting', 'SMTP Port');
INSERT INTO `cms_settings` VALUES (8, 'smtp_username', '', 'text', NULL, NULL, '2019-02-19 04:09:12', NULL, 'Email Setting', 'SMTP Username');
INSERT INTO `cms_settings` VALUES (9, 'smtp_password', '', 'text', NULL, NULL, '2019-02-19 04:09:12', NULL, 'Email Setting', 'SMTP Password');
INSERT INTO `cms_settings` VALUES (10, 'appname', 'eAsset', 'text', NULL, NULL, '2019-02-19 04:09:12', NULL, 'Application Setting', 'Application Name');
INSERT INTO `cms_settings` VALUES (11, 'default_paper_size', 'Legal', 'text', NULL, 'Paper size, ex : A4, Legal, etc', '2019-02-19 04:09:12', NULL, 'Application Setting', 'Default Paper Print Size');
INSERT INTO `cms_settings` VALUES (12, 'logo', 'uploads/2019-02/3547a811b1d8a8e51982defcd0df9f02.png', 'upload_image', NULL, NULL, '2019-02-19 04:09:12', NULL, 'Application Setting', 'Logo');
INSERT INTO `cms_settings` VALUES (13, 'favicon', 'uploads/2019-02/a7ae46c719c6c9cced32e8f2200badde.png', 'upload_image', NULL, NULL, '2019-02-19 04:09:12', NULL, 'Application Setting', 'Favicon');
INSERT INTO `cms_settings` VALUES (14, 'api_debug_mode', 'true', 'select', 'true,false', NULL, '2019-02-19 04:09:12', NULL, 'Application Setting', 'API Debug Mode');
INSERT INTO `cms_settings` VALUES (15, 'google_api_key', NULL, 'text', NULL, NULL, '2019-02-19 04:09:12', NULL, 'Application Setting', 'Google API Key');
INSERT INTO `cms_settings` VALUES (16, 'google_fcm_key', NULL, 'text', NULL, NULL, '2019-02-19 04:09:12', NULL, 'Application Setting', 'Google FCM Key');

-- ----------------------------
-- Table structure for cms_statistic_components
-- ----------------------------
DROP TABLE IF EXISTS `cms_statistic_components`;
CREATE TABLE `cms_statistic_components`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_cms_statistics` int(11) NULL DEFAULT NULL,
  `componentID` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `component_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `area_name` varchar(55) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `sorting` int(11) NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `config` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cms_statistics
-- ----------------------------
DROP TABLE IF EXISTS `cms_statistics`;
CREATE TABLE `cms_statistics`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cms_users
-- ----------------------------
DROP TABLE IF EXISTS `cms_users`;
CREATE TABLE `cms_users`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `photo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `id_cms_privileges` int(11) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `status` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of cms_users
-- ----------------------------
INSERT INTO `cms_users` VALUES (1, 'SA', NULL, 'admin@crudbooster.com', '$2y$10$mdxrw5cDUXrQZO2sQBBka.KDbms1cLbSUHl0pxxYqgACxXwlfhu9W', 1, '2019-02-19 04:09:12', NULL, 'Active');
INSERT INTO `cms_users` VALUES (2, 'Admin', 'uploads/1/2019-02/avatar_372_456324.png', 'admin@easset.com', '$2y$10$nCqBWa3UjZVlipEFriFr4eSF6vsY6FwI9fm2fEtRqQGQxDuZzf3e.', 2, '2019-02-25 03:06:58', NULL, NULL);

-- ----------------------------
-- Table structure for detail_bast_penggunaan
-- ----------------------------
DROP TABLE IF EXISTS `detail_bast_penggunaan`;
CREATE TABLE `detail_bast_penggunaan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bast_penggunaan_id` int(11) NULL DEFAULT NULL,
  `barang_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_detail_bast_penggunaan_barang_1`(`barang_id`) USING BTREE,
  INDEX `fk_detail_bast_penggunaan_bast_penggunaan_1`(`bast_penggunaan_id`) USING BTREE,
  CONSTRAINT `fk_detail_bast_penggunaan_barang_1` FOREIGN KEY (`barang_id`) REFERENCES `barang` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_detail_bast_penggunaan_bast_penggunaan_1` FOREIGN KEY (`bast_penggunaan_id`) REFERENCES `bast_penggunaan` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for detail_sensus
-- ----------------------------
DROP TABLE IF EXISTS `detail_sensus`;
CREATE TABLE `detail_sensus`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sensus_id` int(11) NULL DEFAULT NULL,
  `barang_id` int(11) NULL DEFAULT NULL,
  `kondisi_barang` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_detail_sensus_sensus_1`(`sensus_id`) USING BTREE,
  INDEX `fk_detail_sensus_barang_1`(`barang_id`) USING BTREE,
  CONSTRAINT `fk_detail_sensus_barang_1` FOREIGN KEY (`barang_id`) REFERENCES `barang` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_detail_sensus_sensus_1` FOREIGN KEY (`sensus_id`) REFERENCES `sensus` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2016_08_07_145904_add_table_cms_apicustom', 1);
INSERT INTO `migrations` VALUES (2, '2016_08_07_150834_add_table_cms_dashboard', 1);
INSERT INTO `migrations` VALUES (3, '2016_08_07_151210_add_table_cms_logs', 1);
INSERT INTO `migrations` VALUES (4, '2016_08_07_151211_add_details_cms_logs', 1);
INSERT INTO `migrations` VALUES (5, '2016_08_07_152014_add_table_cms_privileges', 1);
INSERT INTO `migrations` VALUES (6, '2016_08_07_152214_add_table_cms_privileges_roles', 1);
INSERT INTO `migrations` VALUES (7, '2016_08_07_152320_add_table_cms_settings', 1);
INSERT INTO `migrations` VALUES (8, '2016_08_07_152421_add_table_cms_users', 1);
INSERT INTO `migrations` VALUES (9, '2016_08_07_154624_add_table_cms_menus_privileges', 1);
INSERT INTO `migrations` VALUES (10, '2016_08_07_154624_add_table_cms_moduls', 1);
INSERT INTO `migrations` VALUES (11, '2016_08_17_225409_add_status_cms_users', 1);
INSERT INTO `migrations` VALUES (12, '2016_08_20_125418_add_table_cms_notifications', 1);
INSERT INTO `migrations` VALUES (13, '2016_09_04_033706_add_table_cms_email_queues', 1);
INSERT INTO `migrations` VALUES (14, '2016_09_16_035347_add_group_setting', 1);
INSERT INTO `migrations` VALUES (15, '2016_09_16_045425_add_label_setting', 1);
INSERT INTO `migrations` VALUES (16, '2016_09_17_104728_create_nullable_cms_apicustom', 1);
INSERT INTO `migrations` VALUES (17, '2016_10_01_141740_add_method_type_apicustom', 1);
INSERT INTO `migrations` VALUES (18, '2016_10_01_141846_add_parameters_apicustom', 1);
INSERT INTO `migrations` VALUES (19, '2016_10_01_141934_add_responses_apicustom', 1);
INSERT INTO `migrations` VALUES (20, '2016_10_01_144826_add_table_apikey', 1);
INSERT INTO `migrations` VALUES (21, '2016_11_14_141657_create_cms_menus', 1);
INSERT INTO `migrations` VALUES (22, '2016_11_15_132350_create_cms_email_templates', 1);
INSERT INTO `migrations` VALUES (23, '2016_11_15_190410_create_cms_statistics', 1);
INSERT INTO `migrations` VALUES (24, '2016_11_17_102740_create_cms_statistic_components', 1);
INSERT INTO `migrations` VALUES (25, '2017_06_06_164501_add_deleted_at_cms_moduls', 1);

-- ----------------------------
-- Table structure for parameter
-- ----------------------------
DROP TABLE IF EXISTS `parameter`;
CREATE TABLE `parameter`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nilai` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of parameter
-- ----------------------------
INSERT INTO `parameter` VALUES (1, 'Satker', 'DIREKTORAT PRODUKSI DAN DISTRIBUSI KEFARMASIAN');

-- ----------------------------
-- Table structure for pengguna
-- ----------------------------
DROP TABLE IF EXISTS `pengguna`;
CREATE TABLE `pengguna`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `nip` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `jabatan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `golongan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for ruang
-- ----------------------------
DROP TABLE IF EXISTS `ruang`;
CREATE TABLE `ruang`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for satker
-- ----------------------------
DROP TABLE IF EXISTS `satker`;
CREATE TABLE `satker`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `alamat` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for sensus
-- ----------------------------
DROP TABLE IF EXISTS `sensus`;
CREATE TABLE `sensus`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` date NULL DEFAULT NULL,
  `nomor` varchar(0) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `ruang_id` int(11) NULL DEFAULT NULL,
  `nama_pengguna` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_sensus_ruang_1`(`ruang_id`) USING BTREE,
  CONSTRAINT `fk_sensus_ruang_1` FOREIGN KEY (`ruang_id`) REFERENCES `ruang` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
